<?php include("include/connect.php"); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo  ucwords($rstitle)?></title>
<!-- <link href="dq_sirs.css" type="text/css" rel="stylesheet" /> -->
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" />
<link href="css/custom.css" type="text/css" rel="stylesheet" />
<link rel="shortcut icon" href="img/icon.png" />
<script type="text/javascript" language="javascript" src="include/ajaxrequest.js"></script>
<script src="js/jquery-1.7.min.js" language="JavaScript" type="text/javascript"></script>
<SCRIPT>
function jumpTo (link){
   var new_url=link;
   if ((new_url != "")  &&  (new_url != null))
   window.location=new_url;
}

jQuery(document).ready(function(){
	jQuery("#NIP").keyup(function(event){
		if(event.keyCode == 13){
			MyAjaxRequest('valid_nip','include/process.php?NIP=','NIP');
			jQuery("#PWD").focus();
		}
	});
	
	jQuery("#PWD").keyup(function(event){		
		if(event.keyCode == 13){			
			//MyAjaxRequest('valid_pwd','include/process.php?PWD=','PWD');
			jQuery('#frm').submit();
		}
	});
});
</script>
</head>

<body style="background:#65a965;">
<!-- <div id="header">
    <div id="bg_variation">
	<div id="logo">
    </div>
        <div id="info_header">
        	<div id="info_isi">
            <div><?php echo strtoupper($singrstitl)?> <?php echo strtoupper($singhead1)?></div>
             <div>
                <?php
                if(isset($_SESSION['SES_REG'])){
                    echo '<a href="log_out.php">Sign Out</a> | User : '.$_SESSION['NIP'];  
                }else{
                    echo '<a href="login.php">Sign In</a> | guest';
                }
                ?>
            </div>
            <div class="date"><?php echo date("l, F Y"); ?></div>
			<div class="date">
				<?php
					if(isset($_SESSION['KDUNIT']) != ''):
						$dep  = "SELECT * FROM m_login WHERE KDUNIT = '".$_SESSION['KDUNIT']."' AND ROLES = '".$_SESSION['ROLES']."'"; 
						$qe   = mysql_query($dep);
						if($qe){
							$deps = mysql_fetch_assoc($qe);
							echo "<div><b>".$deps['DEPARTEMEN']."</b></div>";
						}
					endif;
				?>
             </div>
             </div>                        
        </div>
    </div>
</div> -->


<div class="container" style="margin-top:7%; padding:0px 10%;">
    <div class="row" style=" box-shadow: 1px 5px 30px rgba(10,10,10,0.3);">
        <div class="col-md-6" style="background:#c3ecc3; padding: 150px 50px; height: 420px;">
            <img src="img/pirrs.png" class="img-fluid" style="max-height: 100px;">
        </div>
        <div class="col-md-6" style="background:#fff; padding: 50px;">
        <br>
        <h3>SIGN IN FORM</h3>
        <hr style="width: 100px; height:2px; background:#2da1d1; float: left; margin-bottom: 40px; margin-top: 0px;">
        <form name="frm" id="frm" action="user_level.php" method="post">
            <?php
              if(isset($_POST['signin'])){
                require_once("login.php");
              } 
            ?>
            <div class="login-form">
                <div class="form-group">
                    <input style="border:none; border-bottom: 2px solid #ddd;" class="form-control" id="NIP" type="text" name="USERNAME" placeholder="Username" autocomplete="off"  onBlur="javascript: MyAjaxRequest('valid_nip','include/process.php?NIP=','NIP');return false;" /><span id="valid_nip"></span>
                </div>
                <div class="form-group">
                    <input style="border:none; border-bottom: 2px solid #ddd;" class="form-control" type="password" id="PWD" name="PWD" placeholder="Password"/>
                </div>
                <div class="form-group login-submit">
                    <input type="button" value=" SIGN IN " class="btn btn-primary btn-block" style="margin: 0px auto;" name="LOGIN" id="LOGIN" onclick="document.getElementById('frm').submit();"/><span id="valid_pwd"></span>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container text-center">
      <span class="text-muted">Sistem Informasi Manajemen Rumah Sakit PIR &copy 2017</span>
    </div>
</footer>

<!-- <div id="fixed-footer">
    <div id="footer-inner">
      <ul class="footer-navigation">
            <li><a href=""><?php echo  ucwords($rstitle)?> &copy; <?php echo date("Y"); ?></a></li>
      </ul>
    </div>
</div> -->
</body>
</html><div>
</div>