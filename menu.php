<?php session_start();
if(!isset($_SESSION['SES_REG'])){
    header("location:login.php");
}
if($_SESSION['ROLES']=="3") { ?>
<link href="../css/dropdown/dropdown.css" media="all" rel="stylesheet" type="text/css" />
<link href="../css/dropdown/themes/default/default.css" media="all" rel="stylesheet" type="text/css" />
    <?php }else { ?>
<!-- <link href="css/dropdown/dropdown.css" media="all" rel="stylesheet" type="text/css" /> -->
<!-- <link href="css/dropdown/themes/default/default.css" media="all" rel="stylesheet" type="text/css" /> -->
<link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <?php } ?>
<?php if($_SESSION['ROLES']=="1017") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a class="dir" href="#">MASTER</a>
        <ul>
            <li><a href="index.php?link=add_user">ADD USER</a></li>
            <li><a href="index.php?link=private">LIST USER</a></li>
            <li><a href="?link=191">EDIT ICD</a></li>
            <li><a href="?link=19">LIST ICD</a></li>
            <li><a href="index.php?link=jdoc2">ADD JADWAL</a></li>
            <li><a href="index.php?link=jdoc3">LIST JADWAL</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="1") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="#pendaftaran" data-toggle="collapse">PENDAFTARAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="pendaftaran">
            <li><a href="index.php?link=2" class="dir">PENDAFTARAN PASIEN</a>
			<li><a href="index.php?link=telepon">PENDAFTARAN MELALUI TELEPON</a></li>
            <li><a href="index.php?link=2bayi">PENDAFTARAN BAYI BARU LAHIR</a></li>
        </ul>
    </li>
    <li><a href="index.php?link=21">LIST DATA PASIEN</a></li>
    <li><a href="index.php?link=22">LIST KUNJUNGAN PASIEN</a></li>
    <li><a href="index.php?link=14_">ASURANSI</a></li>
    <li><a href="#pendaftaranLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="pendaftaranLaporan" class="collapse">
            <li><a href="index.php?link=140">REKAP PENDAFTARAN PASIEN RAWAT JALAN</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="2") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="#pembayaranBill" data-toggle="collapse">BILL <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="pembayaranBill" class="collapse">
            <li><a href="index.php?link=33" class="dir">BILL RAJAL</a>
            <li><a href="index.php?link=billaps">BILL APS</a></li>
		</ul>
    </li>
    <li><a href="index.php?link=37">DEPOSIT RANAP</a></li>
    <li><a href="index.php?link=33a">BILL RANAP</a></li>
	<!--<li><a href="#" class="dir">BILL DEPO</a>
		<ul>-->
			<li><a href="index.php?link=33depo_rajal">DEPO RAJAL</a></li>
			<!--<li><a href="index.php?link=33depo_ranap">DEPO RANAP</a></li>-->
            <!--
		</ul>
	</li>
	<li><a href="index.php?link=33gizi">BILL GIZI</a>
		<ul>-->
			<li><a href="index.php?link=33gizi_rajal">GIZI RAJAL</a></li>
			<!--<li><a href="index.php?link=33gizi_ranap">GIZI RANAP</a></li>
		</ul>
	</li>-->
    <li><a href="#pembayaranLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="pembayaranLaporan" class="collapse">
            <li><a href="index.php?link=31">LAPORAN RAWAT JALAN</a></li>
            <li><a href="index.php?link=35">LAPORAN RAWAT INAP</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="4") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="index.php?link=5&page=<?php echo $_SESSION['page']; ?>&tgl_reg=<?php echo $_SESSION['tgl_reg']; ?>&tgl_reg2=<?php echo $_SESSION['tgl_reg2']; ?>&nama=<?php echo $_SESSION['nama']; ?>&norm=<?php echo $_SESSION['norm']; ?>">LIST KUNJUNGAN PASIEN</a></li>

        <?php if($_SESSION['KDUNIT']=="10") { ?>
	<li><a href="index.php?link=5ranap">LIST PASIEN RAWAT INAP VK</a></li>
    <li><a href="index.php?link=v01">REGISTRASI PARTUS</a></li>
    <!--<li><a href="?link=v03">LIST OPERASI</a></li>-->
    <li><a href="#dalamLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="dalamLaporan" class="collapse">
            <li><a href="?link=139">SENSUS HARIAN</a></li>
            <li><a href="?link=jas9">JASPEL</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>

            <?php }elseif($_SESSION['KDUNIT']=="9") { ?>

    <li><a href="#dalamLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="dalamLaporan" class="collapse">
            <li><a href="?link=54">SENSUS HARIAN</a></li>
            <!--
			<li><a href="?link=jas3">JASPEL</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
			-->
        </ul>
    </li>

            <?php }else { ?>

    <li><a href="#dalamLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="dalamLaporan" class="collapse">
            <li><a href="?link=54">SENSUS HARIAN</a></li>
            <li><a href="?link=jas1">JASPEL</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>

            <?php } ?>

    <li><a href="#dalamFarmasi" data-toggle="collapse">FARMASI & LOGISTIK <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="dalamFarmasi">
            <li><a href="index.php?link=f04" >PENGELUARAN</a></li>
            <li><a href="index.php?link=f06" >LIST PENGELUARAN</a></li>
            <li><a href="index.php?link=f01" >PERMINTAAN</a></li>
            <li><a href="index.php?link=f02" >LIST PERMINTAAN</a></li>
            <li><a href="index.php?link=f21" >PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f22" >LIST PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f07" >PERENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f08" >LIST PRENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f09" >LAPORAN BULANAN</a></li>
            <li><a href="index.php?link=f11" >LAPORAN HARIAN</a></li>
            <li><a href="index.php?link=f66" >LAPORAN STOK</a></li>
        </ul>
    </li>
    <!--
    <li><a class="dir" href="#">RAWAT INAP</a>
        <ul>
            <li><a href="index.php?link=124" >DATA KAMAR</a></li>
        </ul>
    </li>
    -->
    <li><a href="#dalamMaster" data-toggle="collapse">MASTER <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="dalamMaster">
            <li><a href="index.php?link=19" >ICD</a></li>
        </ul>
    </li>

</ul>

    <?php }elseif($_SESSION['ROLES']=="5") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
	<li><a href="?link=6">LIST ORDER LAB</a></li>
    <li><a href="?link=6order">LIST PEMERIKSAAN LAB</a></li>
    <li><a href="?link=61">HASIL PEMERIKSAAN LAB</a></li>
    <li><a href="?link=l01">DAFTAR APS</a></li>
	<li><a href="?link=list_pasien_ranap_lab">DAFTAR PASIEN RANAP</a></li>
    <li><a href="?link=list_pasien_rajal_lab">DAFTAR PASIEN RAJAL</a></li>
    <li><a href="?link=sisipan_lab">SISIPAN</a></li>
    <!--<li><a href="?link=l03">LIST ORDER APS</a></li>-->
    <li><a href="#laboratoriumLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="laboratoriumLaporan" class="collapse">
            <li><a href="?link=l05">REGISTER PELAYANAN</a></li>
            <li><a href="?link=jas5">JASPEL</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
    <!--
    <li><a class="dir" href="#">FARMASI & LOGISTIK</a>
        <ul>
            <li><a href="index.php?link=f04" >PENGELUARAN</a></li>
            <li><a href="index.php?link=f06" >LIST PENGELUARAN</a></li>
            <li><a href="index.php?link=f01" >PERMINTAAN</a></li>
            <li><a href="index.php?link=f02" >LIST PERMINTAAN</a></li>
            <li><a href="index.php?link=f21" >PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f22" >LIST PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f07" >PERENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f08" >LIST PRENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f09" >LAPORAN BULANAN</a></li>
            <li><a href="index.php?link=f11" >LAPORAN HARIAN</a></li>
            <li><a href="index.php?link=f66" >LAPORAN STOK</a></li>
        </ul>
    </li>
    -->
</ul>

    <?php }elseif($_SESSION['ROLES']=="6") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
	<li><a href="?link=7">LIST ORDER RADIOLOGI</a> </li>
    <li><a href="?link=7order">LIST PEMERIKSAAN RADIOLOGI</a> </li>
    <li><a href="?link=71">HASIL PEMERIKSAAN RADIOLOGI</a></li>
    <li><a href="?link=r01">DAFTAR APS</a></li>
    <li><a href="?link=list_pasien_ranap_rad">DAFTAR PASIEN RANAP</a></li>
    <li><a href="?link=list_pasien_rajal_rad">DAFTAR PASIEN RAJAL</a></li>
    <!--
    <li><a href="?link=r03">LIST ORDER APS</a></li>
    <li><a href="?link=r05">HASIL PEMERIKSAAN RADIOLOGI APS</a></li>
    -->
    <li><a href="#radiologiLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="radiologiLaporan">
            <li><a href="?link=74">REGISTER PELAYANAN</a></li>
            <li><a href="?link=jas6">JASPEL</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
    <!--
    <li><a class="dir" href="#">FARMASI & LOGISTIK</a>
        <ul>
            <li><a href="index.php?link=f04" >PENGELUARAN</a></li>
            <li><a href="index.php?link=f06" >LIST PENGELUARAN</a></li>
            <li><a href="index.php?link=f01" >PERMINTAAN</a></li>
            <li><a href="index.php?link=f02" >LIST PERMINTAAN</a></li>
            <li><a href="index.php?link=f21" >PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f22" >LIST PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f07" >PERENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f08" >LIST PRENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f09" >LAPORAN BULANAN</a></li>
            <li><a href="index.php?link=f11" >LAPORAN HARIAN</a></li>
            <li><a href="index.php?link=f66" >LAPORAN STOK</a></li>
        </ul>
    </li>
    -->
</ul>

    <?php }elseif($_SESSION['ROLES']=="7") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="#gudangPermintaan" data-toggle="collapse">PERMINTAAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="gudangPermintaan" class="collapse">
            <li><a href="?link=8">LIST PERMINTAAN</a></li>
            <li><a href="?link=85">HISTORI PERMINTAAN</a></li>
        </ul>
    </li>
    <li><a href="#gudangPenerimaan" data-toggle="collapse">PENERIMAAN BARANG <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="gudangPenerimaan" class="collapse">
            <li><a href="?link=83">FORM PENERIMAAN</a></li>
            <li><a href="?link=x83">HISTORI PENERIMAAN</a></li>
        </ul>
    </li>
    <!--   <li><a href="?link=x83">PENGEMBALIAN BARANG</a></li> -->
    <li><a href="?link=89">PERENCANAAN PENGADAAN</a></li>
    <li><a href="?link=82">MASTER BARANG</a></li>
    <li><a href="#gudangLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="gudangLaporan" class="collapse">
            <!-- <li><a href="index.php?link=84&lap=1" >HARIAN</a></li> -->
            <li><a href="index.php?link=84&lap=2" >BULANAN</a></li>
            <li><a href="index.php?link=84&lap=3" >REKAP BULANAN</a></li>
            <li><a href="index.php?link=84&lap=4" >REKAP TRIWULAN</a></li>
            <li><a href="index.php?link=84&lap=5" >REKAP TAHUNAN</a></li>
            <li><a href="index.php?link=84&lap=6" >STOK UNIT</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="8") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="#logistikPermintaan" data-toggle="collapse">PERMINTAAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="logistikPermintaan">
            <li><a href="?link=8">LIST PERMINTAAN</a></li>
            <li><a href="?link=85">HISTORI PERMINTAAN</a></li>
        </ul>
    </li>
    <li><a href="#logistikPenerimaan" data-toggle="collapse">PENERIMAAN BARANG <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="logistikPenerimaan">
            <li><a href="?link=83">FORM PENERIMAAN</a></li>
            <li><a href="?link=x83">HISTORI PENERIMAAN</a></li>
        </ul>
    </li>
    <li><a href="?link=x83">PENGEMBALIAN BARANG</a></li>
    <li><a href="?link=89">PERENCANAAN PENGADAAN</a></li>
    <li><a href="?link=82">MASTER BARANG</a></li>
    <li><a href="#logistikLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="logistikLaporan">
            <li><a href="index.php?link=84&lap=1" >HARIAN</a></li>
            <li><a href="index.php?link=84&lap=2" >BULANAN</a></li>
            <li><a href="index.php?link=84&lap=3" >REKAP BULANAN</a></li>
            <li><a href="index.php?link=84&lap=4" >REKAP TRIWULAN</a></li>
            <li><a href="index.php?link=84&lap=5" >REKAP TAHUNAN</a></li>
            <li><a href="index.php?link=84&lap=6" >STOK UNIT</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="9") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <!--<li><a class="dir" href="#">PERMINTAAN RESEP</a>
        <ul>
            <li><a href="?link=10">LIST PERMINTAAN RESEP</a></li>
            <li><a href="?link=107">HISTORI PERMINTAAN RESEP</a></li>
        </ul>
    </li>-->
    <li><a href="?link=list_pasien_apotek_rajal">LIST PASIEN RAJAL</a></li>
    <li><a href="?link=list_pasien_apotek_ranap">LIST PASIEN RANAP</a></li>
	<li><a href="?link=list_pasien_apotek_aps">LIST PASIEN APS</a></li>
    <li><a href="#apotikLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="apotikLaporan">
        	<li><a href="?link=list_obat_rajal">PENGELUARAN OBAT RAJAL</a></li>
            <li><a href="?link=list_obat_ranap">PENGELUARAN OBAT RANAP</a></li>
			<li><a href="?link=list_obat_aps">PENGELUARAN OBAT APS</a></li>
            <li><a href="?link=114" >REKAP RESEP</a></li>
            <!--<li><a href="?link=110" >SENSUS LAYANAN</a></li>-->
            <li><a href="#apotikLaporanPemantauan" data-toggle="collapse">LAPORAN PEMANTAUAN RESEP OBAT GENERIK <span class="fa fa-caret-down pull-right"></span></a>
            	<ul class="collapse" id="apotikLaporanPemantauan">
                	<li><a href="?link=110x" >RAWAT JALAN</a></li>
                    <li><a href="?link=110xt" >RAWAT INAP</a></li>
                </ul>
            </li>
            
            <!--<li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>-->
        </ul>
    </li>
    <li><a href="#apotikFarmasiLogistik" data-toggle="collapse">FARMASI & LOGISTIK <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="apotikFarmasiLogistik">
            <li><a href="index.php?link=f04" >PENGELUARAN</a></li>
            <li><a href="index.php?link=f06" >LIST PENGELUARAN</a></li>
            <li><a href="index.php?link=f01" >PERMINTAAN</a></li>
            <li><a href="index.php?link=f02" >LIST PERMINTAAN</a></li>
            <li><a href="index.php?link=f21" >PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f22" >LIST PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f07" >PERENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f08" >LIST PRENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f09" >LAPORAN BULANAN</a></li>
            <li><a href="index.php?link=f11" >LAPORAN HARIAN</a></li>
            <li><a href="index.php?link=f66" >LAPORAN STOK</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="10") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="?link=11">LIST KUNJUNGAN PASIEN UGD</a></li>
    <li><a href="?link=111">PEMERIKSAAN</a></li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="11") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="?link=12">LIST PASIEN RAWAT INAP</a> </li>
    <li><a href="?link=129x">PERM. MAKAN</a></li>
    <li><a href="?link=124">DATA KAMAR</a></li>
    <li><a href="?link=125">PENCARIAN PASIEN</a></li>
    <li><a href="#ranapLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="ranapLaporan">
            <li><a href="?link=122harian">LAPORAN HARIAN</a></li>
            <li><a href="?link=122harianpasienkeluar">LAPORAN HARIAN PASIEN KELUAR</a></li>
            <li><a href="?link=122">SENSUS HARIAN</a></li>
            <li><a href="?link=122x">BUKU REGISTER</a></li>
            <li><a href="?link=jas4">JASPEL</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
    <li><a href="#ranapFarmasi" data-toggle="collapse">FARMASI & LOGISTIK <span class="fa fa-caret-down pull-right" ></span></a>
        <ul id="ranapFarmasi" class="collapse">
            <li><a href="index.php?link=f04" >PENGELUARAN</a></li>
            <li><a href="index.php?link=f06" >LIST PENGELUARAN</a></li>
            <li><a href="index.php?link=f01" >PERMINTAAN</a></li>
            <li><a href="index.php?link=f02" >LIST PERMINTAAN</a></li>
            <li><a href="index.php?link=f21" >PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f22" >LIST PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f07" >PERENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f08" >LIST PRENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f09" >LAPORAN BULANAN</a></li>
            <li><a href="index.php?link=f11" >LAPORAN HARIAN</a></li>
            <li><a href="index.php?link=f66" >LAPORAN STOK</a></li>
        </ul>
    </li>
    <li><a href="#ranapMaster" data-toggle="collapse">MASTER <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="ranapMaster" class="collapse">
            <li><a href="?link=19">ICD</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="12") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="?link=13">TRACER</a></li>
    <li><a href="#rekamMedikLapInternal" data-toggle="collapse">LAPORAN INTERNAL <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="rekamMedikLapInternal">
            <li><a href="?link=133">SENSUS HARIAN RAWAT JALAN</a></li>
            <li><a href="?link=140">SENSUS PENDAFTARAN RAWAT JALAN</a></li>
            <li><a href="?link=134">SENSUS HARIAN RAWAT INAP</a></li>
			<li><a href="?link=135">LIST PASIEN RAWAT INAP</a></li>
            <!--<li><a href="?link=137">LAPORAN HARIAN PERINATOLOGI</a></li>-->
            <li><a href="?link=138">LAPORAN HARIAN RAWAT INAP</a></li>
            <li><a href="?link=139">LAPORAN HARIAN VK</a></li>
            <li><a href="?link=lapok">LAPORAN HARIAN KAMAR OPERASI</a></li>
            <li><a href="?link=1311">SENSUS LAB</a></li>
            <li><a href="?link=1316">SENSUS HARIAN UGD</a></li>
            <li><a href="?link=1313">SENSUS RADIOLOGI</a></li>
            
        </ul>
    </li>
    <li><a href="#rekamMedikRekapanInternal" data-toggle="collapse">REKAPAN INTERNAL <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="rekamMedikRekapanInternal">
            <li><a href="?link=140R">REKAP PENDAFTARAN RAWAT JALAN</a></li>
            <li><a href="?link=144R">REKAP STATUS PULANG RAWAT JALAN</a></li>
            <li><a href="?link=141R">REKAP POLIKLINIK RAWAT JALAN</a></li>
            <li><a href="?link=142R">REKAP PENDAFTARAN RAWAT INAP</a></li>
            <!--<li><a href="?link=143R">REKAP MORDIBITAS RAWAT JALAN</a></li>-->
            <!--<li><a href="?link=RL2A">RL 2 A</a></li>
            <li><a href="?link=RL2A1">RL 2 A1</a></li>
            <li><a href="?link=RL2B">RL 2 B</a></li>
            <li><a href="?link=RL2B1">RL 2 B1</a></li>-->
        </ul>
    </li>
    <li><a href="#rekamMedikRiwayatPasien" data-toggle="collapse">RIWAYAT PASIEN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="rekamMedikRiwayatPasien">
            <li><a href="?link=rm4">RAWAT JALAN</a></li>
            <li><a href="?link=rm5">RAWAT INAP</a></li>
        </ul>
    </li>
    <li><a href="#rekamMedikLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="rekamMedikLaporan">
            <li><a href="?link=private21">GRAFIK KUNJUNGAN PASIEN</a></li>
            <li><a href="?link=jas1">JASPEL</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
            <li><a href="?link=iso2" >ISO PENDAFTARAN</a></li>
            <li><a href="?link=pasienrujukan">PASIEN RUJUKAN</a></li>
        </ul>
    </li>
    <li><a href="#rekamMedikLaporanRL" data-toggle="collapse">LAPORAN RL <span class="fa fa-caret-down pull-right"></span></a>
		<ul class="collapse" id="rekamMedikLaporanRL">
			<li><a href="#rekamMedikRL1" data-toggle="collapse">RL 1 <span class="fa fa-caret-down pull-right"></span></a>
				<ul class="collapse" id="rekamMedikRL1">
					<li><a href="?link=rl11">RL 1.1 Data Dasar</a></li>
					<li><a href="?link=rl12">RL 1.2 Indikator Pelayanan</a></li>
					<li><a href="?link=rl13">RL 1.3 Tempat Tidur</a></li>
				</ul>
			</li>
			<li><a href="?link=rl2">RL 2 Ketenagaan</a></li>
			<li><a href="#rekamMedikRL3" data-toggle="collapse">RL 3 <span class="fa fa-caret-down pull-right"></span></a>
				<ul class="collapse" id="rekamMedikRL3">
					<li><a href="?link=rl31">RL 3.1 Rawat Inap</a></li>
					<li><a href="?link=rl32">RL 3.2 Rawat Darurat</a></li>
					<li><a href="?link=rl33">RL 3.3 Gigi Mulut</a></li>
					<li><a href="?link=rl34">RL 3.4 Kebidanan</a></li>
					<li><a href="?link=rl35">RL 3.5 Perinatologi</a></li>
					<li><a href="?link=rl36">RL 3.6 Pembedahan</a></li>
					<li><a href="?link=rl37">RL 3.7 Radiologi</a></li>
					<li><a href="?link=rl38">RL 3.8 Laboratorium</a></li>
					<li><a href="?link=rl39">RL 3.9 Rehab Medik</a></li>
					<li><a href="?link=rl310">RL 3.10 Pelayanan Khusus</a></li>
					<li><a href="?link=rl311">RL 3.11 Kesehatan Jiwa</a></li>
					<li><a href="?link=rl312">RL 3.12 Keluarga Berencana</a></li>
					<li><a href="?link=rl313">RL 3.13 Obat</a></li>
					<li><a href="?link=rl314">RL 3.14 Rujukan</a></li>
					<li><a href="?link=rl315">RL 3.15 Cara Bayar</a></li>
				</ul>
			</li>
			<li><a href="#rekamMedikRL4" data-toggle="collapse">RL 4 <span class="fa fa-caret-down pull-right"></span></a>
				<ul class="collapse" id="rekamMedikRL4">
					<li><a href="?link=RL2A">RL 4A Penyakit Rawat Inap</a></li>
					<li><a href="?link=RL2B">RL 4B Penyakit Rawat Jalan</a></li>
				</ul>
			</li>
			<li><a href="#rekamMedikRL5" data-toggle="collapse">RL 5 <span class="fa fa-caret-down pull-right"></span></a>
				<ul class="collapse" id="rekamMedikRL5">
					<li><a href="?link=rl51">RL 5.1 Pengunjung</a></li>
					<li><a href="?link=rl52">RL 5.2 Kunj. Rawat Jalan</a></li>
					<li><a href="?link=rl5310">RL 5.3 10 Penyakit Rawat Inap</a></li>
					<li><a href="?link=rl5410">RL 5.4 10 Penyakit Rawat Jalan</a></li>
				</ul>
			</li>
		</ul>
	</li>
    <li><a href="#rekamMedikMaster" data-toggle="collapse">MASTER <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="rekamMedikMaster">
            <li><a href="?link=19">ICD</a></li>
        </ul>
    </li>
</ul>


    <?php }elseif($_SESSION['ROLES']=="13") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="?link=14_">VERIFIKASI</a></li>
    <li><a href="?link=14_askes">DATA ASKES</a></li>
    <!--
    <li><a class="dir" href="#">KLAIM</a>
        <ul>
            <li><a href="?link=141" >INA - DRG</a></li>
            <li><a href="?link=142" >RAWAT JALAN</a></li>
            <li><a href="?link=143" >RAWAT INAP</a></li>
        </ul>
    </li>
    
    <li><a class="dir" href="#">VERIFIKASI</a>
        <ul>
            <li><a href="?link=145" >RAWAT JALAN</a></li>
            <li><a href="?link=146" >RAWAT INAP</a></li>
        </ul>
    </li>
    -->
    <li><a href="?link=33a_asuransi" >BILLING RANAP</a></li>
    <li><a href="?link=33_asuransi" >BILLING RAJAL</a></li>
    <li><a href="#jamkesmasLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="jamkesmasLaporan">
        	<li><a href="?link=144_rekap" >REKAPITULASI KLAIM ASURANSI RAJAL</a></li>
            <li><a href="?link=144_rekap_ranap" >REKAPITULASI KLAIM ASURANSI RANAP</a></li>
            <!--<li><a href="?link=144" >REKAPITULASI KLAIM</a></li>-->
            <li><a href="?link=140">REKAP PENDAFTARAN RAWAT JALAN</a></li>
            <li><a href="?link=142R">REKAP PENDAFTARAN RAWAT INAP</a></li>
            <li><a href="?link=14h" >HISTORI PASIEN</a></li>
        </ul>
    </li>
    <li><a href="#jamkesmasTool" data-toggle="collapse">TOOL <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="jamkesmasTool">
            <!--<li><a href="?link=14" >IMPORT FILE</a></li>-->
            <!--<li><a href="?link=153" >PENCARIAN</a></li>-->
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="15") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="?link=16">DATA DPMP</a></li>
    <li><a href="#giziLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="giziLaporan">
            <li><a href="?link=161" >REKAP DPMP</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="16") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="#eksekutifRawatJalan" data-toggle="collapse">RAWAT JALAN, UGD & VK <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="eksekutifRawatJalan">
            <li><a href="?link=private21">REKAP KUNJUNGAN PASIEN</a></li>
            <li><a href="?link=private22">REKAP KUNJUNGAN PER RUJUKAN</a></li>
            <li><a href="?link=private23">REKAP KUNJUNGAN PER CARA BAYAR</a></li>
            <li><a href="?link=private24">REKAP 10 PENYAKIT TERBANYAK</a></li>
            <li><a href="?link=private25">REKAP PENDAPATAN</a></li>
            <li><a href="?link=private27">REKAP PENDAPATAN PER CARABAYAR</a></li>
        </ul>
    </li>
    <li><a href="#eksekutifRawatInap" data-toggle="collapse">RAWAT INAP <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="eksekutifRawatInap">
            <li><a href="?link=private26">REKAP PASIEN</a></li>
            <li><a href="?link=private26_crbyr">REKAP PASIEN PER CARABAYAR</a></li>
        </ul>
    </li>
    <li><a href="#eksekutifLaboratorium" data-toggle="collapse">LABORATORIUM <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="eksekutifLaboratorium">
            <li><a href="?link=privatelab1">REKAP CARA BAYAR</a></li>
            <!--<li><a href="?link=privatelab2">JNS PERIKSA</a></li>-->
        </ul>
    </li>
    <li><a href="#eksekutifRadiologi" data-toggle="collapse">RADIOLOGI <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="eksekutifRadiologi">
            <li><a href="?link=privaterad1">REKAP CARA BAYAR</a></li>
            <!--<li><a href="?link=privaterad2">JNS PERIKSA</a></li>-->
        </ul>
    </li>
    <li><a href="#eksekutifKamarOperasi" data-toggle="collapse">KAMAR OPERASI <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="eksekutifKamarOperasi">
            <li><a href="?link=privatekam1">REKAP CARA BAYAR</a></li>
            <!--<li><a href="?link=privatekam2">JNS PEMBEDAHAN</a></li>-->
        </ul>
    </li>
    <li><a class="dir" href="?link=privategizi1">GIZI</a></li>
    <li><a class="dir" href="?link=privateapotek1">APOTEK</a></li>
    <li><a class="dir" href="?link=private27All">TOTAL SEMUA PENDAPATAN</a></li>
    	

</ul>

    <?php }elseif($_SESSION['ROLES']=="17") { ?>
	<script>
	function popUpWARNING(URL) {
		//day = new Date();
		//id = day.getTime();
		id	= 'popcuy';
		eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1000,height=400,left=50,top=50');");
	}
	jQuery(document).ready(function(){
								
		var auto_refresh = setInterval(function (){
			//popUpWARNING('daftar_pasien_harus_dipulangkan.php');
			jQuery.get('daftar_pasien_harus_dipulangkan.php',function(data){
				if(data){
					popUpWARNING('daftar_pasien_harus_dipulangkan.php');
				}
			});
		}, <?php echo _POPUPTIME_;?>);
	});
	</script>
<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="?link=17a">DAFTAR RAWAT INAP</a></li>
    <li><a href="?link=171">LIST PASIEN RAWAT INAP</a></li>
    <li><a href="?link=17f">LIST PASIEN RAWAT JALAN</a></li>
    <li><a href="?link=173">DATA KAMAR</a></li>
    <li><a class="dir" href="#">LAPORAN</a>
        <ul>
            <li><a href="?link=138" >SENSUS RAWAT INAP</a></li>
            <li><a href="?link=122x" >BUKU REGISTER RAWAT INAP</a></li>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
    <!--
    <li><a class="dir" href="#">FARMASI & LOGISTIK</a>
        <ul>
            <li><a href="index.php?link=f04" >PENGELUARAN</a></li>
            <li><a href="index.php?link=f06" >LIST PENGELUARAN</a></li>
            <li><a href="index.php?link=f01" >PERMINTAAN</a></li>
            <li><a href="index.php?link=f02" >LIST PERMINTAAN</a></li>
            <li><a href="index.php?link=f21" >PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f22" >LIST PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f07" >PERENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f08" >LIST PRENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f09" >LAPORAN BULANAN</a></li>
            <li><a href="index.php?link=f11" >LAPORAN HARIAN</a></li>
            <li><a href="index.php?link=f66" >LAPORAN STOK</a></li>
        </ul>
    </li>
    -->
    <li><a href="index.php?link=list_billing_ranap">LIST BILLING RANAP</a></li>
    <li><a class="dir" href="index.php?link=setting_dokter">SETTING DOKTER JAGA</a> 
		<ul>
            <li><a href="index.php?link=praktek_dokter" >PRAKTEK DOKTER</a></li>
		</ul>
    <?php #echo date('Y/m/d H:i:s',mktime(date('H')+6,date('i'),date('s'),date('m'),date('d'),date('Y')));?>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="19") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
	<li><a href="?link=20">LIST OPERASI</a></li>
    <li><a href="?link=205">LIST RENCANA OPERASI</a></li>
    
    <li><a href="#operasiLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="operasiLaporan">
        	<li><a href="?link=lapok" >Pasien OK</a></li>
            <!--<li><a href="?link=jas2" >JASPEL</a></li>-->
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
    <li><a href="#operasiFarmasi" data-toggle="collapse">FARMASI & LOGISTIK <span class="fa fa-caret-down pull-right"></span></a>
        <ul id="operasiFarmasi" class="collapse">
            <li><a href="index.php?link=f04" >PENGELUARAN</a></li>
            <li><a href="index.php?link=f06" >LIST PENGELUARAN</a></li>
            <li><a href="index.php?link=f01" >PERMINTAAN</a></li>
            <li><a href="index.php?link=f02" >LIST PERMINTAAN</a></li>
            <li><a href="index.php?link=f21" >PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f22" >LIST PENGEMBALIAN</a></li>
            <li><a href="index.php?link=f07" >PERENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f08" >LIST PRENCANAAN PENGADAAN</a></li>
            <li><a href="index.php?link=f09" >LAPORAN BULANAN</a></li>
            <li><a href="index.php?link=f11" >LAPORAN HARIAN</a></li>
            <li><a href="index.php?link=f66" >LAPORAN STOK</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="22") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="?link=As01">LIST PASIEN ASKES</a></li>
    <li><a class="dir" href="#">LAPORAN</a>
        <ul>
            <li><a href="?link=2f" >PENCARIAN DATA ASURANSI</a></li>
        </ul>
    </li>
</ul>

    <?php }elseif($_SESSION['ROLES']=="23") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
	<li><a href="#keuanganSetup" data-toggle="collapse">SETUP <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="keuanganSetup">
            <li><a href="?link=general_ledger" >General Ledger</a></li>
        </ul>    
    </li>
	<li><a href="#keuanganLaporan" data-toggle="collapse">LAPORAN <span class="fa fa-caret-down pull-right"></span></a>
        <ul class="collapse" id="keuanganLaporan">
            <li><a href="?link=laporan_hutang" >Laporan Hutang</a></li>
            <li><a href="?link=pendapatan_piutang" >Laporan Piutang</a></li>
            <li><a href="?link=general_ledger" >General Ledger</a></li>
        </ul>    
    </li>
	<!--
    <li><a href="#">SETUP</a>
        <ul>
            <li><a href="?link=24k1" >Neraca</a></li>
            <li><a href="?link=24k2" >Realisasi Anggaran</a></li>
            <li><a href="?link=24k3" >Arus Kas</a></li>
            <li><a href="?link=24k4" >Tarif</a></li>
        </ul>    
    </li>
    <li><a href="#">ENTRI</a>
     <ul>
            <li><a href="?link=31k1" >Neraca</a></li>
            <li><a href="?link=31k2" >Realisasi Anggaran</a></li>
            <li><a href="?link=31k3" >Arus Kas</a></li>    
     </ul>            
    </li>
    <li><a href="#">LAPORAN</a>
     <ul>
            <li><a href="?link=35k1" >Neraca</a></li>
            <li><a href="?link=35k2" >Realisasi Anggaran</a></li>
            <li><a href="?link=35k3" >Arus Kas</a></li>    
    </ul>            
    </li>
	-->
    <li><a href="#keuanganPendapatan" data-toggle="collapse">PENDAPATAN <span class="fa fa-caret-down pull-right"></span></a>
     <ul class="collapse" id="keuanganPendapatan">	<!-- <li><a href="?link=36k1" >Rekap Pendapatan per Unit</a></li>-->
     		
            <li><a href="?link=private27All" >Rekap Pendapatan per Unit</a></li>
            <li><a href="?link=36k2" >Rekap Pendapatan per Cara Bayar</a></li>
    </ul>     
    </li>  
      
</ul>

    <?php }elseif($_SESSION['ROLES']=="24") { ?>

<ul id="nav" class="dropdown dropdown-horizontal">
	<li><a href="index.php?link=jas0">SETTING JASPEL</a></li>
    <li><a href="index.php?link=jas1">RAWAT JALAN</a></li>
    <li><a href="index.php?link=jas2">KAMAR OPERASI</a></li>
    
    <!--
    <li><a href="index.php?link=jas3">UGD</a></li>
    <li><a href="index.php?link=jas9">VK</a></li>
    -->
    <li><a href="index.php?link=jas4">RAWAT INAP</a></li>
    <li><a href="index.php?link=jas5">LABORATORIUM</a></li>
    <li><a href="index.php?link=jas6">RADIOLOGI</a></li>
    <li><a href="index.php?link=jas10">REKAP JASPEL ALL</a></li>
    <!--
    <li><a href="index.php?link=jas7">MANAJEMEN</a></li>
    <li><a href="index.php?link=jas8">PENDUKUNG</a></li>
    -->
</ul>
	<?php } elseif($_SESSION['ROLES'] == '26'){ ?>
    <ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="index.php?link=adminrajal">DATA PASIEN RAWAT JALAN</a></li>
    <li><a href="index.php?link=adminrajal_aps">DATA PASIEN APS</a></li>
    <li><a href="index.php?link=adminrajal_daftar_aps">PENDAFTARAN PASIEN APS</a></li>
    
</ul>
    <?php } elseif($_SESSION['ROLES'] == '27'){ ?>
    <ul id="nav" class="dropdown dropdown-horizontal">
	<li><a href="index.php?link=list_kep">DATA PERAWAT</a></li>
	<li><a href="#keperawatanAsuhan" data-toggle="collapse">ASUHAN KEPERAWATAN <span class="fa fa-caret-down pull-right"></span></a>
     <ul class="collapse" id="keperawatanAsuhan">
            <li><a href="?link=askep__" >Pengkajian Keperawatan & Diagnosa Keperawatan</a></li>
            <!--<li><a href="?link=35k2" >Realisasi Anggaran</a></li>
            <li><a href="?link=35k3" >Arus Kas</a></li>    -->
    </ul>            
    </li>
    <li><a href="#keperawatanManajemen" data-toggle="collapse">MANAJEMEN PELAYANAN KEPERAWATAN <span class="fa fa-caret-down pull-right"></span></a>
     <ul class="collapse" id="keperawatanManajemen">
			<li><a href="?link=sdm_kep" >SDM Keperawatan</a></li>
            <li><a href="?link=met_gas" >Metode Penugasan</a></li>
			<li><a href="?link=supvis" >Supervisi</a></li>
			<li><a href="?link=lap_ranap_kep" >Laporan Rawat Inap</a></li>
    </ul>     
    </li>	
</ul>
    <?php } else { ?>
<ul id="nav" class="dropdown dropdown-horizontal">
    <li><a href="#">NO MENU FOUND.</a></li>
</ul>
    <?php } ?>



