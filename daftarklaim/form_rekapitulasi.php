<?php
include('../include/connect.php');
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_ri = 25;
$pageNum_ri = 0;
if (isset($_GET['pageNum_ri'])) {
  $pageNum_ri = $_GET['pageNum_ri'];
}
$startRow_ri = $pageNum_ri * $maxRows_ri;


$query_ri = "select * from t_jamkesmas_master_klaim where kategori='3C'";
$query_limit_ri = sprintf("%s LIMIT %d, %d", $query_ri, $startRow_ri, $maxRows_ri);
$ri = mysql_query($query_limit_ri) or die(mysql_error());
$row_ri = mysql_fetch_assoc($ri);

if (isset($_GET['totalRows_ri'])) {
  $totalRows_ri = $_GET['totalRows_ri'];
} else {
  $all_ri = mysql_query($query_ri);
  $totalRows_ri = mysql_num_rows($all_ri);
}
$totalPages_ri = ceil($totalRows_ri/$maxRows_ri)-1;

$queryString_ri = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_ri") == false && 
        stristr($param, "totalRows_ri") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_ri = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_ri = sprintf("&totalRows_ri=%d%s", $totalRows_ri, $queryString_ri);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div align="center">
    <div id="frame">
    <div id="frame_title"><h3>FORM REKAPITULASI KLAIM</h3></div>
<table border="0" cellpadding="2" cellspacing="2" width="90%">
  <tr>
    <th>Dari Tanggal</th>
    <th>Sampai Tanggal</th>
    <th>Nama Rumah Sakit</th>
    <th>Kode Rumah Sakit</th>
    <th>Kelas Rumah Sakit</th>
    <th>Verifikator</th>
    <th>Status</th>
    <th>&nbsp;</th>
  </tr>
  <?php do { ?>
    <tr>
      <td><?php echo $row_ri['dari']; ?></td>
      <td><?php echo $row_ri['sampai']; ?></td>
      <td><?php echo $row_ri['namars']; ?></td>
      <td><?php echo $row_ri['koders']; ?></td>
      <td><?php echo $row_ri['kelasrs']; ?></td>
      <td><?php echo $row_ri['verifikator']; ?></td>
      <td><?php echo $row_ri['kategori']; ?></td>
      <td><a href="index.php?link=150&dari=<?php echo $row_ri['dari'];?>&sampai=<?php echo $row_ri['sampai'];?>" class="text">LIHAT_DETAIL</a></td>
    </tr>
    <?php } while ($row_ri = mysql_fetch_assoc($ri)); ?>
</table>

<table border="0">
  <tr>
    <td><?php if ($pageNum_ri > 0) { // Show if not first page ?>
          <a href="<?php printf("%s?pageNum_ri=%d%s", $currentPage, 0, $queryString_ri); ?>">First</a>
          <?php } // Show if not first page ?>
    </td>
    <td><?php if ($pageNum_ri > 0) { // Show if not first page ?>
          <a href="<?php printf("%s?pageNum_ri=%d%s", $currentPage, max(0, $pageNum_ri - 1), $queryString_ri); ?>">Previous</a>
          <?php } // Show if not first page ?>
    </td>
    <td><?php if ($pageNum_ri < $totalPages_ri) { // Show if not last page ?>
          <a href="<?php printf("%s?pageNum_ri=%d%s", $currentPage, min($totalPages_ri, $pageNum_ri + 1), $queryString_ri); ?>">Next</a>
          <?php } // Show if not last page ?>
    </td>
    <td><?php if ($pageNum_ri < $totalPages_ri) { // Show if not last page ?>
          <a href="<?php printf("%s?pageNum_ri=%d%s", $currentPage, $totalPages_ri, $queryString_ri); ?>">Last</a>
          <?php } // Show if not last page ?>
    </td>
  </tr>
</table></div></div>
</body>
</html>
<?php
mysql_free_result($ri);
?>
