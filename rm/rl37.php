<style>
thead th, thead td{text-align:center;}
thead tr:last{border-bottom :1px solid #999;}
</style>
<div align="center">
<div id="frame">
    <div id="frame_title"><h3>Laporan RL 3.7</h3></div>


<table border="0" width="95%">
	<tr valign="top">
		<td align="center">
		
			<table cellpadding="0" class="tb" width="95%" cellspacing="0">
				<tr><td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td><td><h2>Formulir 3.7</h2></td><td rowspan="2"><div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div>
</td></tr>
				<tr><td><h1>KEGIATAN RADIOLOGI</h1></td></tr>
			</table>
		
			<table cellpadding="0" class="tb" width="95%" cellspacing="0">
				<tr><td> Kode RS </td><td>: <input type="text" name="kode_rs" class="inputrl12" /></td></tr>
                <tr><td> Nama RS </td><td>: <input type="text" name="nama_rs" class="inputrl12" /></td></tr>
                <tr><td> Tahun </td><td>: <input type="text" name="tahun" class="inputrl12" /></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <!--<tr><td colspan="2"><h2>RL 1.2 Indikator Pelayanan Rumah Sakit</h2></td></tr>-->
			</table>
		
			<table cellspacing="1" cellpadding="1" class="tb" width="95%">
			<thead>
				<tr><th>NO</th><th>JENIS KEGIATAN</th><th>JUMLAH</th></tr>
				<tr><td>1</td><td>2</td><td>3</td></tr>
			</thead>
			<tbody>
				<tr><td colspan="3">RADIODIAGNOSTIK</td></tr>
				<tr><td>1</td><td>Foto tanpa bahan kontras</td><td></td></tr>
				<tr><td>2</td><td>Foto dengan bahan kontras	</td><td></td></tr>
				<tr><td>3</td><td>Foto dengan rol film	</td><td></td></tr>
				<tr><td>4</td><td>Flouroskopi	</td><td></td></tr>
				<tr><td>5</td><td>Foto Gigi : </td><td></td></tr>
				<tr><td>6</td><td>C.T. Scan : </td><td></td></tr>
				<tr><td>7</td><td>Lymphografi</td><td></td></tr>
				<tr><td>8</td><td>Angiograpi</td><td></td></tr>
				<tr><td>9</td><td>Lain-Lain</td><td></td></tr>
				<tr><td colspan="3">RADIOTHERAPI</td></tr>
				<tr><td>1</td><td>Jumlah Kegiatan Radiotherapi</td><td></td></tr>
				<tr><td>2</td><td>Lain-Lain</td><td></td></tr>
				<tr><td colspan="3">KEDOKTERAN NUKLIR</td></tr>
				<tr><td>1</td><td>Jumlah Kegiatan Diagnostik</td><td></td></tr>
				<tr><td>2</td><td>Jumlah Kegiatan Therapi</td><td></td></tr>
				<tr><td>3</td><td>Lain-Lain</td><td></td></tr>
				<tr><td colspan="3">IMAGING / PENCITRAAN</td></tr>
				<tr><td>1</td><td>USG</td><td></td></tr>
				<tr><td>2</td><td>MRI</td><td></td></tr>
				<tr><td>3</td><td>Lain-Lain</td><td></td></tr>
				<tr><td>99</td><td>TOTAL</td><td></td></tr>
			</tbody>
			</table>
        </td>
    </tr>
</table>
</div>
</div>