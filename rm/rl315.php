<style>
thead th, thead td{text-align:center;}
thead tr:last{border-bottom :1px solid #999;}
</style>
<div align="center">
<div id="frame">
    <div id="frame_title"><h3>Laporan RL 3.15</h3></div>


<table border="0" width="95%">
	<tr valign="top">
		<td align="center">
		
			<table cellpadding="0" class="tb" width="95%" cellspacing="0">
				<tr><td rowspan="2" style="width:110px;"><img src="<?php echo _BASE_;?>/img/logobaktihusda.gif"></td><td><h2>Formulir 3.15</h2></td><td rowspan="2"><div style="border:1px dashed #999; padding:10px; display:block; font-style:italic; width:170px;">Ditjen Bina Upaya Kesehatan <br />Kementrian Kesehatan RI</div>
</td></tr>
				<tr><td><h1>CARA BAYAR</h1></td></tr>
			</table>
			

			<table cellpadding="0" class="tb" width="95%" cellspacing="0">
				<tr><td> Kode RS </td><td>: <input type="text" name="kode_rs" class="inputrl12" /></td></tr>
                <tr><td> Nama RS </td><td>: <input type="text" name="nama_rs" class="inputrl12" /></td></tr>
                <tr><td> Tahun </td><td>: <input type="text" name="tahun" class="inputrl12" /></td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <!--<tr><td colspan="2"><h2>3.13. Pengadaan Obat, Penulisan dan Pelayanan Resep</h2></td></tr>-->
			</table>

			<table cellspacing="1" cellpadding="1" class="tb" width="95%">
			<thead>
				<tr><th rowspan="2">NO</th><th rowspan="2">CARA PEMBAYARAN</th><th colspan="2">PASIEN RAWAT INAP</th><th rowspan="2">JUMLAH PASIEN RAWAT JALAN</th><th colspan="3">JUMLAH PASIEN RAWAT JALAN</th></tr>
				<tr><th>JUMLAH PASIEN KELUAR</th><th>JUMLAH LAMA DIRAWAT</th><th>LABORATORIUM</th><th>RADIOLOGI</th><th>LAIN LAIN</th></tr>
				<tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td></tr>
			</thead>
			<tbody>
				<tr><td>1</td><td>Membayar :</td><td colspan="6">&nbsp;</td></tr>
				<tr><td>1.1</td><td>Penuh</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
				<tr><td>1.2</td><td>Keringanan</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
				<tr><td>2</td><td>Asuransi :</td><td colspan="6">&nbsp;</td></tr>
				<tr><td>2.1</td><td>Asuransi Lain</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
				<tr><td>2.2</td><td>JAMKESMAS / JAMKESDA</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
				<tr><td>2.3</td><td>Kontrak</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
				<tr><td>3</td><td>Gratis :</td><td colspan="6">&nbsp;</td></tr>
				<tr><td>3.1</td><td>Kartu Sehat</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
				<tr><td>3.2</td><td>Keterangan Tidak Mampu</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
				<tr><td>3.3</td><td>Lain Lain</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
				<tr><td>99</td><td>TOTAL</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
			</tbody>
			</table>
        </td>
    </tr>
</table>
</div>
</div>