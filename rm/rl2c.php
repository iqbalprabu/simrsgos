<div align="center">
<div id="frame">
  <div id="frame_title"><h3>Laporan RL2c</h3></div>
  <table cellpadding="0" width="95%" class="tb" cellspacing="0">
    <col width="31" />
    <col width="64" />
    <col width="33" span="2" />
    <col width="37" span="6" />
    <col width="41" />
    <col width="37" />
    <col width="33" span="5" />
    <col width="48" />
    <col width="43" />
    <tr>
      <td colspan="19" width="717">DATA STATUS IMUNISASI</td>
    </tr>
    <tr>
      <td colspan="19">Bulan : ……………………….</td>
    </tr>
    <tr>
      <td colspan="19">Tahun : ………………………</td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td colspan="3">Formulir RL 2c</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td colspan="5" rowspan="2" width="717"><img width="168" height="24" src="../../../../Users/Administrator/AppData/Roaming/Adobe/Dreamweaver CS4/en_US/OfficeImageTemp/clip_image006.gif" /></td>
    </tr>
    <tr>
      <td colspan="7">Nama RS :    …………………………………..</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td colspan="2">Kode RS :</td>
    </tr>
  </table>
  <table cellspacing="1" cellpadding="1" class="tb">
  <tr>
    <th rowspan="2" width="717">No</th>
    <th rowspan="2" width="717">No Rekam Medis Pasien</th>
    <th colspan="2" width="717">Umur    Sex</th>
    <th colspan="8" width="717">PENYEBAB    SAKIT</th>
    <th colspan="5" width="717">STATUS    IMUNISASI **</th>
    <th colspan="2" width="717">Keadaan    Pasien Keluar RS</th>
  </tr>
  <tr>
    <th width="717">L</th>
    <th width="717">P</th>
    <th width="717">Dipteri</th>
    <th width="717">Pertusis </th>
    <th width="717">Tetanus</th>
    <th width="717">Tetanus Neonatorum</th>
    <th width="717">TBC    Paru</th>
    <th width="717">Campak</th>
    <th width="717">Polio</th>
    <th width="717">Hepatitis</th>
    <th width="717">0</th>
    <th width="717">1</th>
    <th width="717">2</th>
    <th width="717">3</th>
    <th width="717">TK</th>
    <th width="717">Hidup</th>
    <th width="717">Mati</th>
  </tr>
  <tr>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
    <td>9</td>
    <td>10</td>
    <td>&nbsp;</td>
    <td>11</td>
    <td>12</td>
    <td>13</td>
    <td>14</td>
    <td>15</td>
    <td>16</td>
    <td>17</td>
    <td>18</td>
  </tr>
  <tr <?php   echo "class =";
                $count++;
                if ($count % 2) {
                echo "tr1"; }
                else {
                echo "tr2";
                }
  ?>>    
     <td>1</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><strong>TOTAL</strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
  <table cellpadding="1" cellspacing="1" class="tb" width="95%">
    <tr>
      <td colspan="9">*    Diisi dengan umur sebenarnya dan dibulatkan ke bawah</td>
      <td></td>
      <td></td>
      <td></td>
      <td colspan="7">……………………………………………………</td>
    </tr>
    <tr>
      <td colspan="2">CONTOH    :</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="11">1,    Pasien laki-laki umur 11 tahun 6 bulan ditulis pada kolom 3 (L) : 11 Th</td>
      <td></td>
      <td colspan="7">Direktur Rumah Sakit :    RSUD. KOTA DEPOK</td>
    </tr>
    <tr>
      <td colspan="11">2.    Pasien perempuan umur 10 bulan 20 hari ditulis ada kolom 4 (P) : 10 bl</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="9">3.    Pasien laki-laki umur 10 hari ditulis pada kolom 3 (L) : 10 hr</td>
      <td></td>
      <td></td>
      <td></td>
      <td colspan="3">Tanda tangan :</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="5">**    O = Belum pernah di imunisasi</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="5">TK  = Tidak tahu / tanpa keterangan</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td colspan="3">Nama Terang :</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </table>
</div>
</div>