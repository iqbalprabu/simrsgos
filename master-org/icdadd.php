<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'icd', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "icdinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$icd->Export = @$_GET["export"]; // Get export parameter
$sExport = $icd->Export; // Get export parameter, used in header
$sExportFile = $icd->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["icd_code"] != "") {
  $icd->icd_code->setQueryStringValue($_GET["icd_code"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $icd->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $icd->CurrentAction = "C"; // Copy Record
  } else {
    $icd->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($icd->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($icd->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$icd->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($icd->KeyUrl($icd->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$icd->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_icd_code"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - icd code"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_jenis_penyakit"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - jenis penyakit"))
				return false;
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: icd<br><br><a href="<?php echo $icd->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="ficdadd" id="ficdadd" action="icdadd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">icd code<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $icd->icd_code->CellAttributes() ?>><span id="cb_x_icd_code">
<input type="text" name="x_icd_code" id="x_icd_code"  size="30" maxlength="8" value="<?php echo $icd->icd_code->EditValue ?>"<?php echo $icd->icd_code->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">jenis penyakit<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $icd->jenis_penyakit->CellAttributes() ?>><span id="cb_x_jenis_penyakit">
<textarea name="x_jenis_penyakit" id="x_jenis_penyakit" cols="35" rows="4"<?php echo $icd->jenis_penyakit->EditAttributes() ?>><?php echo $icd->jenis_penyakit->EditValue ?></textarea>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $icd;
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $icd;
	$icd->icd_code->setFormValue($objForm->GetValue("x_icd_code"));
	$icd->jenis_penyakit->setFormValue($objForm->GetValue("x_jenis_penyakit"));
}

// Restore form values
function RestoreFormValues() {
	global $icd;
	$icd->icd_code->CurrentValue = $icd->icd_code->FormValue;
	$icd->jenis_penyakit->CurrentValue = $icd->jenis_penyakit->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $icd;
	$sFilter = $icd->SqlKeyFilter();
	$sFilter = str_replace("@icd_code@", ew_AdjustSql($icd->icd_code->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$icd->Row_Selecting($sFilter);

	// Load sql based on filter
	$icd->CurrentFilter = $sFilter;
	$sSql = $icd->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$icd->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $icd;
	$icd->icd_code->setDbValue($rs->fields('icd_code'));
	$icd->jenis_penyakit->setDbValue($rs->fields('jenis_penyakit'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $icd;

	// Call Row Rendering event
	$icd->Row_Rendering();

	// Common render codes for all row types
	// icd_code

	$icd->icd_code->CellCssStyle = "";
	$icd->icd_code->CellCssClass = "";

	// jenis_penyakit
	$icd->jenis_penyakit->CellCssStyle = "";
	$icd->jenis_penyakit->CellCssClass = "";
	if ($icd->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($icd->RowType == EW_ROWTYPE_ADD) { // Add row

		// icd_code
		$icd->icd_code->EditCustomAttributes = "";
		$icd->icd_code->EditValue = ew_HtmlEncode($icd->icd_code->CurrentValue);

		// jenis_penyakit
		$icd->jenis_penyakit->EditCustomAttributes = "";
		$icd->jenis_penyakit->EditValue = ew_HtmlEncode($icd->jenis_penyakit->CurrentValue);
	} elseif ($icd->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($icd->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$icd->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $icd;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $icd->SqlKeyFilter();
	if (trim(strval($icd->icd_code->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@icd_code@", ew_AdjustSql($icd->icd_code->CurrentValue), $sFilter); // Replace key value
	}
	if ($bCheckKey) {
		$rsChk = $icd->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field icd_code
	$icd->icd_code->SetDbValueDef($icd->icd_code->CurrentValue, "");
	$rsnew['icd_code'] =& $icd->icd_code->DbValue;

	// Field jenis_penyakit
	$icd->jenis_penyakit->SetDbValueDef($icd->jenis_penyakit->CurrentValue, "");
	$rsnew['jenis_penyakit'] =& $icd->jenis_penyakit->DbValue;

	// Call Row Inserting event
	$bInsertRow = $icd->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($icd->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($icd->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $icd->CancelMessage;
			$icd->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {

		// Call Row Inserted event
		$icd->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
