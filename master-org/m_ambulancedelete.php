<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ambulance', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ambulanceinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ambulance->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ambulance->Export; // Get export parameter, used in header
$sExportFile = $m_ambulance->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["kode"] <> "") {
	$m_ambulance->kode->setQueryStringValue($_GET["kode"]);
	$sKey .= $m_ambulance->kode->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_ambulance->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	$sFilter .= "kode='" . ew_AdjustSql($sKeyFld) . "' AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_ambulance class, m_ambulanceinfo.php

$m_ambulance->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_ambulance->CurrentAction = $_POST["a_delete"];
} else {
	$m_ambulance->CurrentAction = "I"; // Display record
}
switch ($m_ambulance->CurrentAction) {
	case "D": // Delete
		$m_ambulance->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_ambulance->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_ambulance->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m ambulance<br><br><a href="<?php echo $m_ambulance->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_ambulancedelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">kode</td>
		<td valign="top">group jasa</td>
		<td valign="top">kode jasa</td>
		<td valign="top">tarif</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_ambulance->CssClass = "ewTableRow";
	$m_ambulance->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_ambulance->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_ambulance->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_ambulance->DisplayAttributes() ?>>
		<td<?php echo $m_ambulance->kode->CellAttributes() ?>>
<div<?php echo $m_ambulance->kode->ViewAttributes() ?>><?php echo $m_ambulance->kode->ViewValue ?></div>
</td>
		<td<?php echo $m_ambulance->group_jasa->CellAttributes() ?>>
<div<?php echo $m_ambulance->group_jasa->ViewAttributes() ?>><?php echo $m_ambulance->group_jasa->ViewValue ?></div>
</td>
		<td<?php echo $m_ambulance->kode_jasa->CellAttributes() ?>>
<div<?php echo $m_ambulance->kode_jasa->ViewAttributes() ?>><?php echo $m_ambulance->kode_jasa->ViewValue ?></div>
</td>
		<td<?php echo $m_ambulance->tarif->CellAttributes() ?>>
<div<?php echo $m_ambulance->tarif->ViewAttributes() ?>><?php echo $m_ambulance->tarif->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_ambulance;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_ambulance->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_ambulance class, m_ambulanceinfo.php

	$m_ambulance->CurrentFilter = $sWrkFilter;
	$sSql = $m_ambulance->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_ambulance->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['kode'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_ambulance->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_ambulance->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_ambulance->CancelMessage;
			$m_ambulance->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_ambulance->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_ambulance;

	// Call Recordset Selecting event
	$m_ambulance->Recordset_Selecting($m_ambulance->CurrentFilter);

	// Load list page sql
	$sSql = $m_ambulance->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_ambulance->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ambulance;
	$sFilter = $m_ambulance->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_ambulance->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ambulance->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ambulance->CurrentFilter = $sFilter;
	$sSql = $m_ambulance->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ambulance->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ambulance;
	$m_ambulance->kode->setDbValue($rs->fields('kode'));
	$m_ambulance->group_jasa->setDbValue($rs->fields('group_jasa'));
	$m_ambulance->kode_jasa->setDbValue($rs->fields('kode_jasa'));
	$m_ambulance->nama_jasa->setDbValue($rs->fields('nama_jasa'));
	$m_ambulance->jarak_tempuh->setDbValue($rs->fields('jarak_tempuh'));
	$m_ambulance->biaya->setDbValue($rs->fields('biaya'));
	$m_ambulance->tarif->setDbValue($rs->fields('tarif'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ambulance;

	// Call Row Rendering event
	$m_ambulance->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_ambulance->kode->CellCssStyle = "";
	$m_ambulance->kode->CellCssClass = "";

	// group_jasa
	$m_ambulance->group_jasa->CellCssStyle = "";
	$m_ambulance->group_jasa->CellCssClass = "";

	// kode_jasa
	$m_ambulance->kode_jasa->CellCssStyle = "";
	$m_ambulance->kode_jasa->CellCssClass = "";

	// tarif
	$m_ambulance->tarif->CellCssStyle = "";
	$m_ambulance->tarif->CellCssClass = "";
	if ($m_ambulance->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode
		$m_ambulance->kode->ViewValue = $m_ambulance->kode->CurrentValue;
		$m_ambulance->kode->CssStyle = "";
		$m_ambulance->kode->CssClass = "";
		$m_ambulance->kode->ViewCustomAttributes = "";

		// group_jasa
		$m_ambulance->group_jasa->ViewValue = $m_ambulance->group_jasa->CurrentValue;
		$m_ambulance->group_jasa->CssStyle = "";
		$m_ambulance->group_jasa->CssClass = "";
		$m_ambulance->group_jasa->ViewCustomAttributes = "";

		// kode_jasa
		$m_ambulance->kode_jasa->ViewValue = $m_ambulance->kode_jasa->CurrentValue;
		$m_ambulance->kode_jasa->CssStyle = "";
		$m_ambulance->kode_jasa->CssClass = "";
		$m_ambulance->kode_jasa->ViewCustomAttributes = "";

		// tarif
		$m_ambulance->tarif->ViewValue = $m_ambulance->tarif->CurrentValue;
		$m_ambulance->tarif->CssStyle = "";
		$m_ambulance->tarif->CssClass = "";
		$m_ambulance->tarif->ViewCustomAttributes = "";

		// kode
		$m_ambulance->kode->HrefValue = "";

		// group_jasa
		$m_ambulance->group_jasa->HrefValue = "";

		// kode_jasa
		$m_ambulance->kode_jasa->HrefValue = "";

		// tarif
		$m_ambulance->tarif->HrefValue = "";
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ambulance->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
