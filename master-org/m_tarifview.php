<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_tarif', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_tarifinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_tarif->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_tarif->Export; // Get export parameter, used in header
$sExportFile = $m_tarif->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["kode"] <> "") {
	$m_tarif->kode->setQueryStringValue($_GET["kode"]);
} else {
	Page_Terminate("m_tariflist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$m_tarif->CurrentAction = $_POST["a_view"];
} else {
	$m_tarif->CurrentAction = "I"; // Display form
}
switch ($m_tarif->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("m_tariflist.php"); // Return to list
		}
}

// Set return url
$m_tarif->setReturnUrl("m_tarifview.php");

// Render row
$m_tarif->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: m tarif
<br><br>
<a href="m_tariflist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_tarifadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_tarif->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_tarif->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_tarif->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode</td>
		<td<?php echo $m_tarif->kode->CellAttributes() ?>>
<div<?php echo $m_tarif->kode->ViewAttributes() ?>><?php echo $m_tarif->kode->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">group jasa</td>
		<td<?php echo $m_tarif->group_jasa->CellAttributes() ?>>
<div<?php echo $m_tarif->group_jasa->ViewAttributes() ?>><?php echo $m_tarif->group_jasa->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode jasa</td>
		<td<?php echo $m_tarif->kode_jasa->CellAttributes() ?>>
<div<?php echo $m_tarif->kode_jasa->ViewAttributes() ?>><?php echo $m_tarif->kode_jasa->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">nama jasa</td>
		<td<?php echo $m_tarif->nama_jasa->CellAttributes() ?>>
<div<?php echo $m_tarif->nama_jasa->ViewAttributes() ?>><?php echo $m_tarif->nama_jasa->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">tarif</td>
		<td<?php echo $m_tarif->tarif->CellAttributes() ?>>
<div<?php echo $m_tarif->tarif->ViewAttributes() ?>><?php echo $m_tarif->tarif->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">askes</td>
		<td<?php echo $m_tarif->askes->CellAttributes() ?>>
<div<?php echo $m_tarif->askes->ViewAttributes() ?>><?php echo $m_tarif->askes->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">cost sharing</td>
		<td<?php echo $m_tarif->cost_sharing->CellAttributes() ?>>
<div<?php echo $m_tarif->cost_sharing->ViewAttributes() ?>><?php echo $m_tarif->cost_sharing->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">jasa sarana</td>
		<td<?php echo $m_tarif->jasa_sarana->CellAttributes() ?>>
<div<?php echo $m_tarif->jasa_sarana->ViewAttributes() ?>><?php echo $m_tarif->jasa_sarana->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">jasa pelayanan</td>
		<td<?php echo $m_tarif->jasa_pelayanan->CellAttributes() ?>>
<div<?php echo $m_tarif->jasa_pelayanan->ViewAttributes() ?>><?php echo $m_tarif->jasa_pelayanan->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">jasa dokter</td>
		<td<?php echo $m_tarif->jasa_dokter->CellAttributes() ?>>
<div<?php echo $m_tarif->jasa_dokter->ViewAttributes() ?>><?php echo $m_tarif->jasa_dokter->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">tim rs</td>
		<td<?php echo $m_tarif->tim_rs->CellAttributes() ?>>
<div<?php echo $m_tarif->tim_rs->ViewAttributes() ?>><?php echo $m_tarif->tim_rs->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_tarif;
	$sFilter = $m_tarif->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_tarif->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_tarif->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_tarif->CurrentFilter = $sFilter;
	$sSql = $m_tarif->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_tarif->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_tarif;
	$m_tarif->kode->setDbValue($rs->fields('kode'));
	$m_tarif->group_jasa->setDbValue($rs->fields('group_jasa'));
	$m_tarif->kode_jasa->setDbValue($rs->fields('kode_jasa'));
	$m_tarif->nama_jasa->setDbValue($rs->fields('nama_jasa'));
	$m_tarif->tarif->setDbValue($rs->fields('tarif'));
	$m_tarif->askes->setDbValue($rs->fields('askes'));
	$m_tarif->cost_sharing->setDbValue($rs->fields('cost_sharing'));
	$m_tarif->jasa_sarana->setDbValue($rs->fields('jasa_sarana'));
	$m_tarif->jasa_pelayanan->setDbValue($rs->fields('jasa_pelayanan'));
	$m_tarif->jasa_dokter->setDbValue($rs->fields('jasa_dokter'));
	$m_tarif->tim_rs->setDbValue($rs->fields('tim_rs'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_tarif;

	// Call Row Rendering event
	$m_tarif->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_tarif->kode->CellCssStyle = "";
	$m_tarif->kode->CellCssClass = "";

	// group_jasa
	$m_tarif->group_jasa->CellCssStyle = "";
	$m_tarif->group_jasa->CellCssClass = "";

	// kode_jasa
	$m_tarif->kode_jasa->CellCssStyle = "";
	$m_tarif->kode_jasa->CellCssClass = "";

	// nama_jasa
	$m_tarif->nama_jasa->CellCssStyle = "";
	$m_tarif->nama_jasa->CellCssClass = "";

	// tarif
	$m_tarif->tarif->CellCssStyle = "";
	$m_tarif->tarif->CellCssClass = "";

	// askes
	$m_tarif->askes->CellCssStyle = "";
	$m_tarif->askes->CellCssClass = "";

	// cost_sharing
	$m_tarif->cost_sharing->CellCssStyle = "";
	$m_tarif->cost_sharing->CellCssClass = "";

	// jasa_sarana
	$m_tarif->jasa_sarana->CellCssStyle = "";
	$m_tarif->jasa_sarana->CellCssClass = "";

	// jasa_pelayanan
	$m_tarif->jasa_pelayanan->CellCssStyle = "";
	$m_tarif->jasa_pelayanan->CellCssClass = "";

	// jasa_dokter
	$m_tarif->jasa_dokter->CellCssStyle = "";
	$m_tarif->jasa_dokter->CellCssClass = "";

	// tim_rs
	$m_tarif->tim_rs->CellCssStyle = "";
	$m_tarif->tim_rs->CellCssClass = "";
	if ($m_tarif->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode
		$m_tarif->kode->ViewValue = $m_tarif->kode->CurrentValue;
		$m_tarif->kode->CssStyle = "";
		$m_tarif->kode->CssClass = "";
		$m_tarif->kode->ViewCustomAttributes = "";

		// group_jasa
		$m_tarif->group_jasa->ViewValue = $m_tarif->group_jasa->CurrentValue;
		$m_tarif->group_jasa->CssStyle = "";
		$m_tarif->group_jasa->CssClass = "";
		$m_tarif->group_jasa->ViewCustomAttributes = "";

		// kode_jasa
		$m_tarif->kode_jasa->ViewValue = $m_tarif->kode_jasa->CurrentValue;
		$m_tarif->kode_jasa->CssStyle = "";
		$m_tarif->kode_jasa->CssClass = "";
		$m_tarif->kode_jasa->ViewCustomAttributes = "";

		// nama_jasa
		$m_tarif->nama_jasa->ViewValue = $m_tarif->nama_jasa->CurrentValue;
		if (!is_null($m_tarif->nama_jasa->ViewValue)) $m_tarif->nama_jasa->ViewValue = str_replace("\n", "<br>", $m_tarif->nama_jasa->ViewValue); 
		$m_tarif->nama_jasa->CssStyle = "";
		$m_tarif->nama_jasa->CssClass = "";
		$m_tarif->nama_jasa->ViewCustomAttributes = "";

		// tarif
		$m_tarif->tarif->ViewValue = $m_tarif->tarif->CurrentValue;
		$m_tarif->tarif->CssStyle = "";
		$m_tarif->tarif->CssClass = "";
		$m_tarif->tarif->ViewCustomAttributes = "";

		// askes
		$m_tarif->askes->ViewValue = $m_tarif->askes->CurrentValue;
		$m_tarif->askes->CssStyle = "";
		$m_tarif->askes->CssClass = "";
		$m_tarif->askes->ViewCustomAttributes = "";

		// cost_sharing
		$m_tarif->cost_sharing->ViewValue = $m_tarif->cost_sharing->CurrentValue;
		$m_tarif->cost_sharing->CssStyle = "";
		$m_tarif->cost_sharing->CssClass = "";
		$m_tarif->cost_sharing->ViewCustomAttributes = "";

		// jasa_sarana
		$m_tarif->jasa_sarana->ViewValue = $m_tarif->jasa_sarana->CurrentValue;
		$m_tarif->jasa_sarana->CssStyle = "";
		$m_tarif->jasa_sarana->CssClass = "";
		$m_tarif->jasa_sarana->ViewCustomAttributes = "";

		// jasa_pelayanan
		$m_tarif->jasa_pelayanan->ViewValue = $m_tarif->jasa_pelayanan->CurrentValue;
		$m_tarif->jasa_pelayanan->CssStyle = "";
		$m_tarif->jasa_pelayanan->CssClass = "";
		$m_tarif->jasa_pelayanan->ViewCustomAttributes = "";

		// jasa_dokter
		$m_tarif->jasa_dokter->ViewValue = $m_tarif->jasa_dokter->CurrentValue;
		$m_tarif->jasa_dokter->CssStyle = "";
		$m_tarif->jasa_dokter->CssClass = "";
		$m_tarif->jasa_dokter->ViewCustomAttributes = "";

		// tim_rs
		$m_tarif->tim_rs->ViewValue = $m_tarif->tim_rs->CurrentValue;
		$m_tarif->tim_rs->CssStyle = "";
		$m_tarif->tim_rs->CssClass = "";
		$m_tarif->tim_rs->ViewCustomAttributes = "";

		// kode
		$m_tarif->kode->HrefValue = "";

		// group_jasa
		$m_tarif->group_jasa->HrefValue = "";

		// kode_jasa
		$m_tarif->kode_jasa->HrefValue = "";

		// nama_jasa
		$m_tarif->nama_jasa->HrefValue = "";

		// tarif
		$m_tarif->tarif->HrefValue = "";

		// askes
		$m_tarif->askes->HrefValue = "";

		// cost_sharing
		$m_tarif->cost_sharing->HrefValue = "";

		// jasa_sarana
		$m_tarif->jasa_sarana->HrefValue = "";

		// jasa_pelayanan
		$m_tarif->jasa_pelayanan->HrefValue = "";

		// jasa_dokter
		$m_tarif->jasa_dokter->HrefValue = "";

		// tim_rs
		$m_tarif->tim_rs->HrefValue = "";
	} elseif ($m_tarif->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_tarif->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_tarif->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_tarif->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_tarif;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_tarif->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_tarif->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_tarif->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_tarif->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_tarif->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_tarif->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_tarif->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
