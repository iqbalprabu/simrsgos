<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'icd_cm', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "icd_cminfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$icd_cm->Export = @$_GET["export"]; // Get export parameter
$sExport = $icd_cm->Export; // Get export parameter, used in header
$sExportFile = $icd_cm->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["kode"] != "") {
  $icd_cm->kode->setQueryStringValue($_GET["kode"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $icd_cm->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $icd_cm->CurrentAction = "C"; // Copy Record
  } else {
    $icd_cm->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($icd_cm->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($icd_cm->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$icd_cm->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($icd_cm->KeyUrl($icd_cm->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$icd_cm->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_kode"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kode"))
				return false;
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: icd cm<br><br><a href="<?php echo $icd_cm->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="ficd_cmadd" id="ficd_cmadd" action="icd_cmadd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">kode<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $icd_cm->kode->CellAttributes() ?>><span id="cb_x_kode">
<input type="text" name="x_kode" id="x_kode"  size="30" maxlength="8" value="<?php echo $icd_cm->kode->EditValue ?>"<?php echo $icd_cm->kode->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">keterangan</td>
    <td<?php echo $icd_cm->keterangan->CellAttributes() ?>><span id="cb_x_keterangan">
<textarea name="x_keterangan" id="x_keterangan" cols="35" rows="4"<?php echo $icd_cm->keterangan->EditAttributes() ?>><?php echo $icd_cm->keterangan->EditValue ?></textarea>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $icd_cm;
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $icd_cm;
	$icd_cm->kode->setFormValue($objForm->GetValue("x_kode"));
	$icd_cm->keterangan->setFormValue($objForm->GetValue("x_keterangan"));
}

// Restore form values
function RestoreFormValues() {
	global $icd_cm;
	$icd_cm->kode->CurrentValue = $icd_cm->kode->FormValue;
	$icd_cm->keterangan->CurrentValue = $icd_cm->keterangan->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $icd_cm;
	$sFilter = $icd_cm->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($icd_cm->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$icd_cm->Row_Selecting($sFilter);

	// Load sql based on filter
	$icd_cm->CurrentFilter = $sFilter;
	$sSql = $icd_cm->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$icd_cm->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $icd_cm;
	$icd_cm->kode->setDbValue($rs->fields('kode'));
	$icd_cm->keterangan->setDbValue($rs->fields('keterangan'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $icd_cm;

	// Call Row Rendering event
	$icd_cm->Row_Rendering();

	// Common render codes for all row types
	// kode

	$icd_cm->kode->CellCssStyle = "";
	$icd_cm->kode->CellCssClass = "";

	// keterangan
	$icd_cm->keterangan->CellCssStyle = "";
	$icd_cm->keterangan->CellCssClass = "";
	if ($icd_cm->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($icd_cm->RowType == EW_ROWTYPE_ADD) { // Add row

		// kode
		$icd_cm->kode->EditCustomAttributes = "";
		$icd_cm->kode->EditValue = ew_HtmlEncode($icd_cm->kode->CurrentValue);

		// keterangan
		$icd_cm->keterangan->EditCustomAttributes = "";
		$icd_cm->keterangan->EditValue = ew_HtmlEncode($icd_cm->keterangan->CurrentValue);
	} elseif ($icd_cm->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($icd_cm->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$icd_cm->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $icd_cm;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $icd_cm->SqlKeyFilter();
	if (trim(strval($icd_cm->kode->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@kode@", ew_AdjustSql($icd_cm->kode->CurrentValue), $sFilter); // Replace key value
	}
	if ($bCheckKey) {
		$rsChk = $icd_cm->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field kode
	$icd_cm->kode->SetDbValueDef($icd_cm->kode->CurrentValue, "");
	$rsnew['kode'] =& $icd_cm->kode->DbValue;

	// Field keterangan
	$icd_cm->keterangan->SetDbValueDef($icd_cm->keterangan->CurrentValue, NULL);
	$rsnew['keterangan'] =& $icd_cm->keterangan->DbValue;

	// Call Row Inserting event
	$bInsertRow = $icd_cm->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($icd_cm->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($icd_cm->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $icd_cm->CancelMessage;
			$icd_cm->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {

		// Call Row Inserted event
		$icd_cm->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
