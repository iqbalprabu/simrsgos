<?php

// PHPMaker 5 configuration for Table m_tarif
$m_tarif = new cm_tarif; // Initialize table object

// Define table class
class cm_tarif {

	// Define table level constants
	var $TableVar;
	var $TableName;
	var $SelectLimit = FALSE;
	var $kode;
	var $group_jasa;
	var $kode_jasa;
	var $nama_jasa;
	var $tarif;
	var $askes;
	var $cost_sharing;
	var $jasa_sarana;
	var $jasa_pelayanan;
	var $jasa_dokter;
	var $tim_rs;
	var $fields = array();

	function cm_tarif() {
		$this->TableVar = "m_tarif";
		$this->TableName = "m_tarif";
		$this->SelectLimit = TRUE;
		$this->kode = new cField('m_tarif', 'x_kode', 'kode', "kode", 200, -1, FALSE);
		$this->fields['kode'] =& $this->kode;
		$this->group_jasa = new cField('m_tarif', 'x_group_jasa', 'group_jasa', "group_jasa", 200, -1, FALSE);
		$this->fields['group_jasa'] =& $this->group_jasa;
		$this->kode_jasa = new cField('m_tarif', 'x_kode_jasa', 'kode_jasa', "kode_jasa", 200, -1, FALSE);
		$this->fields['kode_jasa'] =& $this->kode_jasa;
		$this->nama_jasa = new cField('m_tarif', 'x_nama_jasa', 'nama_jasa', "nama_jasa", 201, -1, FALSE);
		$this->fields['nama_jasa'] =& $this->nama_jasa;
		$this->tarif = new cField('m_tarif', 'x_tarif', 'tarif', "tarif", 3, -1, FALSE);
		$this->fields['tarif'] =& $this->tarif;
		$this->askes = new cField('m_tarif', 'x_askes', 'askes', "askes", 3, -1, FALSE);
		$this->fields['askes'] =& $this->askes;
		$this->cost_sharing = new cField('m_tarif', 'x_cost_sharing', 'cost_sharing', "cost_sharing", 3, -1, FALSE);
		$this->fields['cost_sharing'] =& $this->cost_sharing;
		$this->jasa_sarana = new cField('m_tarif', 'x_jasa_sarana', 'jasa_sarana', "jasa_sarana", 3, -1, FALSE);
		$this->fields['jasa_sarana'] =& $this->jasa_sarana;
		$this->jasa_pelayanan = new cField('m_tarif', 'x_jasa_pelayanan', 'jasa_pelayanan', "jasa_pelayanan", 3, -1, FALSE);
		$this->fields['jasa_pelayanan'] =& $this->jasa_pelayanan;
		$this->jasa_dokter = new cField('m_tarif', 'x_jasa_dokter', 'jasa_dokter', "jasa_dokter", 3, -1, FALSE);
		$this->fields['jasa_dokter'] =& $this->jasa_dokter;
		$this->tim_rs = new cField('m_tarif', 'x_tim_rs', 'tim_rs', "tim_rs", 3, -1, FALSE);
		$this->fields['tim_rs'] =& $this->tim_rs;
	}

	// Records per page
	function getRecordsPerPage() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_REC_PER_PAGE];
	}

	function setRecordsPerPage($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_REC_PER_PAGE] = $v;
	}

	// Start record number
	function getStartRecordNumber() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_START_REC];
	}

	function setStartRecordNumber($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_START_REC] = $v;
	}

	// Advanced search
	function getAdvancedSearch($fld) {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_ADVANCED_SEARCH . "_" . $fld];
	}

	function setAdvancedSearch($fld, $v) {
		if (@$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_ADVANCED_SEARCH . "_" . $fld] <> $v) {
			$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_ADVANCED_SEARCH . "_" . $fld] = $v;
		}
	}

	// Basic search Keyword
	function getBasicSearchKeyword() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_BASIC_SEARCH];
	}

	function setBasicSearchKeyword($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_BASIC_SEARCH] = $v;
	}

	// Basic Search Type
	function getBasicSearchType() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_BASIC_SEARCH_TYPE];
	}

	function setBasicSearchType($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_BASIC_SEARCH_TYPE] = $v;
	}

	// Search where clause
	function getSearchWhere() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_SEARCH_WHERE];
	}

	function setSearchWhere($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_SEARCH_WHERE] = $v;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Session WHERE Clause
	function getSessionWhere() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_WHERE];
	}

	function setSessionWhere($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_WHERE] = $v;
	}

	// Session ORDER BY
	function getSessionOrderBy() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_ORDER_BY];
	}

	function setSessionOrderBy($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_ORDER_BY] = $v;
	}

	// Session Key
	function getKey($fld) {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_KEY . "_" . $fld];
	}

	function setKey($fld, $v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_KEY . "_" . $fld] = $v;
	}

	// Table level SQL
	function SqlSelect() { // Select
		return "SELECT * FROM m_tarif";
	}

	function SqlWhere() { // Where
		return "";
	}

	function SqlGroupBy() { // Group By
		return "";
	}

	function SqlHaving() { // Having
		return "";
	}

	function SqlOrderBy() { // Order By
		return "";
	}

	// SQL variables
	var $CurrentFilter; // Current filter
	var $CurrentOrder; // Current order
	var $CurrentOrderType; // Current order type

	// Report table sql
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$sFilter, $sSort);
	}

	// Return table sql with list page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		if ($this->CurrentFilter <> "") {
			if ($sFilter <> "") $sFilter .= " AND ";
			$sFilter .= $this->CurrentFilter;
		}
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$sFilter, $sSort);
	}

	// Return record count
	function SelectRecordCount() {
		global $conn;
		$cnt = -1;
		$sFilter = $this->CurrentFilter;
		$this->Recordset_Selecting($this->CurrentFilter);
		if ($this->SelectLimit) {
			$sSelect = $this->SelectSQL();
			if (strtoupper(substr($sSelect, 0, 13)) == "SELECT * FROM") {
				$sSelect = "SELECT COUNT(*) FROM" . substr($sSelect, 13);
				if ($rs = $conn->Execute($sSelect)) {
					if (!$rs->EOF) $cnt = $rs->fields[0];
					$rs->Close();
				}
			}
		}
		if ($cnt == -1) {
			if ($rs = $conn->Execute($this->SelectSQL())) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $sFilter;
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= (is_null($value) ? "NULL" : ew_QuotedValue($value, $this->fields[$name]->FldDataType)) . ",";
		}
		if (substr($names, -1) == ",") $names = substr($names, 0, strlen($names)-1);
		if (substr($values, -1) == ",") $values = substr($values, 0, strlen($values)-1);
		return "INSERT INTO m_tarif ($names) VALUES ($values)";
	}

	// UPDATE statement
	function UpdateSQL(&$rs) {
		$SQL = "UPDATE m_tarif SET ";
		foreach ($rs as $name => $value) {
			$SQL .= $this->fields[$name]->FldExpression . "=" .
					(is_null($value) ? "NULL" : ew_QuotedValue($value, $this->fields[$name]->FldDataType)) . ",";
		}
		if (substr($SQL, -1) == ",") $SQL = substr($SQL, 0, strlen($SQL)-1);
		if ($this->CurrentFilter <> "")	$SQL .= " WHERE " . $this->CurrentFilter;
		return $SQL;
	}

	// DELETE statement
	function DeleteSQL(&$rs) {
		$SQL = "DELETE FROM m_tarif WHERE ";
		$SQL .= EW_DB_QUOTE_START . 'kode' . EW_DB_QUOTE_END . '=' .	ew_QuotedValue($rs['kode'], $this->kode->FldDataType) . ' AND ';
		if (substr($SQL, -5) == " AND ") $SQL = substr($SQL, 0, strlen($SQL)-5);
		if ($this->CurrentFilter <> "")	$SQL .= " AND " . $this->CurrentFilter;
		return $SQL;
	}

	// Key filter for table
	function SqlKeyFilter() {
		return "kode = '@kode@'";
	}

	// Return url
	function getReturnUrl() {
		if (@$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] <> "") {
			return $_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL];
		} else {
			return "m_tariflist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// View url
	function ViewUrl() {
		return $this->KeyUrl("m_tarifview.php");
	}

	// Edit url
	function EditUrl() {
		return $this->KeyUrl("m_tarifedit.php");
	}

	// Inline edit url
	function InlineEditUrl() {
		return $this->KeyUrl("m_tariflist.php", "a=edit");
	}

	// Copy url
	function CopyUrl() {
		return $this->KeyUrl("m_tarifadd.php");
	}

	// Inline copy url
	function InlineCopyUrl() {
		return $this->KeyUrl("m_tariflist.php", "a=copy");
	}

	// Delete url
	function DeleteUrl() {
		return $this->KeyUrl("m_tarifdelete.php");
	}

	// Key url
	function KeyUrl($url, $action = "") {
		$sUrl = $url . "?";
		if ($action <> "") $sUrl .= $action . "&";
		if (!is_null($this->kode->CurrentValue)) {
			$sUrl .= "kode=" . urlencode($this->kode->CurrentValue);
		} else {
			return "javascript:alert('Invalid Record! Key is null');";
		}
		return $sUrl;
	}

	// Function LoadRs
	// - Load Row based on Key Value
	function LoadRs($sFilter) {
		global $conn;

		// Set up filter (Sql Where Clause) and get Return Sql
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		return $conn->Execute($sSql);
	}

	// Load row values from rs
	function LoadListRowValues(&$rs) {
		$this->kode->setDbValue($rs->fields('kode'));
		$this->group_jasa->setDbValue($rs->fields('group_jasa'));
		$this->kode_jasa->setDbValue($rs->fields('kode_jasa'));
		$this->nama_jasa->setDbValue($rs->fields('nama_jasa'));
		$this->tarif->setDbValue($rs->fields('tarif'));
		$this->askes->setDbValue($rs->fields('askes'));
		$this->cost_sharing->setDbValue($rs->fields('cost_sharing'));
		$this->jasa_sarana->setDbValue($rs->fields('jasa_sarana'));
		$this->jasa_pelayanan->setDbValue($rs->fields('jasa_pelayanan'));
		$this->jasa_dokter->setDbValue($rs->fields('jasa_dokter'));
		$this->tim_rs->setDbValue($rs->fields('tim_rs'));
	}

	// Render list row values
	function RenderListRow() {
		global $conn, $Security;

		// kode
		$this->kode->ViewValue = $this->kode->CurrentValue;
		$this->kode->CssStyle = "";
		$this->kode->CssClass = "";
		$this->kode->ViewCustomAttributes = "";

		// group_jasa
		$this->group_jasa->ViewValue = $this->group_jasa->CurrentValue;
		$this->group_jasa->CssStyle = "";
		$this->group_jasa->CssClass = "";
		$this->group_jasa->ViewCustomAttributes = "";

		// kode_jasa
		$this->kode_jasa->ViewValue = $this->kode_jasa->CurrentValue;
		$this->kode_jasa->CssStyle = "";
		$this->kode_jasa->CssClass = "";
		$this->kode_jasa->ViewCustomAttributes = "";

		// tarif
		$this->tarif->ViewValue = $this->tarif->CurrentValue;
		$this->tarif->CssStyle = "";
		$this->tarif->CssClass = "";
		$this->tarif->ViewCustomAttributes = "";

		// askes
		$this->askes->ViewValue = $this->askes->CurrentValue;
		$this->askes->CssStyle = "";
		$this->askes->CssClass = "";
		$this->askes->ViewCustomAttributes = "";

		// cost_sharing
		$this->cost_sharing->ViewValue = $this->cost_sharing->CurrentValue;
		$this->cost_sharing->CssStyle = "";
		$this->cost_sharing->CssClass = "";
		$this->cost_sharing->ViewCustomAttributes = "";

		// jasa_sarana
		$this->jasa_sarana->ViewValue = $this->jasa_sarana->CurrentValue;
		$this->jasa_sarana->CssStyle = "";
		$this->jasa_sarana->CssClass = "";
		$this->jasa_sarana->ViewCustomAttributes = "";

		// jasa_pelayanan
		$this->jasa_pelayanan->ViewValue = $this->jasa_pelayanan->CurrentValue;
		$this->jasa_pelayanan->CssStyle = "";
		$this->jasa_pelayanan->CssClass = "";
		$this->jasa_pelayanan->ViewCustomAttributes = "";

		// jasa_dokter
		$this->jasa_dokter->ViewValue = $this->jasa_dokter->CurrentValue;
		$this->jasa_dokter->CssStyle = "";
		$this->jasa_dokter->CssClass = "";
		$this->jasa_dokter->ViewCustomAttributes = "";

		// tim_rs
		$this->tim_rs->ViewValue = $this->tim_rs->CurrentValue;
		$this->tim_rs->CssStyle = "";
		$this->tim_rs->CssClass = "";
		$this->tim_rs->ViewCustomAttributes = "";

		// kode
		$this->kode->HrefValue = "";

		// group_jasa
		$this->group_jasa->HrefValue = "";

		// kode_jasa
		$this->kode_jasa->HrefValue = "";

		// tarif
		$this->tarif->HrefValue = "";

		// askes
		$this->askes->HrefValue = "";

		// cost_sharing
		$this->cost_sharing->HrefValue = "";

		// jasa_sarana
		$this->jasa_sarana->HrefValue = "";

		// jasa_pelayanan
		$this->jasa_pelayanan->HrefValue = "";

		// jasa_dokter
		$this->jasa_dokter->HrefValue = "";

		// tim_rs
		$this->tim_rs->HrefValue = "";
	}
	var $CurrentAction; // Current action
	var $EventName; // Event name
	var $EventCancelled; // Event cancelled
	var $CancelMessage; // Cancel message
	var $RowType; // Row Type
	var $CssClass; // Css class
	var $CssStyle; // Css style
	var $RowClientEvents; // Row client events

	// Display Attribute
	function DisplayAttributes() {
		$sAtt = "";
		if (trim($this->CssStyle) <> "") {
			$sAtt .= " style=\"" . trim($this->CssStyle) . "\"";
		}
		if (trim($this->CssClass) <> "") {
			$sAtt .= " class=\"" . trim($this->CssClass) . "\"";
		}
		if ($this->Export == "") {
			if (trim($this->RowClientEvents) <> "") {
				$sAtt .= " " . $this->RowClientEvents;
			}
		}
		return $sAtt;
	}

	// Export
	var $Export;

//	 ----------------
//	  Field objects
//	 ----------------
	function fields($fldname) {
		return $this->fields[$fldname];
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// Row Inserting event
	function Row_Inserting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted(&$rs) {

		//echo "Row Inserted";
	}

	// Row Updating event
	function Row_Updating(&$rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Updated event
	function Row_Updated(&$rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Deleting event
	function Row_Deleting($rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}
}
?>
