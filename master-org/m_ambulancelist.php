<?php
define("EW_PAGE_ID", "list", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ambulance', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ambulanceinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ambulance->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ambulance->Export; // Get export parameter, used in header
$sExportFile = $m_ambulance->TableVar; // Get export file, used in header
?>
<?php
?>
<?php

// Paging variables
$nStartRec = 0; // Start record index
$nStopRec = 0; // Stop record index
$nTotalRecs = 0; // Total number of records
$nDisplayRecs = 20;
$nRecRange = 10;
$nRecCount = 0; // Record count

// Search filters
$sSrchAdvanced = ""; // Advanced search filter
$sSrchBasic = ""; // Basic search filter
$sSrchWhere = ""; // Search where clause
$sFilter = "";

// Master/Detail
$sDbMasterFilter = ""; // Master filter
$sDbDetailFilter = ""; // Detail filter
$sSqlMaster = ""; // Sql for master record

// Handle reset command
ResetCmd();

// Get basic search criteria
$sSrchBasic = BasicSearchWhere();

// Build search criteria
if ($sSrchAdvanced <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchAdvanced . ")";
}
if ($sSrchBasic <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchBasic . ")";
}

// Save search criteria
if ($sSrchWhere <> "") {
	if ($sSrchBasic == "") ResetBasicSearchParms();
	$m_ambulance->setSearchWhere($sSrchWhere); // Save to Session
	$nStartRec = 1; // Reset start record counter
	$m_ambulance->setStartRecordNumber($nStartRec);
} else {
	RestoreSearchParms();
}

// Build filter
$sFilter = "";
if ($sDbDetailFilter <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sDbDetailFilter . ")";
}
if ($sSrchWhere <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sSrchWhere . ")";
}

// Set up filter in Session
$m_ambulance->setSessionWhere($sFilter);
$m_ambulance->CurrentFilter = "";

// Set Up Sorting Order
SetUpSortOrder();

// Set Return Url
$m_ambulance->setReturnUrl("m_ambulancelist.php");
?>
<?php include "header.php" ?>
<?php if ($m_ambulance->Export == "") { ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "list"; // Page id

//-->
</script>
<script type="text/javascript">
<!--
var firstrowoffset = 1; // First data row start at
var lastrowoffset = 0; // Last data row end at
var EW_LIST_TABLE_NAME = 'ewlistmain'; // Table name for list page
var rowclass = 'ewTableRow'; // Row class
var rowaltclass = 'ewTableAltRow'; // Row alternate class
var rowmoverclass = 'ewTableHighlightRow'; // Row mouse over class
var rowselectedclass = 'ewTableSelectRow'; // Row selected class
var roweditclass = 'ewTableEditRow'; // Row edit class

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<?php } ?>
<?php if ($m_ambulance->Export == "") { ?>
<?php } ?>
<?php

// Load recordset
$bExportAll = (defined("EW_EXPORT_ALL") && $m_ambulance->Export <> "");
$bSelectLimit = ($m_ambulance->Export == "" && $m_ambulance->SelectLimit);
if (!$bSelectLimit) $rs = LoadRecordset();
$nTotalRecs = ($bSelectLimit) ? $m_ambulance->SelectRecordCount() : $rs->RecordCount();
$nStartRec = 1;
if ($nDisplayRecs <= 0) $nDisplayRecs = $nTotalRecs; // Display all records
if (!$bExportAll) SetUpStartRec(); // Set up start record position
if ($bSelectLimit) $rs = LoadRecordset($nStartRec-1, $nDisplayRecs);
?>
<p><span class="phpmaker" style="white-space: nowrap;">TABLE: m ambulance
</span></p>
<?php if ($m_ambulance->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<form name="fm_ambulancelistsrch" id="fm_ambulancelistsrch" action="m_ambulancelist.php" >
<table class="ewBasicSearch">
	<tr>
		<td><span class="phpmaker">
			<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" size="20" value="<?php echo ew_HtmlEncode($m_ambulance->getBasicSearchKeyword()) ?>">
			<input type="Submit" name="Submit" id="Submit" value="Search (*)">&nbsp;
			<a href="m_ambulancelist.php?cmd=reset">Show all</a>&nbsp;
		</span></td>
	</tr>
	<tr>
	<td><span class="phpmaker"><input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="" <?php if ($m_ambulance->getBasicSearchType() == "") { ?>checked<?php } ?>>Exact phrase&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="AND" <?php if ($m_ambulance->getBasicSearchType() == "AND") { ?>checked<?php } ?>>All words&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="OR" <?php if ($m_ambulance->getBasicSearchType() == "OR") { ?>checked<?php } ?>>Any word</span></td>
	</tr>
</table>
</form>
<?php } ?>
<?php } ?>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form method="post" name="fm_ambulancelist" id="fm_ambulancelist">
<?php if ($m_ambulance->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_ambulanceadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php if ($nTotalRecs > 0) { ?>
<table id="ewlistmain" class="ewTable">
<?php
	$OptionCnt = 0;
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // view
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // edit
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // copy
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // delete
}
?>
	<!-- Table header -->
	<tr class="ewTableHeader">
		<td valign="top">
<?php if ($m_ambulance->Export <> "") { ?>
kode
<?php } else { ?>
	<a href="m_ambulancelist.php?order=<?php echo urlencode('kode') ?>&ordertype=<?php echo $m_ambulance->kode->ReverseSort() ?>">kode&nbsp;(*)<?php if ($m_ambulance->kode->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ambulance->kode->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_ambulance->Export <> "") { ?>
group jasa
<?php } else { ?>
	<a href="m_ambulancelist.php?order=<?php echo urlencode('group_jasa') ?>&ordertype=<?php echo $m_ambulance->group_jasa->ReverseSort() ?>">group jasa&nbsp;(*)<?php if ($m_ambulance->group_jasa->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ambulance->group_jasa->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_ambulance->Export <> "") { ?>
kode jasa
<?php } else { ?>
	<a href="m_ambulancelist.php?order=<?php echo urlencode('kode_jasa') ?>&ordertype=<?php echo $m_ambulance->kode_jasa->ReverseSort() ?>">kode jasa&nbsp;(*)<?php if ($m_ambulance->kode_jasa->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ambulance->kode_jasa->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_ambulance->Export <> "") { ?>
tarif
<?php } else { ?>
	<a href="m_ambulancelist.php?order=<?php echo urlencode('tarif') ?>&ordertype=<?php echo $m_ambulance->tarif->ReverseSort() ?>">tarif<?php if ($m_ambulance->tarif->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_ambulance->tarif->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
<?php if ($m_ambulance->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php } ?>
	</tr>
<?php
if (defined("EW_EXPORT_ALL") && $m_ambulance->Export <> "") {
	$nStopRec = $nTotalRecs;
} else {
	$nStopRec = $nStartRec + $nDisplayRecs - 1; // Set the last record to display
}
$nRecCount = $nStartRec - 1;
if (!$rs->EOF) {
	$rs->MoveFirst();
	if (!$m_ambulance->SelectLimit) $rs->Move($nStartRec - 1); // Move to first record directly
}
$RowCnt = 0;
while (!$rs->EOF && $nRecCount < $nStopRec) {
	$nRecCount++;
	if (intval($nRecCount) >= intval($nStartRec)) {
		$RowCnt++;

	// Init row class and style
	$m_ambulance->CssClass = "ewTableRow";
	$m_ambulance->CssStyle = "";

	// Init row event
	$m_ambulance->RowClientEvents = "onmouseover='ew_MouseOver(this);' onmouseout='ew_MouseOut(this);' onclick='ew_Click(this);'";

	// Display alternate color for rows
	if ($RowCnt % 2 == 0) {
		$m_ambulance->CssClass = "ewTableAltRow";
	}
	LoadRowValues($rs); // Load row values
	$m_ambulance->RowType = EW_ROWTYPE_VIEW; // Render view
	RenderRow();
?>
	<!-- Table body -->
	<tr<?php echo $m_ambulance->DisplayAttributes() ?>>
		<!-- kode -->
		<td<?php echo $m_ambulance->kode->CellAttributes() ?>>
<div<?php echo $m_ambulance->kode->ViewAttributes() ?>><?php echo $m_ambulance->kode->ViewValue ?></div>
</td>
		<!-- group_jasa -->
		<td<?php echo $m_ambulance->group_jasa->CellAttributes() ?>>
<div<?php echo $m_ambulance->group_jasa->ViewAttributes() ?>><?php echo $m_ambulance->group_jasa->ViewValue ?></div>
</td>
		<!-- kode_jasa -->
		<td<?php echo $m_ambulance->kode_jasa->CellAttributes() ?>>
<div<?php echo $m_ambulance->kode_jasa->ViewAttributes() ?>><?php echo $m_ambulance->kode_jasa->ViewValue ?></div>
</td>
		<!-- tarif -->
		<td<?php echo $m_ambulance->tarif->CellAttributes() ?>>
<div<?php echo $m_ambulance->tarif->ViewAttributes() ?>><?php echo $m_ambulance->tarif->ViewValue ?></div>
</td>
<?php if ($m_ambulance->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_ambulance->ViewUrl() ?>">View</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_ambulance->EditUrl() ?>">Edit</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_ambulance->CopyUrl() ?>">Copy</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_ambulance->DeleteUrl() ?>">Delete</a>
</span></td>
<?php } ?>
<?php } ?>
	</tr>
<?php
	}
	$rs->MoveNext();
}
?>
</table>
<?php if ($m_ambulance->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_ambulanceadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php } ?>
</form>
<?php

// Close recordset and connection
if ($rs) $rs->Close();
?>
<?php if ($m_ambulance->Export == "") { ?>
<form action="m_ambulancelist.php" name="ewpagerform" id="ewpagerform">
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td nowrap>
<?php if (!isset($Pager)) $Pager = new cPrevNextPager($nStartRec, $nDisplayRecs, $nTotalRecs) ?>
<?php if ($Pager->RecordCount > 0) { ?>
	<table border="0" cellspacing="0" cellpadding="0"><tr><td><span class="phpmaker">Page&nbsp;</span></td>
<!--first page button-->
	<?php if ($Pager->FirstButton->Enabled) { ?>
	<td><a href="m_ambulancelist.php?start=<?php echo $Pager->FirstButton->Start ?>"><img src="images/first.gif" alt="First" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/firstdisab.gif" alt="First" width="16" height="16" border="0"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($Pager->PrevButton->Enabled) { ?>
	<td><a href="m_ambulancelist.php?start=<?php echo $Pager->PrevButton->Start ?>"><img src="images/prev.gif" alt="Previous" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/prevdisab.gif" alt="Previous" width="16" height="16" border="0"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($Pager->NextButton->Enabled) { ?>
	<td><a href="m_ambulancelist.php?start=<?php echo $Pager->NextButton->Start ?>"><img src="images/next.gif" alt="Next" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/nextdisab.gif" alt="Next" width="16" height="16" border="0"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($Pager->LastButton->Enabled) { ?>
	<td><a href="m_ambulancelist.php?start=<?php echo $Pager->LastButton->Start ?>"><img src="images/last.gif" alt="Last" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/lastdisab.gif" alt="Last" width="16" height="16" border="0"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;of <?php echo $Pager->PageCount ?></span></td>
	</tr></table>
	<span class="phpmaker">Records <?php echo $Pager->FromIndex ?> to <?php echo $Pager->ToIndex ?> of <?php echo $Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($sSrchWhere == "0=101") { ?>
	<span class="phpmaker">Please enter search criteria</span>
	<?php } else { ?>
	<span class="phpmaker">No records found</span>
	<?php } ?>
<?php } ?>
		</td>
	</tr>
</table>
</form>
<?php } ?>
<?php if ($m_ambulance->Export == "") { ?>
<?php } ?>
<?php if ($m_ambulance->Export == "") { ?>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php } ?>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Return Basic Search sql
function BasicSearchSQL($Keyword) {
	$sKeyword = ew_AdjustSql($Keyword);
	$sql = "";
	$sql .= "kode LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "group_jasa LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "kode_jasa LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "nama_jasa LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "jarak_tempuh LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "biaya LIKE '%" . $sKeyword . "%' OR ";
	if (substr($sql, -4) == " OR ") $sql = substr($sql, 0, strlen($sql)-4);
	return $sql;
}

// Return Basic Search Where based on search keyword and type
function BasicSearchWhere() {
	global $Security, $m_ambulance;
	$sSearchStr = "";
	$sSearchKeyword = ew_StripSlashes(@$_GET[EW_TABLE_BASIC_SEARCH]);
	$sSearchType = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	if ($sSearchKeyword <> "") {
		$sSearch = trim($sSearchKeyword);
		if ($sSearchType <> "") {
			while (strpos($sSearch, "  ") !== FALSE)
				$sSearch = str_replace("  ", " ", $sSearch);
			$arKeyword = explode(" ", trim($sSearch));
			foreach ($arKeyword as $sKeyword) {
				if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
				$sSearchStr .= "(" . BasicSearchSQL($sKeyword) . ")";
			}
		} else {
			$sSearchStr = BasicSearchSQL($sSearch);
		}
	}
	if ($sSearchKeyword <> "") {
		$m_ambulance->setBasicSearchKeyword($sSearchKeyword);
		$m_ambulance->setBasicSearchType($sSearchType);
	}
	return $sSearchStr;
}

// Clear all search parameters
function ResetSearchParms() {

	// Clear search where
	global $m_ambulance;
	$sSrchWhere = "";
	$m_ambulance->setSearchWhere($sSrchWhere);

	// Clear basic search parameters
	ResetBasicSearchParms();
}

// Clear all basic search parameters
function ResetBasicSearchParms() {

	// Clear basic search parameters
	global $m_ambulance;
	$m_ambulance->setBasicSearchKeyword("");
	$m_ambulance->setBasicSearchType("");
}

// Restore all search parameters
function RestoreSearchParms() {
	global $sSrchWhere, $m_ambulance;
	$sSrchWhere = $m_ambulance->getSearchWhere();
}

// Set up Sort parameters based on Sort Links clicked
function SetUpSortOrder() {
	global $m_ambulance;

	// Check for an Order parameter
	if (@$_GET["order"] <> "") {
		$m_ambulance->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
		$m_ambulance->CurrentOrderType = @$_GET["ordertype"];

		// Field kode
		$m_ambulance->UpdateSort($m_ambulance->kode);

		// Field group_jasa
		$m_ambulance->UpdateSort($m_ambulance->group_jasa);

		// Field kode_jasa
		$m_ambulance->UpdateSort($m_ambulance->kode_jasa);

		// Field tarif
		$m_ambulance->UpdateSort($m_ambulance->tarif);
		$m_ambulance->setStartRecordNumber(1); // Reset start position
	}
	$sOrderBy = $m_ambulance->getSessionOrderBy(); // Get order by from Session
	if ($sOrderBy == "") {
		if ($m_ambulance->SqlOrderBy() <> "") {
			$sOrderBy = $m_ambulance->SqlOrderBy();
			$m_ambulance->setSessionOrderBy($sOrderBy);
		}
	}
}

// Reset command based on querystring parameter cmd=
// - RESET: reset search parameters
// - RESETALL: reset search & master/detail parameters
// - RESETSORT: reset sort parameters
function ResetCmd() {
	global $sDbMasterFilter, $sDbDetailFilter, $nStartRec, $sOrderBy;
	global $m_ambulance;

	// Get reset cmd
	if (@$_GET["cmd"] <> "") {
		$sCmd = $_GET["cmd"];

		// Reset search criteria
		if (strtolower($sCmd) == "reset" || strtolower($sCmd) == "resetall") {
			ResetSearchParms();
		}

		// Reset Sort Criteria
		if (strtolower($sCmd) == "resetsort") {
			$sOrderBy = "";
			$m_ambulance->setSessionOrderBy($sOrderBy);
			$m_ambulance->kode->setSort("");
			$m_ambulance->group_jasa->setSort("");
			$m_ambulance->kode_jasa->setSort("");
			$m_ambulance->tarif->setSort("");
		}

		// Reset start position
		$nStartRec = 1;
		$m_ambulance->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_ambulance;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_ambulance->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_ambulance->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_ambulance->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_ambulance->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_ambulance->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_ambulance->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_ambulance->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_ambulance;

	// Call Recordset Selecting event
	$m_ambulance->Recordset_Selecting($m_ambulance->CurrentFilter);

	// Load list page sql
	$sSql = $m_ambulance->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_ambulance->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ambulance;
	$sFilter = $m_ambulance->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_ambulance->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ambulance->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ambulance->CurrentFilter = $sFilter;
	$sSql = $m_ambulance->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ambulance->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ambulance;
	$m_ambulance->kode->setDbValue($rs->fields('kode'));
	$m_ambulance->group_jasa->setDbValue($rs->fields('group_jasa'));
	$m_ambulance->kode_jasa->setDbValue($rs->fields('kode_jasa'));
	$m_ambulance->nama_jasa->setDbValue($rs->fields('nama_jasa'));
	$m_ambulance->jarak_tempuh->setDbValue($rs->fields('jarak_tempuh'));
	$m_ambulance->biaya->setDbValue($rs->fields('biaya'));
	$m_ambulance->tarif->setDbValue($rs->fields('tarif'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ambulance;

	// Call Row Rendering event
	$m_ambulance->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_ambulance->kode->CellCssStyle = "";
	$m_ambulance->kode->CellCssClass = "";

	// group_jasa
	$m_ambulance->group_jasa->CellCssStyle = "";
	$m_ambulance->group_jasa->CellCssClass = "";

	// kode_jasa
	$m_ambulance->kode_jasa->CellCssStyle = "";
	$m_ambulance->kode_jasa->CellCssClass = "";

	// tarif
	$m_ambulance->tarif->CellCssStyle = "";
	$m_ambulance->tarif->CellCssClass = "";
	if ($m_ambulance->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode
		$m_ambulance->kode->ViewValue = $m_ambulance->kode->CurrentValue;
		$m_ambulance->kode->CssStyle = "";
		$m_ambulance->kode->CssClass = "";
		$m_ambulance->kode->ViewCustomAttributes = "";

		// group_jasa
		$m_ambulance->group_jasa->ViewValue = $m_ambulance->group_jasa->CurrentValue;
		$m_ambulance->group_jasa->CssStyle = "";
		$m_ambulance->group_jasa->CssClass = "";
		$m_ambulance->group_jasa->ViewCustomAttributes = "";

		// kode_jasa
		$m_ambulance->kode_jasa->ViewValue = $m_ambulance->kode_jasa->CurrentValue;
		$m_ambulance->kode_jasa->CssStyle = "";
		$m_ambulance->kode_jasa->CssClass = "";
		$m_ambulance->kode_jasa->ViewCustomAttributes = "";

		// tarif
		$m_ambulance->tarif->ViewValue = $m_ambulance->tarif->CurrentValue;
		$m_ambulance->tarif->CssStyle = "";
		$m_ambulance->tarif->CssClass = "";
		$m_ambulance->tarif->ViewCustomAttributes = "";

		// kode
		$m_ambulance->kode->HrefValue = "";

		// group_jasa
		$m_ambulance->group_jasa->HrefValue = "";

		// kode_jasa
		$m_ambulance->kode_jasa->HrefValue = "";

		// tarif
		$m_ambulance->tarif->HrefValue = "";
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ambulance->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ambulance->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
