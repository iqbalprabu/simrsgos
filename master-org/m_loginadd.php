<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_login', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_logininfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_login->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_login->Export; // Get export parameter, used in header
$sExportFile = $m_login->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["NIP"] != "") {
  $m_login->NIP->setQueryStringValue($_GET["NIP"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $m_login->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $m_login->CurrentAction = "C"; // Copy Record
  } else {
    $m_login->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($m_login->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($m_login->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$m_login->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($m_login->KeyUrl($m_login->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$m_login->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_NIP"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - NIP"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_PWD"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - PWD"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_SES_REG"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - SES REG"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_ROLES"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - ROLES"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_ROLES"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - ROLES"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_KDUNIT"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - KDUNIT"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_KDUNIT"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - KDUNIT"))
				return false; 
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: m login<br><br><a href="<?php echo $m_login->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="fm_loginadd" id="fm_loginadd" action="m_loginadd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">NIP<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_login->NIP->CellAttributes() ?>><span id="cb_x_NIP">
<input type="text" name="x_NIP" id="x_NIP"  size="30" maxlength="50" value="<?php echo $m_login->NIP->EditValue ?>"<?php echo $m_login->NIP->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">PWD<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_login->PWD->CellAttributes() ?>><span id="cb_x_PWD">
<input type="text" name="x_PWD" id="x_PWD"  size="30" maxlength="32" value="<?php echo $m_login->PWD->EditValue ?>"<?php echo $m_login->PWD->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">SES REG<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_login->SES_REG->CellAttributes() ?>><span id="cb_x_SES_REG">
<input type="text" name="x_SES_REG" id="x_SES_REG"  size="30" maxlength="32" readonly="readonly" value="<?php echo md5($m_login->PWD->EditValue); ?>"<?php echo $m_login->SES_REG->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">ROLES<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_login->ROLES->CellAttributes() ?>><span id="cb_x_ROLES">
<input type="text" name="x_ROLES" id="x_ROLES"  size="30" value="<?php echo $m_login->ROLES->EditValue ?>"<?php echo $m_login->ROLES->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">KDUNIT<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_login->KDUNIT->CellAttributes() ?>><span id="cb_x_KDUNIT">
<input type="text" name="x_KDUNIT" id="x_KDUNIT"  size="30" value="<?php echo $m_login->KDUNIT->EditValue ?>"<?php echo $m_login->KDUNIT->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">DEPARTEMEN</td>
    <td<?php echo $m_login->DEPARTEMEN->CellAttributes() ?>><span id="cb_x_DEPARTEMEN">
<input type="text" name="x_DEPARTEMEN" id="x_DEPARTEMEN"  size="30" maxlength="20" value="<?php echo $m_login->DEPARTEMEN->EditValue ?>"<?php echo $m_login->DEPARTEMEN->EditAttributes() ?>>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $m_login;
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_login;
	$m_login->NIP->setFormValue($objForm->GetValue("x_NIP"));
	$m_login->PWD->setFormValue($objForm->GetValue("x_PWD"));
	$m_login->SES_REG->setFormValue(md5($objForm->GetValue("x_PWD")));
	$m_login->ROLES->setFormValue($objForm->GetValue("x_ROLES"));
	$m_login->KDUNIT->setFormValue($objForm->GetValue("x_KDUNIT"));
	$m_login->DEPARTEMEN->setFormValue($objForm->GetValue("x_DEPARTEMEN"));
}

// Restore form values
function RestoreFormValues() {
	global $m_login;
	$m_login->NIP->CurrentValue = $m_login->NIP->FormValue;
	$m_login->PWD->CurrentValue = $m_login->PWD->FormValue;
	$m_login->SES_REG->CurrentValue = $m_login->SES_REG->FormValue;
	$m_login->ROLES->CurrentValue = $m_login->ROLES->FormValue;
	$m_login->KDUNIT->CurrentValue = $m_login->KDUNIT->FormValue;
	$m_login->DEPARTEMEN->CurrentValue = $m_login->DEPARTEMEN->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_login;
	$sFilter = $m_login->SqlKeyFilter();
	$sFilter = str_replace("@NIP@", ew_AdjustSql($m_login->NIP->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_login->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_login->CurrentFilter = $sFilter;
	$sSql = $m_login->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_login->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_login;
	$m_login->NIP->setDbValue($rs->fields('NIP'));
	$m_login->PWD->setDbValue($rs->fields('PWD'));
	$m_login->SES_REG->setDbValue($rs->fields('SES_REG'));
	$m_login->ROLES->setDbValue($rs->fields('ROLES'));
	$m_login->KDUNIT->setDbValue($rs->fields('KDUNIT'));
	$m_login->DEPARTEMEN->setDbValue($rs->fields('DEPARTEMEN'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_login;

	// Call Row Rendering event
	$m_login->Row_Rendering();

	// Common render codes for all row types
	// NIP

	$m_login->NIP->CellCssStyle = "";
	$m_login->NIP->CellCssClass = "";

	// PWD
	$m_login->PWD->CellCssStyle = "";
	$m_login->PWD->CellCssClass = "";

	// SES_REG
	$m_login->SES_REG->CellCssStyle = "";
	$m_login->SES_REG->CellCssClass = "";

	// ROLES
	$m_login->ROLES->CellCssStyle = "";
	$m_login->ROLES->CellCssClass = "";

	// KDUNIT
	$m_login->KDUNIT->CellCssStyle = "";
	$m_login->KDUNIT->CellCssClass = "";

	// DEPARTEMEN
	$m_login->DEPARTEMEN->CellCssStyle = "";
	$m_login->DEPARTEMEN->CellCssClass = "";
	if ($m_login->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_login->RowType == EW_ROWTYPE_ADD) { // Add row

		// NIP
		$m_login->NIP->EditCustomAttributes = "";
		$m_login->NIP->EditValue = ew_HtmlEncode($m_login->NIP->CurrentValue);

		// PWD
		$m_login->PWD->EditCustomAttributes = "";
		$m_login->PWD->EditValue = ew_HtmlEncode($m_login->PWD->CurrentValue);

		// SES_REG
		$m_login->SES_REG->EditCustomAttributes = "";
		$m_login->SES_REG->EditValue = ew_HtmlEncode($m_login->SES_REG->CurrentValue);

		// ROLES
		$m_login->ROLES->EditCustomAttributes = "";
		$m_login->ROLES->EditValue = ew_HtmlEncode($m_login->ROLES->CurrentValue);

		// KDUNIT
		$m_login->KDUNIT->EditCustomAttributes = "";
		$m_login->KDUNIT->EditValue = ew_HtmlEncode($m_login->KDUNIT->CurrentValue);

		// DEPARTEMEN
		$m_login->DEPARTEMEN->EditCustomAttributes = "";
		$m_login->DEPARTEMEN->EditValue = ew_HtmlEncode($m_login->DEPARTEMEN->CurrentValue);
	} elseif ($m_login->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_login->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_login->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $m_login;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $m_login->SqlKeyFilter();
	if (trim(strval($m_login->NIP->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@NIP@", ew_AdjustSql($m_login->NIP->CurrentValue), $sFilter); // Replace key value
	}
	if ($bCheckKey) {
		$rsChk = $m_login->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field NIP
	$m_login->NIP->SetDbValueDef($m_login->NIP->CurrentValue, "");
	$rsnew['NIP'] =& $m_login->NIP->DbValue;

	// Field PWD
	$m_login->PWD->SetDbValueDef($m_login->PWD->CurrentValue, "");
	$rsnew['PWD'] =& $m_login->PWD->DbValue;

	// Field SES_REG
	$m_login->SES_REG->SetDbValueDef($m_login->SES_REG->CurrentValue, "");
	$rsnew['SES_REG'] =& $m_login->SES_REG->DbValue;

	// Field ROLES
	$m_login->ROLES->SetDbValueDef($m_login->ROLES->CurrentValue, 0);
	$rsnew['ROLES'] =& $m_login->ROLES->DbValue;

	// Field KDUNIT
	$m_login->KDUNIT->SetDbValueDef($m_login->KDUNIT->CurrentValue, 0);
	$rsnew['KDUNIT'] =& $m_login->KDUNIT->DbValue;

	// Field DEPARTEMEN
	$m_login->DEPARTEMEN->SetDbValueDef($m_login->DEPARTEMEN->CurrentValue, NULL);
	$rsnew['DEPARTEMEN'] =& $m_login->DEPARTEMEN->DbValue;

	// Call Row Inserting event
	$bInsertRow = $m_login->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($m_login->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($m_login->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_login->CancelMessage;
			$m_login->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {

		// Call Row Inserted event
		$m_login->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
