<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ugd', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ugdinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ugd->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ugd->Export; // Get export parameter, used in header
$sExportFile = $m_ugd->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["kode_ugd"] != "") {
  $m_ugd->kode_ugd->setQueryStringValue($_GET["kode_ugd"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $m_ugd->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $m_ugd->CurrentAction = "C"; // Copy Record
  } else {
    $m_ugd->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($m_ugd->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($m_ugd->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$m_ugd->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($m_ugd->KeyUrl($m_ugd->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$m_ugd->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_kode_ugd"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kode ugd"))
				return false;
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: m ugd<br><br><a href="<?php echo $m_ugd->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="fm_ugdadd" id="fm_ugdadd" action="m_ugdadd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">kode ugd<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_ugd->kode_ugd->CellAttributes() ?>><span id="cb_x_kode_ugd">
<input type="text" name="x_kode_ugd" id="x_kode_ugd"  size="30" maxlength="20" value="<?php echo $m_ugd->kode_ugd->EditValue ?>"<?php echo $m_ugd->kode_ugd->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">kode group</td>
    <td<?php echo $m_ugd->kode_group->CellAttributes() ?>><span id="cb_x_kode_group">
<input type="text" name="x_kode_group" id="x_kode_group"  size="30" maxlength="3" value="<?php echo $m_ugd->kode_group->EditValue ?>"<?php echo $m_ugd->kode_group->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">group</td>
    <td<?php echo $m_ugd->group->CellAttributes() ?>><span id="cb_x_group">
<input type="text" name="x_group" id="x_group"  size="30" maxlength="50" value="<?php echo $m_ugd->group->EditValue ?>"<?php echo $m_ugd->group->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">pemeriksaan ugd</td>
    <td<?php echo $m_ugd->pemeriksaan_ugd->CellAttributes() ?>><span id="cb_x_pemeriksaan_ugd">
<input type="text" name="x_pemeriksaan_ugd" id="x_pemeriksaan_ugd"  size="30" maxlength="128" value="<?php echo $m_ugd->pemeriksaan_ugd->EditValue ?>"<?php echo $m_ugd->pemeriksaan_ugd->EditAttributes() ?>>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $m_ugd;
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_ugd;
	$m_ugd->kode_ugd->setFormValue($objForm->GetValue("x_kode_ugd"));
	$m_ugd->kode_group->setFormValue($objForm->GetValue("x_kode_group"));
	$m_ugd->group->setFormValue($objForm->GetValue("x_group"));
	$m_ugd->pemeriksaan_ugd->setFormValue($objForm->GetValue("x_pemeriksaan_ugd"));
}

// Restore form values
function RestoreFormValues() {
	global $m_ugd;
	$m_ugd->kode_ugd->CurrentValue = $m_ugd->kode_ugd->FormValue;
	$m_ugd->kode_group->CurrentValue = $m_ugd->kode_group->FormValue;
	$m_ugd->group->CurrentValue = $m_ugd->group->FormValue;
	$m_ugd->pemeriksaan_ugd->CurrentValue = $m_ugd->pemeriksaan_ugd->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ugd;
	$sFilter = $m_ugd->SqlKeyFilter();
	$sFilter = str_replace("@kode_ugd@", ew_AdjustSql($m_ugd->kode_ugd->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ugd->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ugd->CurrentFilter = $sFilter;
	$sSql = $m_ugd->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ugd->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ugd;
	$m_ugd->kode_ugd->setDbValue($rs->fields('kode_ugd'));
	$m_ugd->kode_group->setDbValue($rs->fields('kode_group'));
	$m_ugd->group->setDbValue($rs->fields('group'));
	$m_ugd->pemeriksaan_ugd->setDbValue($rs->fields('pemeriksaan_ugd'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ugd;

	// Call Row Rendering event
	$m_ugd->Row_Rendering();

	// Common render codes for all row types
	// kode_ugd

	$m_ugd->kode_ugd->CellCssStyle = "";
	$m_ugd->kode_ugd->CellCssClass = "";

	// kode_group
	$m_ugd->kode_group->CellCssStyle = "";
	$m_ugd->kode_group->CellCssClass = "";

	// group
	$m_ugd->group->CellCssStyle = "";
	$m_ugd->group->CellCssClass = "";

	// pemeriksaan_ugd
	$m_ugd->pemeriksaan_ugd->CellCssStyle = "";
	$m_ugd->pemeriksaan_ugd->CellCssClass = "";
	if ($m_ugd->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_ugd->RowType == EW_ROWTYPE_ADD) { // Add row

		// kode_ugd
		$m_ugd->kode_ugd->EditCustomAttributes = "";
		$m_ugd->kode_ugd->EditValue = ew_HtmlEncode($m_ugd->kode_ugd->CurrentValue);

		// kode_group
		$m_ugd->kode_group->EditCustomAttributes = "";
		$m_ugd->kode_group->EditValue = ew_HtmlEncode($m_ugd->kode_group->CurrentValue);

		// group
		$m_ugd->group->EditCustomAttributes = "";
		$m_ugd->group->EditValue = ew_HtmlEncode($m_ugd->group->CurrentValue);

		// pemeriksaan_ugd
		$m_ugd->pemeriksaan_ugd->EditCustomAttributes = "";
		$m_ugd->pemeriksaan_ugd->EditValue = ew_HtmlEncode($m_ugd->pemeriksaan_ugd->CurrentValue);
	} elseif ($m_ugd->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ugd->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ugd->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $m_ugd;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $m_ugd->SqlKeyFilter();
	if (trim(strval($m_ugd->kode_ugd->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@kode_ugd@", ew_AdjustSql($m_ugd->kode_ugd->CurrentValue), $sFilter); // Replace key value
	}
	if ($bCheckKey) {
		$rsChk = $m_ugd->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field kode_ugd
	$m_ugd->kode_ugd->SetDbValueDef($m_ugd->kode_ugd->CurrentValue, "");
	$rsnew['kode_ugd'] =& $m_ugd->kode_ugd->DbValue;

	// Field kode_group
	$m_ugd->kode_group->SetDbValueDef($m_ugd->kode_group->CurrentValue, NULL);
	$rsnew['kode_group'] =& $m_ugd->kode_group->DbValue;

	// Field group
	$m_ugd->group->SetDbValueDef($m_ugd->group->CurrentValue, NULL);
	$rsnew['group'] =& $m_ugd->group->DbValue;

	// Field pemeriksaan_ugd
	$m_ugd->pemeriksaan_ugd->SetDbValueDef($m_ugd->pemeriksaan_ugd->CurrentValue, NULL);
	$rsnew['pemeriksaan_ugd'] =& $m_ugd->pemeriksaan_ugd->DbValue;

	// Call Row Inserting event
	$bInsertRow = $m_ugd->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($m_ugd->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($m_ugd->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_ugd->CancelMessage;
			$m_ugd->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {

		// Call Row Inserted event
		$m_ugd->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
