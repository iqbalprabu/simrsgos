<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_poly', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_polyinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_poly->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_poly->Export; // Get export parameter, used in header
$sExportFile = $m_poly->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["kode"] <> "") {
	$m_poly->kode->setQueryStringValue($_GET["kode"]);
	if (!is_numeric($m_poly->kode->QueryStringValue)) {
		Page_Terminate($m_poly->getReturnUrl()); // Prevent sql injection, exit
	}
	$sKey .= $m_poly->kode->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_poly->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	if (!is_numeric($sKeyFld)) {
		Page_Terminate($m_poly->getReturnUrl()); // Prevent sql injection, exit
	}
	$sFilter .= "kode=" . ew_AdjustSql($sKeyFld) . " AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_poly class, m_polyinfo.php

$m_poly->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_poly->CurrentAction = $_POST["a_delete"];
} else {
	$m_poly->CurrentAction = "I"; // Display record
}
switch ($m_poly->CurrentAction) {
	case "D": // Delete
		$m_poly->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_poly->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_poly->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m poly<br><br><a href="<?php echo $m_poly->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_polydelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">kode</td>
		<td valign="top">nama</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_poly->CssClass = "ewTableRow";
	$m_poly->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_poly->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_poly->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_poly->DisplayAttributes() ?>>
		<td<?php echo $m_poly->kode->CellAttributes() ?>>
<div<?php echo $m_poly->kode->ViewAttributes() ?>><?php echo $m_poly->kode->ViewValue ?></div>
</td>
		<td<?php echo $m_poly->nama->CellAttributes() ?>>
<div<?php echo $m_poly->nama->ViewAttributes() ?>><?php echo $m_poly->nama->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_poly;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_poly->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_poly class, m_polyinfo.php

	$m_poly->CurrentFilter = $sWrkFilter;
	$sSql = $m_poly->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_poly->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['kode'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_poly->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_poly->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_poly->CancelMessage;
			$m_poly->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_poly->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_poly;

	// Call Recordset Selecting event
	$m_poly->Recordset_Selecting($m_poly->CurrentFilter);

	// Load list page sql
	$sSql = $m_poly->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_poly->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_poly;
	$sFilter = $m_poly->SqlKeyFilter();
	if (!is_numeric($m_poly->kode->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_poly->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_poly->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_poly->CurrentFilter = $sFilter;
	$sSql = $m_poly->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_poly->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_poly;
	$m_poly->kode->setDbValue($rs->fields('kode'));
	$m_poly->nama->setDbValue($rs->fields('nama'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_poly;

	// Call Row Rendering event
	$m_poly->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_poly->kode->CellCssStyle = "";
	$m_poly->kode->CellCssClass = "";

	// nama
	$m_poly->nama->CellCssStyle = "";
	$m_poly->nama->CellCssClass = "";
	if ($m_poly->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode
		$m_poly->kode->ViewValue = $m_poly->kode->CurrentValue;
		$m_poly->kode->CssStyle = "";
		$m_poly->kode->CssClass = "";
		$m_poly->kode->ViewCustomAttributes = "";

		// nama
		$m_poly->nama->ViewValue = $m_poly->nama->CurrentValue;
		$m_poly->nama->CssStyle = "";
		$m_poly->nama->CssClass = "";
		$m_poly->nama->ViewCustomAttributes = "";

		// kode
		$m_poly->kode->HrefValue = "";

		// nama
		$m_poly->nama->HrefValue = "";
	} elseif ($m_poly->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_poly->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_poly->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_poly->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
