<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_tarif', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_tarifinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_tarif->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_tarif->Export; // Get export parameter, used in header
$sExportFile = $m_tarif->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["kode"] <> "") {
	$m_tarif->kode->setQueryStringValue($_GET["kode"]);
	$sKey .= $m_tarif->kode->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_tarif->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	$sFilter .= "kode='" . ew_AdjustSql($sKeyFld) . "' AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_tarif class, m_tarifinfo.php

$m_tarif->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_tarif->CurrentAction = $_POST["a_delete"];
} else {
	$m_tarif->CurrentAction = "I"; // Display record
}
switch ($m_tarif->CurrentAction) {
	case "D": // Delete
		$m_tarif->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_tarif->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_tarif->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m tarif<br><br><a href="<?php echo $m_tarif->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_tarifdelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">kode</td>
		<td valign="top">group jasa</td>
		<td valign="top">kode jasa</td>
		<td valign="top">tarif</td>
		<td valign="top">askes</td>
		<td valign="top">cost sharing</td>
		<td valign="top">jasa sarana</td>
		<td valign="top">jasa pelayanan</td>
		<td valign="top">jasa dokter</td>
		<td valign="top">tim rs</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_tarif->CssClass = "ewTableRow";
	$m_tarif->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_tarif->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_tarif->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_tarif->DisplayAttributes() ?>>
		<td<?php echo $m_tarif->kode->CellAttributes() ?>>
<div<?php echo $m_tarif->kode->ViewAttributes() ?>><?php echo $m_tarif->kode->ViewValue ?></div>
</td>
		<td<?php echo $m_tarif->group_jasa->CellAttributes() ?>>
<div<?php echo $m_tarif->group_jasa->ViewAttributes() ?>><?php echo $m_tarif->group_jasa->ViewValue ?></div>
</td>
		<td<?php echo $m_tarif->kode_jasa->CellAttributes() ?>>
<div<?php echo $m_tarif->kode_jasa->ViewAttributes() ?>><?php echo $m_tarif->kode_jasa->ViewValue ?></div>
</td>
		<td<?php echo $m_tarif->tarif->CellAttributes() ?>>
<div<?php echo $m_tarif->tarif->ViewAttributes() ?>><?php echo $m_tarif->tarif->ViewValue ?></div>
</td>
		<td<?php echo $m_tarif->askes->CellAttributes() ?>>
<div<?php echo $m_tarif->askes->ViewAttributes() ?>><?php echo $m_tarif->askes->ViewValue ?></div>
</td>
		<td<?php echo $m_tarif->cost_sharing->CellAttributes() ?>>
<div<?php echo $m_tarif->cost_sharing->ViewAttributes() ?>><?php echo $m_tarif->cost_sharing->ViewValue ?></div>
</td>
		<td<?php echo $m_tarif->jasa_sarana->CellAttributes() ?>>
<div<?php echo $m_tarif->jasa_sarana->ViewAttributes() ?>><?php echo $m_tarif->jasa_sarana->ViewValue ?></div>
</td>
		<td<?php echo $m_tarif->jasa_pelayanan->CellAttributes() ?>>
<div<?php echo $m_tarif->jasa_pelayanan->ViewAttributes() ?>><?php echo $m_tarif->jasa_pelayanan->ViewValue ?></div>
</td>
		<td<?php echo $m_tarif->jasa_dokter->CellAttributes() ?>>
<div<?php echo $m_tarif->jasa_dokter->ViewAttributes() ?>><?php echo $m_tarif->jasa_dokter->ViewValue ?></div>
</td>
		<td<?php echo $m_tarif->tim_rs->CellAttributes() ?>>
<div<?php echo $m_tarif->tim_rs->ViewAttributes() ?>><?php echo $m_tarif->tim_rs->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_tarif;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_tarif->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_tarif class, m_tarifinfo.php

	$m_tarif->CurrentFilter = $sWrkFilter;
	$sSql = $m_tarif->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_tarif->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['kode'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_tarif->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_tarif->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_tarif->CancelMessage;
			$m_tarif->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_tarif->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_tarif;

	// Call Recordset Selecting event
	$m_tarif->Recordset_Selecting($m_tarif->CurrentFilter);

	// Load list page sql
	$sSql = $m_tarif->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_tarif->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_tarif;
	$sFilter = $m_tarif->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($m_tarif->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_tarif->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_tarif->CurrentFilter = $sFilter;
	$sSql = $m_tarif->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_tarif->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_tarif;
	$m_tarif->kode->setDbValue($rs->fields('kode'));
	$m_tarif->group_jasa->setDbValue($rs->fields('group_jasa'));
	$m_tarif->kode_jasa->setDbValue($rs->fields('kode_jasa'));
	$m_tarif->nama_jasa->setDbValue($rs->fields('nama_jasa'));
	$m_tarif->tarif->setDbValue($rs->fields('tarif'));
	$m_tarif->askes->setDbValue($rs->fields('askes'));
	$m_tarif->cost_sharing->setDbValue($rs->fields('cost_sharing'));
	$m_tarif->jasa_sarana->setDbValue($rs->fields('jasa_sarana'));
	$m_tarif->jasa_pelayanan->setDbValue($rs->fields('jasa_pelayanan'));
	$m_tarif->jasa_dokter->setDbValue($rs->fields('jasa_dokter'));
	$m_tarif->tim_rs->setDbValue($rs->fields('tim_rs'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_tarif;

	// Call Row Rendering event
	$m_tarif->Row_Rendering();

	// Common render codes for all row types
	// kode

	$m_tarif->kode->CellCssStyle = "";
	$m_tarif->kode->CellCssClass = "";

	// group_jasa
	$m_tarif->group_jasa->CellCssStyle = "";
	$m_tarif->group_jasa->CellCssClass = "";

	// kode_jasa
	$m_tarif->kode_jasa->CellCssStyle = "";
	$m_tarif->kode_jasa->CellCssClass = "";

	// tarif
	$m_tarif->tarif->CellCssStyle = "";
	$m_tarif->tarif->CellCssClass = "";

	// askes
	$m_tarif->askes->CellCssStyle = "";
	$m_tarif->askes->CellCssClass = "";

	// cost_sharing
	$m_tarif->cost_sharing->CellCssStyle = "";
	$m_tarif->cost_sharing->CellCssClass = "";

	// jasa_sarana
	$m_tarif->jasa_sarana->CellCssStyle = "";
	$m_tarif->jasa_sarana->CellCssClass = "";

	// jasa_pelayanan
	$m_tarif->jasa_pelayanan->CellCssStyle = "";
	$m_tarif->jasa_pelayanan->CellCssClass = "";

	// jasa_dokter
	$m_tarif->jasa_dokter->CellCssStyle = "";
	$m_tarif->jasa_dokter->CellCssClass = "";

	// tim_rs
	$m_tarif->tim_rs->CellCssStyle = "";
	$m_tarif->tim_rs->CellCssClass = "";
	if ($m_tarif->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode
		$m_tarif->kode->ViewValue = $m_tarif->kode->CurrentValue;
		$m_tarif->kode->CssStyle = "";
		$m_tarif->kode->CssClass = "";
		$m_tarif->kode->ViewCustomAttributes = "";

		// group_jasa
		$m_tarif->group_jasa->ViewValue = $m_tarif->group_jasa->CurrentValue;
		$m_tarif->group_jasa->CssStyle = "";
		$m_tarif->group_jasa->CssClass = "";
		$m_tarif->group_jasa->ViewCustomAttributes = "";

		// kode_jasa
		$m_tarif->kode_jasa->ViewValue = $m_tarif->kode_jasa->CurrentValue;
		$m_tarif->kode_jasa->CssStyle = "";
		$m_tarif->kode_jasa->CssClass = "";
		$m_tarif->kode_jasa->ViewCustomAttributes = "";

		// tarif
		$m_tarif->tarif->ViewValue = $m_tarif->tarif->CurrentValue;
		$m_tarif->tarif->CssStyle = "";
		$m_tarif->tarif->CssClass = "";
		$m_tarif->tarif->ViewCustomAttributes = "";

		// askes
		$m_tarif->askes->ViewValue = $m_tarif->askes->CurrentValue;
		$m_tarif->askes->CssStyle = "";
		$m_tarif->askes->CssClass = "";
		$m_tarif->askes->ViewCustomAttributes = "";

		// cost_sharing
		$m_tarif->cost_sharing->ViewValue = $m_tarif->cost_sharing->CurrentValue;
		$m_tarif->cost_sharing->CssStyle = "";
		$m_tarif->cost_sharing->CssClass = "";
		$m_tarif->cost_sharing->ViewCustomAttributes = "";

		// jasa_sarana
		$m_tarif->jasa_sarana->ViewValue = $m_tarif->jasa_sarana->CurrentValue;
		$m_tarif->jasa_sarana->CssStyle = "";
		$m_tarif->jasa_sarana->CssClass = "";
		$m_tarif->jasa_sarana->ViewCustomAttributes = "";

		// jasa_pelayanan
		$m_tarif->jasa_pelayanan->ViewValue = $m_tarif->jasa_pelayanan->CurrentValue;
		$m_tarif->jasa_pelayanan->CssStyle = "";
		$m_tarif->jasa_pelayanan->CssClass = "";
		$m_tarif->jasa_pelayanan->ViewCustomAttributes = "";

		// jasa_dokter
		$m_tarif->jasa_dokter->ViewValue = $m_tarif->jasa_dokter->CurrentValue;
		$m_tarif->jasa_dokter->CssStyle = "";
		$m_tarif->jasa_dokter->CssClass = "";
		$m_tarif->jasa_dokter->ViewCustomAttributes = "";

		// tim_rs
		$m_tarif->tim_rs->ViewValue = $m_tarif->tim_rs->CurrentValue;
		$m_tarif->tim_rs->CssStyle = "";
		$m_tarif->tim_rs->CssClass = "";
		$m_tarif->tim_rs->ViewCustomAttributes = "";

		// kode
		$m_tarif->kode->HrefValue = "";

		// group_jasa
		$m_tarif->group_jasa->HrefValue = "";

		// kode_jasa
		$m_tarif->kode_jasa->HrefValue = "";

		// tarif
		$m_tarif->tarif->HrefValue = "";

		// askes
		$m_tarif->askes->HrefValue = "";

		// cost_sharing
		$m_tarif->cost_sharing->HrefValue = "";

		// jasa_sarana
		$m_tarif->jasa_sarana->HrefValue = "";

		// jasa_pelayanan
		$m_tarif->jasa_pelayanan->HrefValue = "";

		// jasa_dokter
		$m_tarif->jasa_dokter->HrefValue = "";

		// tim_rs
		$m_tarif->tim_rs->HrefValue = "";
	} elseif ($m_tarif->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_tarif->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_tarif->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_tarif->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
