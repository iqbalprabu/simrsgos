<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_barang_unit', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_barang_unitinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_barang_unit->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_barang_unit->Export; // Get export parameter, used in header
$sExportFile = $m_barang_unit->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["kode_barang"] != "") {
  $m_barang_unit->kode_barang->setQueryStringValue($_GET["kode_barang"]);
} else {
  $bCopy = FALSE;
}
if (@$_GET["KDUNIT"] != "") {
  $m_barang_unit->KDUNIT->setQueryStringValue($_GET["KDUNIT"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $m_barang_unit->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $m_barang_unit->CurrentAction = "C"; // Copy Record
  } else {
    $m_barang_unit->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($m_barang_unit->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($m_barang_unit->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$m_barang_unit->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($m_barang_unit->KeyUrl($m_barang_unit->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$m_barang_unit->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_kode_barang"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - kode barang"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_kode_barang"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - kode barang"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_KDUNIT"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - KDUNIT"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_KDUNIT"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - KDUNIT"))
				return false; 
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: m barang unit<br><br><a href="<?php echo $m_barang_unit->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="fm_barang_unitadd" id="fm_barang_unitadd" action="m_barang_unitadd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">kode barang<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_barang_unit->kode_barang->CellAttributes() ?>><span id="cb_x_kode_barang">
<input type="text" name="x_kode_barang" id="x_kode_barang"  size="30" value="<?php echo $m_barang_unit->kode_barang->EditValue ?>"<?php echo $m_barang_unit->kode_barang->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">KDUNIT<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_barang_unit->KDUNIT->CellAttributes() ?>><span id="cb_x_KDUNIT">
<input type="text" name="x_KDUNIT" id="x_KDUNIT"  size="30" value="<?php echo $m_barang_unit->KDUNIT->EditValue ?>"<?php echo $m_barang_unit->KDUNIT->EditAttributes() ?>>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $m_barang_unit;
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_barang_unit;
	$m_barang_unit->kode_barang->setFormValue($objForm->GetValue("x_kode_barang"));
	$m_barang_unit->KDUNIT->setFormValue($objForm->GetValue("x_KDUNIT"));
}

// Restore form values
function RestoreFormValues() {
	global $m_barang_unit;
	$m_barang_unit->kode_barang->CurrentValue = $m_barang_unit->kode_barang->FormValue;
	$m_barang_unit->KDUNIT->CurrentValue = $m_barang_unit->KDUNIT->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_barang_unit;
	$sFilter = $m_barang_unit->SqlKeyFilter();
	if (!is_numeric($m_barang_unit->kode_barang->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode_barang@", ew_AdjustSql($m_barang_unit->kode_barang->CurrentValue), $sFilter); // Replace key value
	if (!is_numeric($m_barang_unit->KDUNIT->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@KDUNIT@", ew_AdjustSql($m_barang_unit->KDUNIT->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_barang_unit->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_barang_unit->CurrentFilter = $sFilter;
	$sSql = $m_barang_unit->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_barang_unit->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_barang_unit;
	$m_barang_unit->kode_barang->setDbValue($rs->fields('kode_barang'));
	$m_barang_unit->KDUNIT->setDbValue($rs->fields('KDUNIT'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_barang_unit;

	// Call Row Rendering event
	$m_barang_unit->Row_Rendering();

	// Common render codes for all row types
	// kode_barang

	$m_barang_unit->kode_barang->CellCssStyle = "";
	$m_barang_unit->kode_barang->CellCssClass = "";

	// KDUNIT
	$m_barang_unit->KDUNIT->CellCssStyle = "";
	$m_barang_unit->KDUNIT->CellCssClass = "";
	if ($m_barang_unit->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_barang_unit->RowType == EW_ROWTYPE_ADD) { // Add row

		// kode_barang
		$m_barang_unit->kode_barang->EditCustomAttributes = "";
		$m_barang_unit->kode_barang->EditValue = ew_HtmlEncode($m_barang_unit->kode_barang->CurrentValue);

		// KDUNIT
		$m_barang_unit->KDUNIT->EditCustomAttributes = "";
		$m_barang_unit->KDUNIT->EditValue = ew_HtmlEncode($m_barang_unit->KDUNIT->CurrentValue);
	} elseif ($m_barang_unit->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_barang_unit->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_barang_unit->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $m_barang_unit;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $m_barang_unit->SqlKeyFilter();
	if (trim(strval($m_barang_unit->kode_barang->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@kode_barang@", ew_AdjustSql($m_barang_unit->kode_barang->CurrentValue), $sFilter); // Replace key value
	}
	if (!is_numeric($m_barang_unit->kode_barang->CurrentValue)) {
		$bCheckKey = FALSE;
	}
	if (trim(strval($m_barang_unit->KDUNIT->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@KDUNIT@", ew_AdjustSql($m_barang_unit->KDUNIT->CurrentValue), $sFilter); // Replace key value
	}
	if (!is_numeric($m_barang_unit->KDUNIT->CurrentValue)) {
		$bCheckKey = FALSE;
	}
	if ($bCheckKey) {
		$rsChk = $m_barang_unit->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field kode_barang
	$m_barang_unit->kode_barang->SetDbValueDef($m_barang_unit->kode_barang->CurrentValue, 0);
	$rsnew['kode_barang'] =& $m_barang_unit->kode_barang->DbValue;

	// Field KDUNIT
	$m_barang_unit->KDUNIT->SetDbValueDef($m_barang_unit->KDUNIT->CurrentValue, 0);
	$rsnew['KDUNIT'] =& $m_barang_unit->KDUNIT->DbValue;

	// Call Row Inserting event
	$bInsertRow = $m_barang_unit->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($m_barang_unit->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($m_barang_unit->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_barang_unit->CancelMessage;
			$m_barang_unit->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {

		// Call Row Inserted event
		$m_barang_unit->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
