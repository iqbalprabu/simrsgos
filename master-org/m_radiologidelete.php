<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_radiologi', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_radiologiinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_radiologi->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_radiologi->Export; // Get export parameter, used in header
$sExportFile = $m_radiologi->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["kd_rad"] <> "") {
	$m_radiologi->kd_rad->setQueryStringValue($_GET["kd_rad"]);
	$sKey .= $m_radiologi->kd_rad->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_radiologi->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	$sFilter .= "kd_rad='" . ew_AdjustSql($sKeyFld) . "' AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_radiologi class, m_radiologiinfo.php

$m_radiologi->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_radiologi->CurrentAction = $_POST["a_delete"];
} else {
	$m_radiologi->CurrentAction = "I"; // Display record
}
switch ($m_radiologi->CurrentAction) {
	case "D": // Delete
		$m_radiologi->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_radiologi->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_radiologi->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m radiologi<br><br><a href="<?php echo $m_radiologi->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_radiologidelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">kd rad</td>
		<td valign="top">nama rad</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_radiologi->CssClass = "ewTableRow";
	$m_radiologi->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_radiologi->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_radiologi->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_radiologi->DisplayAttributes() ?>>
		<td<?php echo $m_radiologi->kd_rad->CellAttributes() ?>>
<div<?php echo $m_radiologi->kd_rad->ViewAttributes() ?>><?php echo $m_radiologi->kd_rad->ViewValue ?></div>
</td>
		<td<?php echo $m_radiologi->nama_rad->CellAttributes() ?>>
<div<?php echo $m_radiologi->nama_rad->ViewAttributes() ?>><?php echo $m_radiologi->nama_rad->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_radiologi;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_radiologi->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_radiologi class, m_radiologiinfo.php

	$m_radiologi->CurrentFilter = $sWrkFilter;
	$sSql = $m_radiologi->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_radiologi->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['kd_rad'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_radiologi->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_radiologi->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_radiologi->CancelMessage;
			$m_radiologi->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_radiologi->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_radiologi;

	// Call Recordset Selecting event
	$m_radiologi->Recordset_Selecting($m_radiologi->CurrentFilter);

	// Load list page sql
	$sSql = $m_radiologi->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_radiologi->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_radiologi;
	$sFilter = $m_radiologi->SqlKeyFilter();
	$sFilter = str_replace("@kd_rad@", ew_AdjustSql($m_radiologi->kd_rad->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_radiologi->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_radiologi->CurrentFilter = $sFilter;
	$sSql = $m_radiologi->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_radiologi->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_radiologi;
	$m_radiologi->kd_rad->setDbValue($rs->fields('kd_rad'));
	$m_radiologi->nama_rad->setDbValue($rs->fields('nama_rad'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_radiologi;

	// Call Row Rendering event
	$m_radiologi->Row_Rendering();

	// Common render codes for all row types
	// kd_rad

	$m_radiologi->kd_rad->CellCssStyle = "";
	$m_radiologi->kd_rad->CellCssClass = "";

	// nama_rad
	$m_radiologi->nama_rad->CellCssStyle = "";
	$m_radiologi->nama_rad->CellCssClass = "";
	if ($m_radiologi->RowType == EW_ROWTYPE_VIEW) { // View row

		// kd_rad
		$m_radiologi->kd_rad->ViewValue = $m_radiologi->kd_rad->CurrentValue;
		$m_radiologi->kd_rad->CssStyle = "";
		$m_radiologi->kd_rad->CssClass = "";
		$m_radiologi->kd_rad->ViewCustomAttributes = "";

		// nama_rad
		$m_radiologi->nama_rad->ViewValue = $m_radiologi->nama_rad->CurrentValue;
		$m_radiologi->nama_rad->CssStyle = "";
		$m_radiologi->nama_rad->CssClass = "";
		$m_radiologi->nama_rad->ViewCustomAttributes = "";

		// kd_rad
		$m_radiologi->kd_rad->HrefValue = "";

		// nama_rad
		$m_radiologi->nama_rad->HrefValue = "";
	} elseif ($m_radiologi->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_radiologi->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_radiologi->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_radiologi->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
