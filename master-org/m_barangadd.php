<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_barang', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_baranginfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_barang->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_barang->Export; // Get export parameter, used in header
$sExportFile = $m_barang->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["kode_barang"] != "") {
  $m_barang->kode_barang->setQueryStringValue($_GET["kode_barang"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $m_barang->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $m_barang->CurrentAction = "C"; // Copy Record
  } else {
    $m_barang->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($m_barang->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($m_barang->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$m_barang->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($m_barang->KeyUrl($m_barang->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$m_barang->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_expiry"];
		if (elm && !ew_CheckDate(elm.value)) {
			if (!ew_OnError(elm, "Incorrect date, format = yyyy/mm/dd - expiry"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_harga"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - harga"))
				return false; 
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: m barang<br><br><a href="<?php echo $m_barang->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="fm_barangadd" id="fm_barangadd" action="m_barangadd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">no batch</td>
    <td<?php echo $m_barang->no_batch->CellAttributes() ?>><span id="cb_x_no_batch">
<input type="text" name="x_no_batch" id="x_no_batch"  size="30" maxlength="128" value="<?php echo $m_barang->no_batch->EditValue ?>"<?php echo $m_barang->no_batch->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">expiry</td>
    <td<?php echo $m_barang->expiry->CellAttributes() ?>><span id="cb_x_expiry">
<input type="text" name="x_expiry" id="x_expiry"  value="<?php echo $m_barang->expiry->EditValue ?>"<?php echo $m_barang->expiry->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">nama barang</td>
    <td<?php echo $m_barang->nama_barang->CellAttributes() ?>><span id="cb_x_nama_barang">
<textarea name="x_nama_barang" id="x_nama_barang" cols="35" rows="4"<?php echo $m_barang->nama_barang->EditAttributes() ?>><?php echo $m_barang->nama_barang->EditValue ?></textarea>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">group barang</td>
    <td<?php echo $m_barang->group_barang->CellAttributes() ?>><span id="cb_x_group_barang">
<select id="x_group_barang" name="x_group_barang"<?php echo $m_barang->group_barang->EditAttributes() ?>>
<!--option value="">Please Select</option-->
<?php
if (is_array($m_barang->group_barang->EditValue)) {
	$arwrk = $m_barang->group_barang->EditValue;
	$rowswrk = count($arwrk);
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($m_barang->group_barang->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected" : "";	
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
</option>
<?php
			}
}
?>
</select>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">farmasi</td>
    <td<?php echo $m_barang->farmasi->CellAttributes() ?>><span id="cb_x_farmasi">
<input type="text" name="x_farmasi" id="x_farmasi"  size="30" maxlength="1" value="<?php echo $m_barang->farmasi->EditValue ?>"<?php echo $m_barang->farmasi->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">satuan</td>
    <td<?php echo $m_barang->satuan->CellAttributes() ?>><span id="cb_x_satuan">
<input type="text" name="x_satuan" id="x_satuan"  size="30" maxlength="20" value="<?php echo $m_barang->satuan->EditValue ?>"<?php echo $m_barang->satuan->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">harga</td>
    <td<?php echo $m_barang->harga->CellAttributes() ?>><span id="cb_x_harga">
<input type="text" name="x_harga" id="x_harga"  size="30" value="<?php echo $m_barang->harga->EditValue ?>"<?php echo $m_barang->harga->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">hide when print</td>
    <td<?php echo $m_barang->hide_when_print->CellAttributes() ?>><span id="cb_x_hide_when_print">
<input type="text" name="x_hide_when_print" id="x_hide_when_print"  size="30" maxlength="1" value="<?php echo $m_barang->hide_when_print->EditValue ?>"<?php echo $m_barang->hide_when_print->EditAttributes() ?>>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $m_barang;
	$m_barang->harga->CurrentValue = 0;
	$m_barang->hide_when_print->CurrentValue = "0";
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_barang;
	$m_barang->no_batch->setFormValue($objForm->GetValue("x_no_batch"));
	$m_barang->expiry->setFormValue($objForm->GetValue("x_expiry"));
	$m_barang->expiry->CurrentValue = ew_UnFormatDateTime($m_barang->expiry->CurrentValue, 5);
	$m_barang->nama_barang->setFormValue($objForm->GetValue("x_nama_barang"));
	$m_barang->group_barang->setFormValue($objForm->GetValue("x_group_barang"));
	$m_barang->farmasi->setFormValue($objForm->GetValue("x_farmasi"));
	$m_barang->satuan->setFormValue($objForm->GetValue("x_satuan"));
	$m_barang->harga->setFormValue($objForm->GetValue("x_harga"));
	$m_barang->hide_when_print->setFormValue($objForm->GetValue("x_hide_when_print"));
}

// Restore form values
function RestoreFormValues() {
	global $m_barang;
	$m_barang->no_batch->CurrentValue = $m_barang->no_batch->FormValue;
	$m_barang->expiry->CurrentValue = $m_barang->expiry->FormValue;
	$m_barang->expiry->CurrentValue = ew_UnFormatDateTime($m_barang->expiry->CurrentValue, 5);
	$m_barang->nama_barang->CurrentValue = $m_barang->nama_barang->FormValue;
	$m_barang->group_barang->CurrentValue = $m_barang->group_barang->FormValue;
	$m_barang->farmasi->CurrentValue = $m_barang->farmasi->FormValue;
	$m_barang->satuan->CurrentValue = $m_barang->satuan->FormValue;
	$m_barang->harga->CurrentValue = $m_barang->harga->FormValue;
	$m_barang->hide_when_print->CurrentValue = $m_barang->hide_when_print->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_barang;
	$sFilter = $m_barang->SqlKeyFilter();
	if (!is_numeric($m_barang->kode_barang->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode_barang@", ew_AdjustSql($m_barang->kode_barang->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_barang->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_barang->CurrentFilter = $sFilter;
	$sSql = $m_barang->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_barang->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_barang;
	$m_barang->kode_barang->setDbValue($rs->fields('kode_barang'));
	$m_barang->no_batch->setDbValue($rs->fields('no_batch'));
	$m_barang->expiry->setDbValue($rs->fields('expiry'));
	$m_barang->nama_barang->setDbValue($rs->fields('nama_barang'));
	$m_barang->group_barang->setDbValue($rs->fields('group_barang'));
	$m_barang->farmasi->setDbValue($rs->fields('farmasi'));
	$m_barang->satuan->setDbValue($rs->fields('satuan'));
	$m_barang->harga->setDbValue($rs->fields('harga'));
	$m_barang->hide_when_print->setDbValue($rs->fields('hide_when_print'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_barang;

	// Call Row Rendering event
	$m_barang->Row_Rendering();

	// Common render codes for all row types
	// no_batch

	$m_barang->no_batch->CellCssStyle = "";
	$m_barang->no_batch->CellCssClass = "";

	// expiry
	$m_barang->expiry->CellCssStyle = "";
	$m_barang->expiry->CellCssClass = "";

	// nama_barang
	$m_barang->nama_barang->CellCssStyle = "";
	$m_barang->nama_barang->CellCssClass = "";

	// group_barang
	$m_barang->group_barang->CellCssStyle = "";
	$m_barang->group_barang->CellCssClass = "";

	// farmasi
	$m_barang->farmasi->CellCssStyle = "";
	$m_barang->farmasi->CellCssClass = "";

	// satuan
	$m_barang->satuan->CellCssStyle = "";
	$m_barang->satuan->CellCssClass = "";

	// harga
	$m_barang->harga->CellCssStyle = "";
	$m_barang->harga->CellCssClass = "";

	// hide_when_print
	$m_barang->hide_when_print->CellCssStyle = "";
	$m_barang->hide_when_print->CellCssClass = "";
	if ($m_barang->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_barang->RowType == EW_ROWTYPE_ADD) { // Add row

		// no_batch
		$m_barang->no_batch->EditCustomAttributes = "";
		$m_barang->no_batch->EditValue = ew_HtmlEncode($m_barang->no_batch->CurrentValue);

		// expiry
		$m_barang->expiry->EditCustomAttributes = "";
		$m_barang->expiry->EditValue = ew_HtmlEncode(ew_FormatDateTime($m_barang->expiry->CurrentValue, 5));

		// nama_barang
		$m_barang->nama_barang->EditCustomAttributes = "";
		$m_barang->nama_barang->EditValue = ew_HtmlEncode($m_barang->nama_barang->CurrentValue);

		// group_barang
		$m_barang->group_barang->EditCustomAttributes = "";

		// farmasi
		$m_barang->farmasi->EditCustomAttributes = "";
		$m_barang->farmasi->EditValue = ew_HtmlEncode($m_barang->farmasi->CurrentValue);

		// satuan
		$m_barang->satuan->EditCustomAttributes = "";
		$m_barang->satuan->EditValue = ew_HtmlEncode($m_barang->satuan->CurrentValue);

		// harga
		$m_barang->harga->EditCustomAttributes = "";
		$m_barang->harga->EditValue = ew_HtmlEncode($m_barang->harga->CurrentValue);

		// hide_when_print
		$m_barang->hide_when_print->EditCustomAttributes = "";
		$m_barang->hide_when_print->EditValue = ew_HtmlEncode($m_barang->hide_when_print->CurrentValue);
	} elseif ($m_barang->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_barang->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_barang->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $m_barang;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $m_barang->SqlKeyFilter();
	if (trim(strval($m_barang->kode_barang->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@kode_barang@", ew_AdjustSql($m_barang->kode_barang->CurrentValue), $sFilter); // Replace key value
	}
	if (!is_numeric($m_barang->kode_barang->CurrentValue)) {
		$bCheckKey = FALSE;
	}
	if ($bCheckKey) {
		$rsChk = $m_barang->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field no_batch
	$m_barang->no_batch->SetDbValueDef($m_barang->no_batch->CurrentValue, NULL);
	$rsnew['no_batch'] =& $m_barang->no_batch->DbValue;

	// Field expiry
	$m_barang->expiry->SetDbValueDef(ew_UnFormatDateTime($m_barang->expiry->CurrentValue, 5), NULL);
	$rsnew['expiry'] =& $m_barang->expiry->DbValue;

	// Field nama_barang
	$m_barang->nama_barang->SetDbValueDef($m_barang->nama_barang->CurrentValue, NULL);
	$rsnew['nama_barang'] =& $m_barang->nama_barang->DbValue;

	// Field group_barang
	$m_barang->group_barang->SetDbValueDef($m_barang->group_barang->CurrentValue, NULL);
	$rsnew['group_barang'] =& $m_barang->group_barang->DbValue;

	// Field farmasi
	$m_barang->farmasi->SetDbValueDef($m_barang->farmasi->CurrentValue, NULL);
	$rsnew['farmasi'] =& $m_barang->farmasi->DbValue;

	// Field satuan
	$m_barang->satuan->SetDbValueDef($m_barang->satuan->CurrentValue, NULL);
	$rsnew['satuan'] =& $m_barang->satuan->DbValue;

	// Field harga
	$m_barang->harga->SetDbValueDef($m_barang->harga->CurrentValue, NULL);
	$rsnew['harga'] =& $m_barang->harga->DbValue;

	// Field hide_when_print
	$m_barang->hide_when_print->SetDbValueDef($m_barang->hide_when_print->CurrentValue, NULL);
	$rsnew['hide_when_print'] =& $m_barang->hide_when_print->DbValue;

	// Call Row Inserting event
	$bInsertRow = $m_barang->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($m_barang->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($m_barang->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_barang->CancelMessage;
			$m_barang->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {
		$m_barang->kode_barang->setDbValue($conn->Insert_ID());
		$rsnew['kode_barang'] =& $m_barang->kode_barang->DbValue;

		// Call Row Inserted event
		$m_barang->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
