<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_barang', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_baranginfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_barang->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_barang->Export; // Get export parameter, used in header
$sExportFile = $m_barang->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["kode_barang"] <> "") {
	$m_barang->kode_barang->setQueryStringValue($_GET["kode_barang"]);
} else {
	Page_Terminate("m_baranglist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$m_barang->CurrentAction = $_POST["a_view"];
} else {
	$m_barang->CurrentAction = "I"; // Display form
}
switch ($m_barang->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("m_baranglist.php"); // Return to list
		}
}

// Set return url
$m_barang->setReturnUrl("m_barangview.php");

// Render row
$m_barang->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: m barang
<br><br>
<a href="m_baranglist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_barangadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_barang->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_barang->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $m_barang->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode barang</td>
		<td<?php echo $m_barang->kode_barang->CellAttributes() ?>>
<div<?php echo $m_barang->kode_barang->ViewAttributes() ?>><?php echo $m_barang->kode_barang->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">no batch</td>
		<td<?php echo $m_barang->no_batch->CellAttributes() ?>>
<div<?php echo $m_barang->no_batch->ViewAttributes() ?>><?php echo $m_barang->no_batch->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">expiry</td>
		<td<?php echo $m_barang->expiry->CellAttributes() ?>>
<div<?php echo $m_barang->expiry->ViewAttributes() ?>><?php echo $m_barang->expiry->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">nama barang</td>
		<td<?php echo $m_barang->nama_barang->CellAttributes() ?>>
<div<?php echo $m_barang->nama_barang->ViewAttributes() ?>><?php echo $m_barang->nama_barang->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">group barang</td>
		<td<?php echo $m_barang->group_barang->CellAttributes() ?>>
<div<?php echo $m_barang->group_barang->ViewAttributes() ?>><?php echo $m_barang->group_barang->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">farmasi</td>
		<td<?php echo $m_barang->farmasi->CellAttributes() ?>>
<div<?php echo $m_barang->farmasi->ViewAttributes() ?>><?php echo $m_barang->farmasi->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">satuan</td>
		<td<?php echo $m_barang->satuan->CellAttributes() ?>>
<div<?php echo $m_barang->satuan->ViewAttributes() ?>><?php echo $m_barang->satuan->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">harga</td>
		<td<?php echo $m_barang->harga->CellAttributes() ?>>
<div<?php echo $m_barang->harga->ViewAttributes() ?>><?php echo $m_barang->harga->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableRow">
		<td class="ewTableHeader">hide when print</td>
		<td<?php echo $m_barang->hide_when_print->CellAttributes() ?>>
<div<?php echo $m_barang->hide_when_print->ViewAttributes() ?>><?php echo $m_barang->hide_when_print->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_barang;
	$sFilter = $m_barang->SqlKeyFilter();
	if (!is_numeric($m_barang->kode_barang->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode_barang@", ew_AdjustSql($m_barang->kode_barang->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_barang->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_barang->CurrentFilter = $sFilter;
	$sSql = $m_barang->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_barang->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_barang;
	$m_barang->kode_barang->setDbValue($rs->fields('kode_barang'));
	$m_barang->no_batch->setDbValue($rs->fields('no_batch'));
	$m_barang->expiry->setDbValue($rs->fields('expiry'));
	$m_barang->nama_barang->setDbValue($rs->fields('nama_barang'));
	$m_barang->group_barang->setDbValue($rs->fields('group_barang'));
	$m_barang->farmasi->setDbValue($rs->fields('farmasi'));
	$m_barang->satuan->setDbValue($rs->fields('satuan'));
	$m_barang->harga->setDbValue($rs->fields('harga'));
	$m_barang->hide_when_print->setDbValue($rs->fields('hide_when_print'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_barang;

	// Call Row Rendering event
	$m_barang->Row_Rendering();

	// Common render codes for all row types
	// kode_barang

	$m_barang->kode_barang->CellCssStyle = "";
	$m_barang->kode_barang->CellCssClass = "";

	// no_batch
	$m_barang->no_batch->CellCssStyle = "";
	$m_barang->no_batch->CellCssClass = "";

	// expiry
	$m_barang->expiry->CellCssStyle = "";
	$m_barang->expiry->CellCssClass = "";

	// nama_barang
	$m_barang->nama_barang->CellCssStyle = "";
	$m_barang->nama_barang->CellCssClass = "";

	// group_barang
	$m_barang->group_barang->CellCssStyle = "";
	$m_barang->group_barang->CellCssClass = "";

	// farmasi
	$m_barang->farmasi->CellCssStyle = "";
	$m_barang->farmasi->CellCssClass = "";

	// satuan
	$m_barang->satuan->CellCssStyle = "";
	$m_barang->satuan->CellCssClass = "";

	// harga
	$m_barang->harga->CellCssStyle = "";
	$m_barang->harga->CellCssClass = "";

	// hide_when_print
	$m_barang->hide_when_print->CellCssStyle = "";
	$m_barang->hide_when_print->CellCssClass = "";
	if ($m_barang->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode_barang
		$m_barang->kode_barang->ViewValue = $m_barang->kode_barang->CurrentValue;
		$m_barang->kode_barang->CssStyle = "";
		$m_barang->kode_barang->CssClass = "";
		$m_barang->kode_barang->ViewCustomAttributes = "";

		// no_batch
		$m_barang->no_batch->ViewValue = $m_barang->no_batch->CurrentValue;
		$m_barang->no_batch->CssStyle = "";
		$m_barang->no_batch->CssClass = "";
		$m_barang->no_batch->ViewCustomAttributes = "";

		// expiry
		$m_barang->expiry->ViewValue = $m_barang->expiry->CurrentValue;
		$m_barang->expiry->ViewValue = ew_FormatDateTime($m_barang->expiry->ViewValue, 5);
		$m_barang->expiry->CssStyle = "";
		$m_barang->expiry->CssClass = "";
		$m_barang->expiry->ViewCustomAttributes = "";

		// nama_barang
		$m_barang->nama_barang->ViewValue = $m_barang->nama_barang->CurrentValue;
		if (!is_null($m_barang->nama_barang->ViewValue)) $m_barang->nama_barang->ViewValue = str_replace("\n", "<br>", $m_barang->nama_barang->ViewValue); 
		$m_barang->nama_barang->CssStyle = "";
		$m_barang->nama_barang->CssClass = "";
		$m_barang->nama_barang->ViewCustomAttributes = "";

		// group_barang
		$m_barang->group_barang->CssStyle = "";
		$m_barang->group_barang->CssClass = "";
		$m_barang->group_barang->ViewCustomAttributes = "";

		// farmasi
		$m_barang->farmasi->ViewValue = $m_barang->farmasi->CurrentValue;
		$m_barang->farmasi->CssStyle = "";
		$m_barang->farmasi->CssClass = "";
		$m_barang->farmasi->ViewCustomAttributes = "";

		// satuan
		$m_barang->satuan->ViewValue = $m_barang->satuan->CurrentValue;
		$m_barang->satuan->CssStyle = "";
		$m_barang->satuan->CssClass = "";
		$m_barang->satuan->ViewCustomAttributes = "";

		// harga
		$m_barang->harga->ViewValue = $m_barang->harga->CurrentValue;
		$m_barang->harga->CssStyle = "";
		$m_barang->harga->CssClass = "";
		$m_barang->harga->ViewCustomAttributes = "";

		// hide_when_print
		$m_barang->hide_when_print->ViewValue = $m_barang->hide_when_print->CurrentValue;
		$m_barang->hide_when_print->CssStyle = "";
		$m_barang->hide_when_print->CssClass = "";
		$m_barang->hide_when_print->ViewCustomAttributes = "";

		// kode_barang
		$m_barang->kode_barang->HrefValue = "";

		// no_batch
		$m_barang->no_batch->HrefValue = "";

		// expiry
		$m_barang->expiry->HrefValue = "";

		// nama_barang
		$m_barang->nama_barang->HrefValue = "";

		// group_barang
		$m_barang->group_barang->HrefValue = "";

		// farmasi
		$m_barang->farmasi->HrefValue = "";

		// satuan
		$m_barang->satuan->HrefValue = "";

		// harga
		$m_barang->harga->HrefValue = "";

		// hide_when_print
		$m_barang->hide_when_print->HrefValue = "";
	} elseif ($m_barang->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_barang->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_barang->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_barang->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_barang;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_barang->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_barang->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_barang->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_barang->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_barang->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_barang->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_barang->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
