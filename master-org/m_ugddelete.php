<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_ugd', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_ugdinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_ugd->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_ugd->Export; // Get export parameter, used in header
$sExportFile = $m_ugd->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["kode_ugd"] <> "") {
	$m_ugd->kode_ugd->setQueryStringValue($_GET["kode_ugd"]);
	$sKey .= $m_ugd->kode_ugd->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_ugd->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	$sFilter .= "kode_ugd='" . ew_AdjustSql($sKeyFld) . "' AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_ugd class, m_ugdinfo.php

$m_ugd->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_ugd->CurrentAction = $_POST["a_delete"];
} else {
	$m_ugd->CurrentAction = "I"; // Display record
}
switch ($m_ugd->CurrentAction) {
	case "D": // Delete
		$m_ugd->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_ugd->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_ugd->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m ugd<br><br><a href="<?php echo $m_ugd->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_ugddelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">kode ugd</td>
		<td valign="top">kode group</td>
		<td valign="top">group</td>
		<td valign="top">pemeriksaan ugd</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_ugd->CssClass = "ewTableRow";
	$m_ugd->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_ugd->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_ugd->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_ugd->DisplayAttributes() ?>>
		<td<?php echo $m_ugd->kode_ugd->CellAttributes() ?>>
<div<?php echo $m_ugd->kode_ugd->ViewAttributes() ?>><?php echo $m_ugd->kode_ugd->ViewValue ?></div>
</td>
		<td<?php echo $m_ugd->kode_group->CellAttributes() ?>>
<div<?php echo $m_ugd->kode_group->ViewAttributes() ?>><?php echo $m_ugd->kode_group->ViewValue ?></div>
</td>
		<td<?php echo $m_ugd->group->CellAttributes() ?>>
<div<?php echo $m_ugd->group->ViewAttributes() ?>><?php echo $m_ugd->group->ViewValue ?></div>
</td>
		<td<?php echo $m_ugd->pemeriksaan_ugd->CellAttributes() ?>>
<div<?php echo $m_ugd->pemeriksaan_ugd->ViewAttributes() ?>><?php echo $m_ugd->pemeriksaan_ugd->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_ugd;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_ugd->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_ugd class, m_ugdinfo.php

	$m_ugd->CurrentFilter = $sWrkFilter;
	$sSql = $m_ugd->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_ugd->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['kode_ugd'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_ugd->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_ugd->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_ugd->CancelMessage;
			$m_ugd->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_ugd->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_ugd;

	// Call Recordset Selecting event
	$m_ugd->Recordset_Selecting($m_ugd->CurrentFilter);

	// Load list page sql
	$sSql = $m_ugd->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_ugd->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_ugd;
	$sFilter = $m_ugd->SqlKeyFilter();
	$sFilter = str_replace("@kode_ugd@", ew_AdjustSql($m_ugd->kode_ugd->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_ugd->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_ugd->CurrentFilter = $sFilter;
	$sSql = $m_ugd->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_ugd->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_ugd;
	$m_ugd->kode_ugd->setDbValue($rs->fields('kode_ugd'));
	$m_ugd->kode_group->setDbValue($rs->fields('kode_group'));
	$m_ugd->group->setDbValue($rs->fields('group'));
	$m_ugd->pemeriksaan_ugd->setDbValue($rs->fields('pemeriksaan_ugd'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_ugd;

	// Call Row Rendering event
	$m_ugd->Row_Rendering();

	// Common render codes for all row types
	// kode_ugd

	$m_ugd->kode_ugd->CellCssStyle = "";
	$m_ugd->kode_ugd->CellCssClass = "";

	// kode_group
	$m_ugd->kode_group->CellCssStyle = "";
	$m_ugd->kode_group->CellCssClass = "";

	// group
	$m_ugd->group->CellCssStyle = "";
	$m_ugd->group->CellCssClass = "";

	// pemeriksaan_ugd
	$m_ugd->pemeriksaan_ugd->CellCssStyle = "";
	$m_ugd->pemeriksaan_ugd->CellCssClass = "";
	if ($m_ugd->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode_ugd
		$m_ugd->kode_ugd->ViewValue = $m_ugd->kode_ugd->CurrentValue;
		$m_ugd->kode_ugd->CssStyle = "";
		$m_ugd->kode_ugd->CssClass = "";
		$m_ugd->kode_ugd->ViewCustomAttributes = "";

		// kode_group
		$m_ugd->kode_group->ViewValue = $m_ugd->kode_group->CurrentValue;
		$m_ugd->kode_group->CssStyle = "";
		$m_ugd->kode_group->CssClass = "";
		$m_ugd->kode_group->ViewCustomAttributes = "";

		// group
		$m_ugd->group->ViewValue = $m_ugd->group->CurrentValue;
		$m_ugd->group->CssStyle = "";
		$m_ugd->group->CssClass = "";
		$m_ugd->group->ViewCustomAttributes = "";

		// pemeriksaan_ugd
		$m_ugd->pemeriksaan_ugd->ViewValue = $m_ugd->pemeriksaan_ugd->CurrentValue;
		$m_ugd->pemeriksaan_ugd->CssStyle = "";
		$m_ugd->pemeriksaan_ugd->CssClass = "";
		$m_ugd->pemeriksaan_ugd->ViewCustomAttributes = "";

		// kode_ugd
		$m_ugd->kode_ugd->HrefValue = "";

		// kode_group
		$m_ugd->kode_group->HrefValue = "";

		// group
		$m_ugd->group->HrefValue = "";

		// pemeriksaan_ugd
		$m_ugd->pemeriksaan_ugd->HrefValue = "";
	} elseif ($m_ugd->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_ugd->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_ugd->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_ugd->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
