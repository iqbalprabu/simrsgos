<?php
define("EW_PAGE_ID", "delete", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_barang', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_baranginfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_barang->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_barang->Export; // Get export parameter, used in header
$sExportFile = $m_barang->TableVar; // Get export file, used in header
?>
<?php

// Load Key Parameters
$sKey = "";
$bSingleDelete = TRUE; // Initialize as single delete
$arRecKeys = array();
$nKeySelected = 0; // Initialize selected key count
$sFilter = "";
if (@$_GET["kode_barang"] <> "") {
	$m_barang->kode_barang->setQueryStringValue($_GET["kode_barang"]);
	if (!is_numeric($m_barang->kode_barang->QueryStringValue)) {
		Page_Terminate($m_barang->getReturnUrl()); // Prevent sql injection, exit
	}
	$sKey .= $m_barang->kode_barang->QueryStringValue;
} else {
	$bSingleDelete = FALSE;
}
if ($bSingleDelete) {
	$nKeySelected = 1; // Set up key selected count
	$arRecKeys[0] = $sKey;
} else {
	if (isset($_POST["key_m"])) { // Key in form
		$nKeySelected = count($_POST["key_m"]); // Set up key selected count
		$arRecKeys = ew_StripSlashes($_POST["key_m"]);
	}
}
if ($nKeySelected <= 0) Page_Terminate($m_barang->getReturnUrl()); // No key specified, exit

// Build filter
foreach ($arRecKeys as $sKey) {
	$sFilter .= "(";

	// Set up key field
	$sKeyFld = $sKey;
	if (!is_numeric($sKeyFld)) {
		Page_Terminate($m_barang->getReturnUrl()); // Prevent sql injection, exit
	}
	$sFilter .= "kode_barang=" . ew_AdjustSql($sKeyFld) . " AND ";
	if (substr($sFilter, -5) == " AND ") $sFilter = substr($sFilter, 0, strlen($sFilter)-5) . ") OR ";
}
if (substr($sFilter, -4) == " OR ") $sFilter = substr($sFilter, 0, strlen($sFilter)-4);

// Set up filter (Sql Where Clause) and get Return Sql
// Sql constructor in m_barang class, m_baranginfo.php

$m_barang->CurrentFilter = $sFilter;

// Get action
if (@$_POST["a_delete"] <> "") {
	$m_barang->CurrentAction = $_POST["a_delete"];
} else {
	$m_barang->CurrentAction = "I"; // Display record
}
switch ($m_barang->CurrentAction) {
	case "D": // Delete
		$m_barang->SendEmail = TRUE; // Send email on delete success
		if (DeleteRows()) { // delete rows
			$_SESSION[EW_SESSION_MESSAGE] = "Delete Successful"; // Set up success message
			Page_Terminate($m_barang->getReturnUrl()); // Return to caller
		}
}

// Load records for display
$rs = LoadRecordset();
$nTotalRecs = $rs->RecordCount(); // Get record count
if ($nTotalRecs <= 0) { // No record found, exit
	$rs->Close();
	Page_Terminate($m_barang->getReturnUrl()); // Return to caller
}
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "delete"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Delete from TABLE: m barang<br><br><a href="<?php echo $m_barang->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form action="m_barangdelete.php" method="post">
<p>
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($arRecKeys as $sKey) { ?>
<input type="hidden" name="key_m[]" id="key_m[]" value="<?php echo ew_HtmlEncode($sKey) ?>">
<?php } ?>
<table class="ewTable">
	<tr class="ewTableHeader">
		<td valign="top">kode barang</td>
		<td valign="top">no batch</td>
		<td valign="top">expiry</td>
		<td valign="top">group barang</td>
		<td valign="top">farmasi</td>
		<td valign="top">satuan</td>
		<td valign="top">harga</td>
		<td valign="top">hide when print</td>
	</tr>
<?php
$nRecCount = 0;
$i = 0;
while (!$rs->EOF) {
	$nRecCount++;

	// Set row class and style
	$m_barang->CssClass = "ewTableRow";
	$m_barang->CssStyle = "";

	// Display alternate color for rows
	if ($nRecCount % 2 <> 1) {
		$m_barang->CssClass = "ewTableAltRow";
	}

	// Get the field contents
	LoadRowValues($rs);

	// Render row value
	$m_barang->RowType = EW_ROWTYPE_VIEW; // view
	RenderRow();
?>
	<tr<?php echo $m_barang->DisplayAttributes() ?>>
		<td<?php echo $m_barang->kode_barang->CellAttributes() ?>>
<div<?php echo $m_barang->kode_barang->ViewAttributes() ?>><?php echo $m_barang->kode_barang->ViewValue ?></div>
</td>
		<td<?php echo $m_barang->no_batch->CellAttributes() ?>>
<div<?php echo $m_barang->no_batch->ViewAttributes() ?>><?php echo $m_barang->no_batch->ViewValue ?></div>
</td>
		<td<?php echo $m_barang->expiry->CellAttributes() ?>>
<div<?php echo $m_barang->expiry->ViewAttributes() ?>><?php echo $m_barang->expiry->ViewValue ?></div>
</td>
		<td<?php echo $m_barang->group_barang->CellAttributes() ?>>
<div<?php echo $m_barang->group_barang->ViewAttributes() ?>><?php echo $m_barang->group_barang->ViewValue ?></div>
</td>
		<td<?php echo $m_barang->farmasi->CellAttributes() ?>>
<div<?php echo $m_barang->farmasi->ViewAttributes() ?>><?php echo $m_barang->farmasi->ViewValue ?></div>
</td>
		<td<?php echo $m_barang->satuan->CellAttributes() ?>>
<div<?php echo $m_barang->satuan->ViewAttributes() ?>><?php echo $m_barang->satuan->ViewValue ?></div>
</td>
		<td<?php echo $m_barang->harga->CellAttributes() ?>>
<div<?php echo $m_barang->harga->ViewAttributes() ?>><?php echo $m_barang->harga->ViewValue ?></div>
</td>
		<td<?php echo $m_barang->hide_when_print->CellAttributes() ?>>
<div<?php echo $m_barang->hide_when_print->ViewAttributes() ?>><?php echo $m_barang->hide_when_print->ViewValue ?></div>
</td>
	</tr>
<?php
	$rs->MoveNext();
}
$rs->Close();
?>
</table>
<p>
<input type="submit" name="Action" id="Action" value="Confirm Delete">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// ------------------------------------------------
//  Function DeleteRows
//  - Delete Records based on current filter
function DeleteRows() {
	global $conn, $Security, $m_barang;
	$DeleteRows = TRUE;
	$sWrkFilter = $m_barang->CurrentFilter;

	// Set up filter (Sql Where Clause) and get Return Sql
	// Sql constructor in m_barang class, m_baranginfo.php

	$m_barang->CurrentFilter = $sWrkFilter;
	$sSql = $m_barang->SQL();
	$conn->raiseErrorFn = 'ew_ErrorFn';
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';
	if ($rs === FALSE) {
		return FALSE;
	} elseif ($rs->EOF) {
		$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
		$rs->Close();
		return FALSE;
	}
	$conn->BeginTrans();

	// Clone old rows
	$rsold = ($rs) ? $rs->GetRows() : array();
	if ($rs) $rs->Close();

	// Call row deleting event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$DeleteRows = $m_barang->Row_Deleting($row);
			if (!$DeleteRows) break;
		}
	}
	if ($DeleteRows) {
		$sKey = "";
		foreach ($rsold as $row) {
			$sThisKey = "";
			if ($sThisKey <> "") $sThisKey .= EW_COMPOSITE_KEY_SEPARATOR;
			$sThisKey .= $row['kode_barang'];
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$DeleteRows = $conn->Execute($m_barang->DeleteSQL($row)); // Delete
			$conn->raiseErrorFn = '';
			if ($DeleteRows === FALSE)
				break;
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}
	} else {

		// Set up error message
		if ($m_barang->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_barang->CancelMessage;
			$m_barang->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Delete cancelled";
		}
	}
	if ($DeleteRows) {
		$conn->CommitTrans(); // Commit the changes
	} else {
		$conn->RollbackTrans(); // Rollback changes
	}

	// Call recordset deleted event
	if ($DeleteRows) {
		foreach ($rsold as $row) {
			$m_barang->Row_Deleted($row);
		}	
	}
	return $DeleteRows;
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_barang;

	// Call Recordset Selecting event
	$m_barang->Recordset_Selecting($m_barang->CurrentFilter);

	// Load list page sql
	$sSql = $m_barang->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_barang->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_barang;
	$sFilter = $m_barang->SqlKeyFilter();
	if (!is_numeric($m_barang->kode_barang->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@kode_barang@", ew_AdjustSql($m_barang->kode_barang->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_barang->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_barang->CurrentFilter = $sFilter;
	$sSql = $m_barang->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_barang->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_barang;
	$m_barang->kode_barang->setDbValue($rs->fields('kode_barang'));
	$m_barang->no_batch->setDbValue($rs->fields('no_batch'));
	$m_barang->expiry->setDbValue($rs->fields('expiry'));
	$m_barang->nama_barang->setDbValue($rs->fields('nama_barang'));
	$m_barang->group_barang->setDbValue($rs->fields('group_barang'));
	$m_barang->farmasi->setDbValue($rs->fields('farmasi'));
	$m_barang->satuan->setDbValue($rs->fields('satuan'));
	$m_barang->harga->setDbValue($rs->fields('harga'));
	$m_barang->hide_when_print->setDbValue($rs->fields('hide_when_print'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_barang;

	// Call Row Rendering event
	$m_barang->Row_Rendering();

	// Common render codes for all row types
	// kode_barang

	$m_barang->kode_barang->CellCssStyle = "";
	$m_barang->kode_barang->CellCssClass = "";

	// no_batch
	$m_barang->no_batch->CellCssStyle = "";
	$m_barang->no_batch->CellCssClass = "";

	// expiry
	$m_barang->expiry->CellCssStyle = "";
	$m_barang->expiry->CellCssClass = "";

	// group_barang
	$m_barang->group_barang->CellCssStyle = "";
	$m_barang->group_barang->CellCssClass = "";

	// farmasi
	$m_barang->farmasi->CellCssStyle = "";
	$m_barang->farmasi->CellCssClass = "";

	// satuan
	$m_barang->satuan->CellCssStyle = "";
	$m_barang->satuan->CellCssClass = "";

	// harga
	$m_barang->harga->CellCssStyle = "";
	$m_barang->harga->CellCssClass = "";

	// hide_when_print
	$m_barang->hide_when_print->CellCssStyle = "";
	$m_barang->hide_when_print->CellCssClass = "";
	if ($m_barang->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode_barang
		$m_barang->kode_barang->ViewValue = $m_barang->kode_barang->CurrentValue;
		$m_barang->kode_barang->CssStyle = "";
		$m_barang->kode_barang->CssClass = "";
		$m_barang->kode_barang->ViewCustomAttributes = "";

		// no_batch
		$m_barang->no_batch->ViewValue = $m_barang->no_batch->CurrentValue;
		$m_barang->no_batch->CssStyle = "";
		$m_barang->no_batch->CssClass = "";
		$m_barang->no_batch->ViewCustomAttributes = "";

		// expiry
		$m_barang->expiry->ViewValue = $m_barang->expiry->CurrentValue;
		$m_barang->expiry->ViewValue = ew_FormatDateTime($m_barang->expiry->ViewValue, 5);
		$m_barang->expiry->CssStyle = "";
		$m_barang->expiry->CssClass = "";
		$m_barang->expiry->ViewCustomAttributes = "";

		// group_barang
		$m_barang->group_barang->CssStyle = "";
		$m_barang->group_barang->CssClass = "";
		$m_barang->group_barang->ViewCustomAttributes = "";

		// farmasi
		$m_barang->farmasi->ViewValue = $m_barang->farmasi->CurrentValue;
		$m_barang->farmasi->CssStyle = "";
		$m_barang->farmasi->CssClass = "";
		$m_barang->farmasi->ViewCustomAttributes = "";

		// satuan
		$m_barang->satuan->ViewValue = $m_barang->satuan->CurrentValue;
		$m_barang->satuan->CssStyle = "";
		$m_barang->satuan->CssClass = "";
		$m_barang->satuan->ViewCustomAttributes = "";

		// harga
		$m_barang->harga->ViewValue = $m_barang->harga->CurrentValue;
		$m_barang->harga->CssStyle = "";
		$m_barang->harga->CssClass = "";
		$m_barang->harga->ViewCustomAttributes = "";

		// hide_when_print
		$m_barang->hide_when_print->ViewValue = $m_barang->hide_when_print->CurrentValue;
		$m_barang->hide_when_print->CssStyle = "";
		$m_barang->hide_when_print->CssClass = "";
		$m_barang->hide_when_print->ViewCustomAttributes = "";

		// kode_barang
		$m_barang->kode_barang->HrefValue = "";

		// no_batch
		$m_barang->no_batch->HrefValue = "";

		// expiry
		$m_barang->expiry->HrefValue = "";

		// group_barang
		$m_barang->group_barang->HrefValue = "";

		// farmasi
		$m_barang->farmasi->HrefValue = "";

		// satuan
		$m_barang->satuan->HrefValue = "";

		// harga
		$m_barang->harga->HrefValue = "";

		// hide_when_print
		$m_barang->hide_when_print->HrefValue = "";
	} elseif ($m_barang->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_barang->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_barang->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_barang->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
