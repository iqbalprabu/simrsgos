<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_dokter', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_dokterinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_dokter->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_dokter->Export; // Get export parameter, used in header
$sExportFile = $m_dokter->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["KDDOKTER"] != "") {
  $m_dokter->KDDOKTER->setQueryStringValue($_GET["KDDOKTER"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $m_dokter->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $m_dokter->CurrentAction = "C"; // Copy Record
  } else {
    $m_dokter->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($m_dokter->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($m_dokter->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$m_dokter->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($m_dokter->KeyUrl($m_dokter->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$m_dokter->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
		elm = fobj.elements["x" + infix + "_KDDOKTER"];
		if (elm && !ew_HasValue(elm)) {
			if (!ew_OnError(elm, "Please enter required field - KDDOKTER"))
				return false;
		}
		elm = fobj.elements["x" + infix + "_KDDOKTER"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - KDDOKTER"))
				return false; 
		}
		elm = fobj.elements["x" + infix + "_KDPOLY"];
		if (elm && !ew_CheckInteger(elm.value)) {
			if (!ew_OnError(elm, "Incorrect integer - KDPOLY"))
				return false; 
		}
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: m dokter<br><br><a href="<?php echo $m_dokter->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="fm_dokteradd" id="fm_dokteradd" action="m_dokteradd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">KDDOKTER<span class='ewmsg'>&nbsp;*</span></td>
    <td<?php echo $m_dokter->KDDOKTER->CellAttributes() ?>><span id="cb_x_KDDOKTER">
<input type="text" name="x_KDDOKTER" id="x_KDDOKTER"  size="30" value="<?php echo $m_dokter->KDDOKTER->EditValue ?>"<?php echo $m_dokter->KDDOKTER->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">KDPOLY</td>
    <td<?php echo $m_dokter->KDPOLY->CellAttributes() ?>><span id="cb_x_KDPOLY">
<input type="text" name="x_KDPOLY" id="x_KDPOLY"  size="30" value="<?php echo $m_dokter->KDPOLY->EditValue ?>"<?php echo $m_dokter->KDPOLY->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableRow">
    <td class="ewTableHeader">NAMADOKTER</td>
    <td<?php echo $m_dokter->NAMADOKTER->CellAttributes() ?>><span id="cb_x_NAMADOKTER">
<input type="text" name="x_NAMADOKTER" id="x_NAMADOKTER"  size="30" maxlength="32" value="<?php echo $m_dokter->NAMADOKTER->EditValue ?>"<?php echo $m_dokter->NAMADOKTER->EditAttributes() ?>>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $m_dokter;
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_dokter;
	$m_dokter->KDDOKTER->setFormValue($objForm->GetValue("x_KDDOKTER"));
	$m_dokter->KDPOLY->setFormValue($objForm->GetValue("x_KDPOLY"));
	$m_dokter->NAMADOKTER->setFormValue($objForm->GetValue("x_NAMADOKTER"));
}

// Restore form values
function RestoreFormValues() {
	global $m_dokter;
	$m_dokter->KDDOKTER->CurrentValue = $m_dokter->KDDOKTER->FormValue;
	$m_dokter->KDPOLY->CurrentValue = $m_dokter->KDPOLY->FormValue;
	$m_dokter->NAMADOKTER->CurrentValue = $m_dokter->NAMADOKTER->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_dokter;
	$sFilter = $m_dokter->SqlKeyFilter();
	if (!is_numeric($m_dokter->KDDOKTER->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@KDDOKTER@", ew_AdjustSql($m_dokter->KDDOKTER->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_dokter->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_dokter->CurrentFilter = $sFilter;
	$sSql = $m_dokter->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_dokter->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_dokter;
	$m_dokter->KDDOKTER->setDbValue($rs->fields('KDDOKTER'));
	$m_dokter->KDPOLY->setDbValue($rs->fields('KDPOLY'));
	$m_dokter->NAMADOKTER->setDbValue($rs->fields('NAMADOKTER'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_dokter;

	// Call Row Rendering event
	$m_dokter->Row_Rendering();

	// Common render codes for all row types
	// KDDOKTER

	$m_dokter->KDDOKTER->CellCssStyle = "";
	$m_dokter->KDDOKTER->CellCssClass = "";

	// KDPOLY
	$m_dokter->KDPOLY->CellCssStyle = "";
	$m_dokter->KDPOLY->CellCssClass = "";

	// NAMADOKTER
	$m_dokter->NAMADOKTER->CellCssStyle = "";
	$m_dokter->NAMADOKTER->CellCssClass = "";
	if ($m_dokter->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_dokter->RowType == EW_ROWTYPE_ADD) { // Add row

		// KDDOKTER
		$m_dokter->KDDOKTER->EditCustomAttributes = "";
		$m_dokter->KDDOKTER->EditValue = ew_HtmlEncode($m_dokter->KDDOKTER->CurrentValue);

		// KDPOLY
		$m_dokter->KDPOLY->EditCustomAttributes = "";
		$m_dokter->KDPOLY->EditValue = ew_HtmlEncode($m_dokter->KDPOLY->CurrentValue);

		// NAMADOKTER
		$m_dokter->NAMADOKTER->EditCustomAttributes = "";
		$m_dokter->NAMADOKTER->EditValue = ew_HtmlEncode($m_dokter->NAMADOKTER->CurrentValue);
	} elseif ($m_dokter->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_dokter->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_dokter->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $m_dokter;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $m_dokter->SqlKeyFilter();
	if (trim(strval($m_dokter->KDDOKTER->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@KDDOKTER@", ew_AdjustSql($m_dokter->KDDOKTER->CurrentValue), $sFilter); // Replace key value
	}
	if (!is_numeric($m_dokter->KDDOKTER->CurrentValue)) {
		$bCheckKey = FALSE;
	}
	if ($bCheckKey) {
		$rsChk = $m_dokter->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field KDDOKTER
	$m_dokter->KDDOKTER->SetDbValueDef($m_dokter->KDDOKTER->CurrentValue, 0);
	$rsnew['KDDOKTER'] =& $m_dokter->KDDOKTER->DbValue;

	// Field KDPOLY
	$m_dokter->KDPOLY->SetDbValueDef($m_dokter->KDPOLY->CurrentValue, NULL);
	$rsnew['KDPOLY'] =& $m_dokter->KDPOLY->DbValue;

	// Field NAMADOKTER
	$m_dokter->NAMADOKTER->SetDbValueDef($m_dokter->NAMADOKTER->CurrentValue, NULL);
	$rsnew['NAMADOKTER'] =& $m_dokter->NAMADOKTER->DbValue;

	// Call Row Inserting event
	$bInsertRow = $m_dokter->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($m_dokter->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($m_dokter->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_dokter->CancelMessage;
			$m_dokter->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {

		// Call Row Inserted event
		$m_dokter->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
