<?php
define("EW_PAGE_ID", "add", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_dpmp', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_dpmpinfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_dpmp->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_dpmp->Export; // Get export parameter, used in header
$sExportFile = $m_dpmp->TableVar; // Get export file, used in header
?>
<?php

// Load key values from QueryString
$bCopy = TRUE;
if (@$_GET["KDMAKANAN"] != "") {
  $m_dpmp->KDMAKANAN->setQueryStringValue($_GET["KDMAKANAN"]);
} else {
  $bCopy = FALSE;
}

// Create form object
$objForm = new cFormObj();

// Process form if post back
if (@$_POST["a_add"] <> "") {
  $m_dpmp->CurrentAction = $_POST["a_add"]; // Get form action
  LoadFormValues(); // Load form values
} else { // Not post back
  if ($bCopy) {
    $m_dpmp->CurrentAction = "C"; // Copy Record
  } else {
    $m_dpmp->CurrentAction = "I"; // Display Blank Record
    LoadDefaultValues(); // Load default values
  }
}

// Perform action based on action code
switch ($m_dpmp->CurrentAction) {
  case "I": // Blank record, no action required
		break;
  case "C": // Copy an existing record
   if (!LoadRow()) { // Load record based on key
      $_SESSION[EW_SESSION_MESSAGE] = "No records found"; // No record found
      Page_Terminate($m_dpmp->getReturnUrl()); // Clean up and return
    }
		break;
  case "A": // ' Add new record
		$m_dpmp->SendEmail = TRUE; // Send email on add success
    if (AddRow()) { // Add successful
      $_SESSION[EW_SESSION_MESSAGE] = "Add New Record Successful"; // Set up success message
      Page_Terminate($m_dpmp->KeyUrl($m_dpmp->getReturnUrl())); // Clean up and return
    } else {
      RestoreFormValues(); // Add failed, restore form values
    }
}

// Render row based on row type
$m_dpmp->RowType = EW_ROWTYPE_ADD;  // Render add type
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "add"; // Page id

//-->
</script>
<script type="text/javascript">
<!--

function ew_ValidateForm(fobj) {
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var i, elm, aelm, infix;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	for (i=0; i<rowcnt; i++) {
		infix = (fobj.key_count) ? String(i+1) : "";
	}
	return true;
}

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script type="text/javascript">
<!--
var ew_MultiPagePage = "Page"; // multi-page Page Text
var ew_MultiPageOf = "of"; // multi-page Of Text
var ew_MultiPagePrev = "Prev"; // multi-page Prev Text
var ew_MultiPageNext = "Next"; // multi-page Next Text

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">Add to TABLE: m dpmp<br><br><a href="<?php echo $m_dpmp->getReturnUrl() ?>">Go Back</a></span></p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") { // Mesasge in Session, display
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
  $_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
}
?>
<form name="fm_dpmpadd" id="fm_dpmpadd" action="m_dpmpadd.php" method="post" onSubmit="return ew_ValidateForm(this);">
<p>
<input type="hidden" name="a_add" id="a_add" value="A">
<table class="ewTable">
  <tr class="ewTableRow">
    <td class="ewTableHeader">GROUPMAKANAN</td>
    <td<?php echo $m_dpmp->GROUPMAKANAN->CellAttributes() ?>><span id="cb_x_GROUPMAKANAN">
<input type="text" name="x_GROUPMAKANAN" id="x_GROUPMAKANAN"  size="30" maxlength="8" value="<?php echo $m_dpmp->GROUPMAKANAN->EditValue ?>"<?php echo $m_dpmp->GROUPMAKANAN->EditAttributes() ?>>
</span></td>
  </tr>
  <tr class="ewTableAltRow">
    <td class="ewTableHeader">NAMAMAKANAN</td>
    <td<?php echo $m_dpmp->NAMAMAKANAN->CellAttributes() ?>><span id="cb_x_NAMAMAKANAN">
<input type="text" name="x_NAMAMAKANAN" id="x_NAMAMAKANAN"  size="30" maxlength="10" value="<?php echo $m_dpmp->NAMAMAKANAN->EditValue ?>"<?php echo $m_dpmp->NAMAMAKANAN->EditAttributes() ?>>
</span></td>
  </tr>
</table>
<p>
<input type="submit" name="btnAction" id="btnAction" value="    Add    ">
</form>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load default values
function LoadDefaultValues() {
	global $m_dpmp;
}
?>
<?php

// Load form values
function LoadFormValues() {

	// Load from form
	global $objForm, $m_dpmp;
	$m_dpmp->GROUPMAKANAN->setFormValue($objForm->GetValue("x_GROUPMAKANAN"));
	$m_dpmp->NAMAMAKANAN->setFormValue($objForm->GetValue("x_NAMAMAKANAN"));
}

// Restore form values
function RestoreFormValues() {
	global $m_dpmp;
	$m_dpmp->GROUPMAKANAN->CurrentValue = $m_dpmp->GROUPMAKANAN->FormValue;
	$m_dpmp->NAMAMAKANAN->CurrentValue = $m_dpmp->NAMAMAKANAN->FormValue;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_dpmp;
	$sFilter = $m_dpmp->SqlKeyFilter();
	if (!is_numeric($m_dpmp->KDMAKANAN->CurrentValue)) {
		return FALSE; // Invalid key, exit
	}
	$sFilter = str_replace("@KDMAKANAN@", ew_AdjustSql($m_dpmp->KDMAKANAN->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_dpmp->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_dpmp->CurrentFilter = $sFilter;
	$sSql = $m_dpmp->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_dpmp->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_dpmp;
	$m_dpmp->KDMAKANAN->setDbValue($rs->fields('KDMAKANAN'));
	$m_dpmp->GROUPMAKANAN->setDbValue($rs->fields('GROUPMAKANAN'));
	$m_dpmp->NAMAMAKANAN->setDbValue($rs->fields('NAMAMAKANAN'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_dpmp;

	// Call Row Rendering event
	$m_dpmp->Row_Rendering();

	// Common render codes for all row types
	// GROUPMAKANAN

	$m_dpmp->GROUPMAKANAN->CellCssStyle = "";
	$m_dpmp->GROUPMAKANAN->CellCssClass = "";

	// NAMAMAKANAN
	$m_dpmp->NAMAMAKANAN->CellCssStyle = "";
	$m_dpmp->NAMAMAKANAN->CellCssClass = "";
	if ($m_dpmp->RowType == EW_ROWTYPE_VIEW) { // View row
	} elseif ($m_dpmp->RowType == EW_ROWTYPE_ADD) { // Add row

		// GROUPMAKANAN
		$m_dpmp->GROUPMAKANAN->EditCustomAttributes = "";
		$m_dpmp->GROUPMAKANAN->EditValue = ew_HtmlEncode($m_dpmp->GROUPMAKANAN->CurrentValue);

		// NAMAMAKANAN
		$m_dpmp->NAMAMAKANAN->EditCustomAttributes = "";
		$m_dpmp->NAMAMAKANAN->EditValue = ew_HtmlEncode($m_dpmp->NAMAMAKANAN->CurrentValue);
	} elseif ($m_dpmp->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_dpmp->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_dpmp->Row_Rendered();
}
?>
<?php

// Add record
function AddRow() {
	global $conn, $Security, $m_dpmp;

	// Check for duplicate key
	$bCheckKey = TRUE;
	$sFilter = $m_dpmp->SqlKeyFilter();
	if (trim(strval($m_dpmp->KDMAKANAN->CurrentValue)) == "") {
		$bCheckKey = FALSE;
	} else {
		$sFilter = str_replace("@KDMAKANAN@", ew_AdjustSql($m_dpmp->KDMAKANAN->CurrentValue), $sFilter); // Replace key value
	}
	if (!is_numeric($m_dpmp->KDMAKANAN->CurrentValue)) {
		$bCheckKey = FALSE;
	}
	if ($bCheckKey) {
		$rsChk = $m_dpmp->LoadRs($sFilter);
		if ($rsChk && !$rsChk->EOF) {
			$_SESSION[EW_SESSION_MESSAGE] = "Duplicate value for primary key";
			$rsChk->Close();
			return FALSE;
		}
	}
	$rsnew = array();

	// Field GROUPMAKANAN
	$m_dpmp->GROUPMAKANAN->SetDbValueDef($m_dpmp->GROUPMAKANAN->CurrentValue, NULL);
	$rsnew['GROUPMAKANAN'] =& $m_dpmp->GROUPMAKANAN->DbValue;

	// Field NAMAMAKANAN
	$m_dpmp->NAMAMAKANAN->SetDbValueDef($m_dpmp->NAMAMAKANAN->CurrentValue, NULL);
	$rsnew['NAMAMAKANAN'] =& $m_dpmp->NAMAMAKANAN->DbValue;

	// Call Row Inserting event
	$bInsertRow = $m_dpmp->Row_Inserting($rsnew);
	if ($bInsertRow) {
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$AddRow = $conn->Execute($m_dpmp->InsertSQL($rsnew));
		$conn->raiseErrorFn = '';
	} else {
		if ($m_dpmp->CancelMessage <> "") {
			$_SESSION[EW_SESSION_MESSAGE] = $m_dpmp->CancelMessage;
			$m_dpmp->CancelMessage = "";
		} else {
			$_SESSION[EW_SESSION_MESSAGE] = "Insert cancelled";
		}
		$AddRow = FALSE;
	}
	if ($AddRow) {
		$m_dpmp->KDMAKANAN->setDbValue($conn->Insert_ID());
		$rsnew['KDMAKANAN'] =& $m_dpmp->KDMAKANAN->DbValue;

		// Call Row Inserted event
		$m_dpmp->Row_Inserted($rsnew);
	}
	return $AddRow;
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
