<?php
define("EW_PAGE_ID", "list", TRUE); // Page ID
define("EW_TABLE_NAME", 'm_login', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "m_logininfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$m_login->Export = @$_GET["export"]; // Get export parameter
$sExport = $m_login->Export; // Get export parameter, used in header
$sExportFile = $m_login->TableVar; // Get export file, used in header
?>
<?php
?>
<?php

// Paging variables
$nStartRec = 0; // Start record index
$nStopRec = 0; // Stop record index
$nTotalRecs = 0; // Total number of records
$nDisplayRecs = 20;
$nRecRange = 10;
$nRecCount = 0; // Record count

// Search filters
$sSrchAdvanced = ""; // Advanced search filter
$sSrchBasic = ""; // Basic search filter
$sSrchWhere = ""; // Search where clause
$sFilter = "";

// Master/Detail
$sDbMasterFilter = ""; // Master filter
$sDbDetailFilter = ""; // Detail filter
$sSqlMaster = ""; // Sql for master record

// Handle reset command
ResetCmd();

// Get basic search criteria
$sSrchBasic = BasicSearchWhere();

// Build search criteria
if ($sSrchAdvanced <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchAdvanced . ")";
}
if ($sSrchBasic <> "") {
	if ($sSrchWhere <> "") $sSrchWhere .= " AND ";
	$sSrchWhere .= "(" . $sSrchBasic . ")";
}

// Save search criteria
if ($sSrchWhere <> "") {
	if ($sSrchBasic == "") ResetBasicSearchParms();
	$m_login->setSearchWhere($sSrchWhere); // Save to Session
	$nStartRec = 1; // Reset start record counter
	$m_login->setStartRecordNumber($nStartRec);
} else {
	RestoreSearchParms();
}

// Build filter
$sFilter = "";
if ($sDbDetailFilter <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sDbDetailFilter . ")";
}
if ($sSrchWhere <> "") {
	if ($sFilter <> "") $sFilter .= " AND ";
	$sFilter .= "(" . $sSrchWhere . ")";
}

// Set up filter in Session
$m_login->setSessionWhere($sFilter);
$m_login->CurrentFilter = "";

// Set Up Sorting Order
SetUpSortOrder();

// Set Return Url
$m_login->setReturnUrl("m_loginlist.php");
?>
<?php include "header.php" ?>
<?php if ($m_login->Export == "") { ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "list"; // Page id

//-->
</script>
<script type="text/javascript">
<!--
var firstrowoffset = 1; // First data row start at
var lastrowoffset = 0; // Last data row end at
var EW_LIST_TABLE_NAME = 'ewlistmain'; // Table name for list page
var rowclass = 'ewTableRow'; // Row class
var rowaltclass = 'ewTableAltRow'; // Row alternate class
var rowmoverclass = 'ewTableHighlightRow'; // Row mouse over class
var rowselectedclass = 'ewTableSelectRow'; // Row selected class
var roweditclass = 'ewTableEditRow'; // Row edit class

//-->
</script>
<script type="text/javascript">
<!--

// js for DHtml Editor
//-->

</script>
<script type="text/javascript">
<!--

// js for Popup Calendar
//-->

</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<?php } ?>
<?php if ($m_login->Export == "") { ?>
<?php } ?>
<?php

// Load recordset
$bExportAll = (defined("EW_EXPORT_ALL") && $m_login->Export <> "");
$bSelectLimit = ($m_login->Export == "" && $m_login->SelectLimit);
if (!$bSelectLimit) $rs = LoadRecordset();
$nTotalRecs = ($bSelectLimit) ? $m_login->SelectRecordCount() : $rs->RecordCount();
$nStartRec = 1;
if ($nDisplayRecs <= 0) $nDisplayRecs = $nTotalRecs; // Display all records
if (!$bExportAll) SetUpStartRec(); // Set up start record position
if ($bSelectLimit) $rs = LoadRecordset($nStartRec-1, $nDisplayRecs);
?>
<p><span class="phpmaker" style="white-space: nowrap;">TABLE: m login
</span></p>
<?php if ($m_login->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<form name="fm_loginlistsrch" id="fm_loginlistsrch" action="m_loginlist.php" >
<table class="ewBasicSearch">
	<tr>
		<td><span class="phpmaker">
			<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" size="20" value="<?php echo ew_HtmlEncode($m_login->getBasicSearchKeyword()) ?>">
			<input type="Submit" name="Submit" id="Submit" value="Search (*)">&nbsp;
			<a href="m_loginlist.php?cmd=reset">Show all</a>&nbsp;
		</span></td>
	</tr>
	<tr>
	<td><span class="phpmaker"><input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="" <?php if ($m_login->getBasicSearchType() == "") { ?>checked<?php } ?>>Exact phrase&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="AND" <?php if ($m_login->getBasicSearchType() == "AND") { ?>checked<?php } ?>>All words&nbsp;&nbsp;<input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="OR" <?php if ($m_login->getBasicSearchType() == "OR") { ?>checked<?php } ?>>Any word</span></td>
	</tr>
</table>
</form>
<?php } ?>
<?php } ?>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<form method="post" name="fm_loginlist" id="fm_loginlist">
<?php if ($m_login->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_loginadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php if ($nTotalRecs > 0) { ?>
<table id="ewlistmain" class="ewTable">
<?php
	$OptionCnt = 0;
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // view
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // edit
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // copy
}
if ($Security->IsLoggedIn()) {
	$OptionCnt++; // delete
}
?>
	<!-- Table header -->
	<tr class="ewTableHeader">
		<td valign="top">
<?php if ($m_login->Export <> "") { ?>
NIP
<?php } else { ?>
	<a href="m_loginlist.php?order=<?php echo urlencode('NIP') ?>&ordertype=<?php echo $m_login->NIP->ReverseSort() ?>">NIP&nbsp;(*)<?php if ($m_login->NIP->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_login->NIP->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_login->Export <> "") { ?>
PWD
<?php } else { ?>
	<a href="m_loginlist.php?order=<?php echo urlencode('PWD') ?>&ordertype=<?php echo $m_login->PWD->ReverseSort() ?>">PWD&nbsp;(*)<?php if ($m_login->PWD->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_login->PWD->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_login->Export <> "") { ?>
SES REG
<?php } else { ?>
	<a href="m_loginlist.php?order=<?php echo urlencode('SES_REG') ?>&ordertype=<?php echo $m_login->SES_REG->ReverseSort() ?>">SES REG&nbsp;(*)<?php if ($m_login->SES_REG->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_login->SES_REG->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_login->Export <> "") { ?>
ROLES
<?php } else { ?>
	<a href="m_loginlist.php?order=<?php echo urlencode('ROLES') ?>&ordertype=<?php echo $m_login->ROLES->ReverseSort() ?>">ROLES<?php if ($m_login->ROLES->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_login->ROLES->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_login->Export <> "") { ?>
KDUNIT
<?php } else { ?>
	<a href="m_loginlist.php?order=<?php echo urlencode('KDUNIT') ?>&ordertype=<?php echo $m_login->KDUNIT->ReverseSort() ?>">KDUNIT<?php if ($m_login->KDUNIT->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_login->KDUNIT->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
		<td valign="top">
<?php if ($m_login->Export <> "") { ?>
DEPARTEMEN
<?php } else { ?>
	<a href="m_loginlist.php?order=<?php echo urlencode('DEPARTEMEN') ?>&ordertype=<?php echo $m_login->DEPARTEMEN->ReverseSort() ?>">DEPARTEMEN&nbsp;(*)<?php if ($m_login->DEPARTEMEN->getSort() == "ASC") { ?><img src="images/sortup.gif" width="10" height="9" border="0"><?php } elseif ($m_login->DEPARTEMEN->getSort() == "DESC") { ?><img src="images/sortdown.gif" width="10" height="9" border="0"><?php } ?></a>
<?php } ?>
		</td>
<?php if ($m_login->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap>&nbsp;</td>
<?php } ?>
<?php } ?>
	</tr>
<?php
if (defined("EW_EXPORT_ALL") && $m_login->Export <> "") {
	$nStopRec = $nTotalRecs;
} else {
	$nStopRec = $nStartRec + $nDisplayRecs - 1; // Set the last record to display
}
$nRecCount = $nStartRec - 1;
if (!$rs->EOF) {
	$rs->MoveFirst();
	if (!$m_login->SelectLimit) $rs->Move($nStartRec - 1); // Move to first record directly
}
$RowCnt = 0;
while (!$rs->EOF && $nRecCount < $nStopRec) {
	$nRecCount++;
	if (intval($nRecCount) >= intval($nStartRec)) {
		$RowCnt++;

	// Init row class and style
	$m_login->CssClass = "ewTableRow";
	$m_login->CssStyle = "";

	// Init row event
	$m_login->RowClientEvents = "onmouseover='ew_MouseOver(this);' onmouseout='ew_MouseOut(this);' onclick='ew_Click(this);'";

	// Display alternate color for rows
	if ($RowCnt % 2 == 0) {
		$m_login->CssClass = "ewTableAltRow";
	}
	LoadRowValues($rs); // Load row values
	$m_login->RowType = EW_ROWTYPE_VIEW; // Render view
	RenderRow();
?>
	<!-- Table body -->
	<tr<?php echo $m_login->DisplayAttributes() ?>>
		<!-- NIP -->
		<td<?php echo $m_login->NIP->CellAttributes() ?>>
<div<?php echo $m_login->NIP->ViewAttributes() ?>><?php echo $m_login->NIP->ViewValue ?></div>
</td>
		<!-- PWD -->
		<td<?php echo $m_login->PWD->CellAttributes() ?>>
<div<?php echo $m_login->PWD->ViewAttributes() ?>><?php echo $m_login->PWD->ViewValue ?></div>
</td>
		<!-- SES_REG -->
		<td<?php echo $m_login->SES_REG->CellAttributes() ?>>
<div<?php echo $m_login->SES_REG->ViewAttributes() ?>><?php echo $m_login->SES_REG->ViewValue ?></div>
</td>
		<!-- ROLES -->
		<td<?php echo $m_login->ROLES->CellAttributes() ?>>
<div<?php echo $m_login->ROLES->ViewAttributes() ?>><?php echo $m_login->ROLES->ViewValue ?></div>
</td>
		<!-- KDUNIT -->
		<td<?php echo $m_login->KDUNIT->CellAttributes() ?>>
<div<?php echo $m_login->KDUNIT->ViewAttributes() ?>><?php echo $m_login->KDUNIT->ViewValue ?></div>
</td>
		<!-- DEPARTEMEN -->
		<td<?php echo $m_login->DEPARTEMEN->CellAttributes() ?>>
<div<?php echo $m_login->DEPARTEMEN->ViewAttributes() ?>><?php echo $m_login->DEPARTEMEN->ViewValue ?></div>
</td>
<?php if ($m_login->Export == "") { ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_login->ViewUrl() ?>">View</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_login->EditUrl() ?>">Edit</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_login->CopyUrl() ?>">Copy</a>
</span></td>
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<td nowrap><span class="phpmaker">
<a href="<?php echo $m_login->DeleteUrl() ?>">Delete</a>
</span></td>
<?php } ?>
<?php } ?>
	</tr>
<?php
	}
	$rs->MoveNext();
}
?>
</table>
<?php if ($m_login->Export == "") { ?>
<table>
	<tr><td><span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<a href="m_loginadd.php">Add</a>&nbsp;&nbsp;
<?php } ?>
	</span></td></tr>
</table>
<?php } ?>
<?php } ?>
</form>
<?php

// Close recordset and connection
if ($rs) $rs->Close();
?>
<?php if ($m_login->Export == "") { ?>
<form action="m_loginlist.php" name="ewpagerform" id="ewpagerform">
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td nowrap>
<?php if (!isset($Pager)) $Pager = new cPrevNextPager($nStartRec, $nDisplayRecs, $nTotalRecs) ?>
<?php if ($Pager->RecordCount > 0) { ?>
	<table border="0" cellspacing="0" cellpadding="0"><tr><td><span class="phpmaker">Page&nbsp;</span></td>
<!--first page button-->
	<?php if ($Pager->FirstButton->Enabled) { ?>
	<td><a href="m_loginlist.php?start=<?php echo $Pager->FirstButton->Start ?>"><img src="images/first.gif" alt="First" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/firstdisab.gif" alt="First" width="16" height="16" border="0"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($Pager->PrevButton->Enabled) { ?>
	<td><a href="m_loginlist.php?start=<?php echo $Pager->PrevButton->Start ?>"><img src="images/prev.gif" alt="Previous" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/prevdisab.gif" alt="Previous" width="16" height="16" border="0"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($Pager->NextButton->Enabled) { ?>
	<td><a href="m_loginlist.php?start=<?php echo $Pager->NextButton->Start ?>"><img src="images/next.gif" alt="Next" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/nextdisab.gif" alt="Next" width="16" height="16" border="0"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($Pager->LastButton->Enabled) { ?>
	<td><a href="m_loginlist.php?start=<?php echo $Pager->LastButton->Start ?>"><img src="images/last.gif" alt="Last" width="16" height="16" border="0"></a></td>
	<?php } else { ?>
	<td><img src="images/lastdisab.gif" alt="Last" width="16" height="16" border="0"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;of <?php echo $Pager->PageCount ?></span></td>
	</tr></table>
	<span class="phpmaker">Records <?php echo $Pager->FromIndex ?> to <?php echo $Pager->ToIndex ?> of <?php echo $Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($sSrchWhere == "0=101") { ?>
	<span class="phpmaker">Please enter search criteria</span>
	<?php } else { ?>
	<span class="phpmaker">No records found</span>
	<?php } ?>
<?php } ?>
		</td>
	</tr>
</table>
</form>
<?php } ?>
<?php if ($m_login->Export == "") { ?>
<?php } ?>
<?php if ($m_login->Export == "") { ?>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php } ?>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Return Basic Search sql
function BasicSearchSQL($Keyword) {
	$sKeyword = ew_AdjustSql($Keyword);
	$sql = "";
	$sql .= "NIP LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "PWD LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "SES_REG LIKE '%" . $sKeyword . "%' OR ";
	$sql .= "DEPARTEMEN LIKE '%" . $sKeyword . "%' OR ";
	if (substr($sql, -4) == " OR ") $sql = substr($sql, 0, strlen($sql)-4);
	return $sql;
}

// Return Basic Search Where based on search keyword and type
function BasicSearchWhere() {
	global $Security, $m_login;
	$sSearchStr = "";
	$sSearchKeyword = ew_StripSlashes(@$_GET[EW_TABLE_BASIC_SEARCH]);
	$sSearchType = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	if ($sSearchKeyword <> "") {
		$sSearch = trim($sSearchKeyword);
		if ($sSearchType <> "") {
			while (strpos($sSearch, "  ") !== FALSE)
				$sSearch = str_replace("  ", " ", $sSearch);
			$arKeyword = explode(" ", trim($sSearch));
			foreach ($arKeyword as $sKeyword) {
				if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
				$sSearchStr .= "(" . BasicSearchSQL($sKeyword) . ")";
			}
		} else {
			$sSearchStr = BasicSearchSQL($sSearch);
		}
	}
	if ($sSearchKeyword <> "") {
		$m_login->setBasicSearchKeyword($sSearchKeyword);
		$m_login->setBasicSearchType($sSearchType);
	}
	return $sSearchStr;
}

// Clear all search parameters
function ResetSearchParms() {

	// Clear search where
	global $m_login;
	$sSrchWhere = "";
	$m_login->setSearchWhere($sSrchWhere);

	// Clear basic search parameters
	ResetBasicSearchParms();
}

// Clear all basic search parameters
function ResetBasicSearchParms() {

	// Clear basic search parameters
	global $m_login;
	$m_login->setBasicSearchKeyword("");
	$m_login->setBasicSearchType("");
}

// Restore all search parameters
function RestoreSearchParms() {
	global $sSrchWhere, $m_login;
	$sSrchWhere = $m_login->getSearchWhere();
}

// Set up Sort parameters based on Sort Links clicked
function SetUpSortOrder() {
	global $m_login;

	// Check for an Order parameter
	if (@$_GET["order"] <> "") {
		$m_login->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
		$m_login->CurrentOrderType = @$_GET["ordertype"];

		// Field NIP
		$m_login->UpdateSort($m_login->NIP);

		// Field PWD
		$m_login->UpdateSort($m_login->PWD);

		// Field SES_REG
		$m_login->UpdateSort($m_login->SES_REG);

		// Field ROLES
		$m_login->UpdateSort($m_login->ROLES);

		// Field KDUNIT
		$m_login->UpdateSort($m_login->KDUNIT);

		// Field DEPARTEMEN
		$m_login->UpdateSort($m_login->DEPARTEMEN);
		$m_login->setStartRecordNumber(1); // Reset start position
	}
	$sOrderBy = $m_login->getSessionOrderBy(); // Get order by from Session
	if ($sOrderBy == "") {
		if ($m_login->SqlOrderBy() <> "") {
			$sOrderBy = $m_login->SqlOrderBy();
			$m_login->setSessionOrderBy($sOrderBy);
		}
	}
}

// Reset command based on querystring parameter cmd=
// - RESET: reset search parameters
// - RESETALL: reset search & master/detail parameters
// - RESETSORT: reset sort parameters
function ResetCmd() {
	global $sDbMasterFilter, $sDbDetailFilter, $nStartRec, $sOrderBy;
	global $m_login;

	// Get reset cmd
	if (@$_GET["cmd"] <> "") {
		$sCmd = $_GET["cmd"];

		// Reset search criteria
		if (strtolower($sCmd) == "reset" || strtolower($sCmd) == "resetall") {
			ResetSearchParms();
		}

		// Reset Sort Criteria
		if (strtolower($sCmd) == "resetsort") {
			$sOrderBy = "";
			$m_login->setSessionOrderBy($sOrderBy);
			$m_login->NIP->setSort("");
			$m_login->PWD->setSort("");
			$m_login->SES_REG->setSort("");
			$m_login->ROLES->setSort("");
			$m_login->KDUNIT->setSort("");
			$m_login->DEPARTEMEN->setSort("");
		}

		// Reset start position
		$nStartRec = 1;
		$m_login->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $m_login;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$m_login->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$m_login->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $m_login->getStartRecordNumber();
		}
	} else {
		$nStartRec = $m_login->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$m_login->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$m_login->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$m_login->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Load recordset
function LoadRecordset($offset = -1, $rowcnt = -1) {
	global $conn, $m_login;

	// Call Recordset Selecting event
	$m_login->Recordset_Selecting($m_login->CurrentFilter);

	// Load list page sql
	$sSql = $m_login->SelectSQL();
	if ($offset > -1 && $rowcnt > -1) $sSql .= " LIMIT $offset, $rowcnt";

	// Load recordset
	$conn->raiseErrorFn = 'ew_ErrorFn';	
	$rs = $conn->Execute($sSql);
	$conn->raiseErrorFn = '';

	// Call Recordset Selected event
	$m_login->Recordset_Selected($rs);
	return $rs;
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $m_login;
	$sFilter = $m_login->SqlKeyFilter();
	$sFilter = str_replace("@NIP@", ew_AdjustSql($m_login->NIP->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$m_login->Row_Selecting($sFilter);

	// Load sql based on filter
	$m_login->CurrentFilter = $sFilter;
	$sSql = $m_login->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$m_login->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $m_login;
	$m_login->NIP->setDbValue($rs->fields('NIP'));
	$m_login->PWD->setDbValue($rs->fields('PWD'));
	$m_login->SES_REG->setDbValue($rs->fields('SES_REG'));
	$m_login->ROLES->setDbValue($rs->fields('ROLES'));
	$m_login->KDUNIT->setDbValue($rs->fields('KDUNIT'));
	$m_login->DEPARTEMEN->setDbValue($rs->fields('DEPARTEMEN'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $m_login;

	// Call Row Rendering event
	$m_login->Row_Rendering();

	// Common render codes for all row types
	// NIP

	$m_login->NIP->CellCssStyle = "";
	$m_login->NIP->CellCssClass = "";

	// PWD
	$m_login->PWD->CellCssStyle = "";
	$m_login->PWD->CellCssClass = "";

	// SES_REG
	$m_login->SES_REG->CellCssStyle = "";
	$m_login->SES_REG->CellCssClass = "";

	// ROLES
	$m_login->ROLES->CellCssStyle = "";
	$m_login->ROLES->CellCssClass = "";

	// KDUNIT
	$m_login->KDUNIT->CellCssStyle = "";
	$m_login->KDUNIT->CellCssClass = "";

	// DEPARTEMEN
	$m_login->DEPARTEMEN->CellCssStyle = "";
	$m_login->DEPARTEMEN->CellCssClass = "";
	if ($m_login->RowType == EW_ROWTYPE_VIEW) { // View row

		// NIP
		$m_login->NIP->ViewValue = $m_login->NIP->CurrentValue;
		$m_login->NIP->CssStyle = "";
		$m_login->NIP->CssClass = "";
		$m_login->NIP->ViewCustomAttributes = "";

		// PWD
		$m_login->PWD->ViewValue = $m_login->PWD->CurrentValue;
		$m_login->PWD->CssStyle = "";
		$m_login->PWD->CssClass = "";
		$m_login->PWD->ViewCustomAttributes = "";

		// SES_REG
		$m_login->SES_REG->ViewValue = $m_login->SES_REG->CurrentValue;
		$m_login->SES_REG->CssStyle = "";
		$m_login->SES_REG->CssClass = "";
		$m_login->SES_REG->ViewCustomAttributes = "";

		// ROLES
		$m_login->ROLES->ViewValue = $m_login->ROLES->CurrentValue;
		$m_login->ROLES->CssStyle = "";
		$m_login->ROLES->CssClass = "";
		$m_login->ROLES->ViewCustomAttributes = "";

		// KDUNIT
		$m_login->KDUNIT->ViewValue = $m_login->KDUNIT->CurrentValue;
		$m_login->KDUNIT->CssStyle = "";
		$m_login->KDUNIT->CssClass = "";
		$m_login->KDUNIT->ViewCustomAttributes = "";

		// DEPARTEMEN
		$m_login->DEPARTEMEN->ViewValue = $m_login->DEPARTEMEN->CurrentValue;
		$m_login->DEPARTEMEN->CssStyle = "";
		$m_login->DEPARTEMEN->CssClass = "";
		$m_login->DEPARTEMEN->ViewCustomAttributes = "";

		// NIP
		$m_login->NIP->HrefValue = "";

		// PWD
		$m_login->PWD->HrefValue = "";

		// SES_REG
		$m_login->SES_REG->HrefValue = "";

		// ROLES
		$m_login->ROLES->HrefValue = "";

		// KDUNIT
		$m_login->KDUNIT->HrefValue = "";

		// DEPARTEMEN
		$m_login->DEPARTEMEN->HrefValue = "";
	} elseif ($m_login->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($m_login->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($m_login->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$m_login->Row_Rendered();
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
