<?php
define("EW_PAGE_ID", "view", TRUE); // Page ID
define("EW_TABLE_NAME", 'icd_cm', TRUE);
?>
<?php
session_start(); // Initialize session data
ob_start(); // Turn on output buffering
?>
<?php include "ewcfg50.php" ?>
<?php include "ewmysql50.php" ?>
<?php include "phpfn50.php" ?>
<?php include "icd_cminfo.php" ?>
<?php include "userfn50.php" ?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>
<?php

// Open connection to the database
$conn = ew_Connect();
?>
<?php
$Security = new cAdvancedSecurity();
?>
<?php
if (!$Security->IsLoggedIn()) $Security->AutoLogin();
if (!$Security->IsLoggedIn()) {
	$Security->SaveLastUrl();
	Page_Terminate("login.php");
}
?>
<?php

// Common page loading event (in userfn*.php)
Page_Loading();
?>
<?php

// Page load event, used in current page
Page_Load();
?>
<?php
$icd_cm->Export = @$_GET["export"]; // Get export parameter
$sExport = $icd_cm->Export; // Get export parameter, used in header
$sExportFile = $icd_cm->TableVar; // Get export file, used in header
?>
<?php
if (@$_GET["kode"] <> "") {
	$icd_cm->kode->setQueryStringValue($_GET["kode"]);
} else {
	Page_Terminate("icd_cmlist.php"); // Return to list page
}

// Get action
if (@$_POST["a_view"] <> "") {
	$icd_cm->CurrentAction = $_POST["a_view"];
} else {
	$icd_cm->CurrentAction = "I"; // Display form
}
switch ($icd_cm->CurrentAction) {
	case "I": // Get a record to display
		if (!LoadRow()) { // Load record based on key
			$_SESSION[EW_SESSION_MESSAGE] = "No records found"; // Set no record message
			Page_Terminate("icd_cmlist.php"); // Return to list
		}
}

// Set return url
$icd_cm->setReturnUrl("icd_cmview.php");

// Render row
$icd_cm->RowType = EW_ROWTYPE_VIEW;
RenderRow();
?>
<?php include "header.php" ?>
<script type="text/javascript">
<!--
var EW_PAGE_ID = "view"; // Page id

//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--

// Write your client script here, no need to add script tags.
// To include another .js script, use:
// ew_ClientScriptInclude("my_javascript.js"); 
//-->

</script>
<p><span class="phpmaker">View TABLE: icd cm
<br><br>
<a href="icd_cmlist.php">Back to List</a>&nbsp;
<?php if ($Security->IsLoggedIn()) { ?>
<a href="icd_cmadd.php">Add</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $icd_cm->EditUrl() ?>">Edit</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $icd_cm->CopyUrl() ?>">Copy</a>&nbsp;
<?php } ?>
<?php if ($Security->IsLoggedIn()) { ?>
<a href="<?php echo $icd_cm->DeleteUrl() ?>">Delete</a>&nbsp;
<?php } ?>
</span>
</p>
<?php
if (@$_SESSION[EW_SESSION_MESSAGE] <> "") {
?>
<p><span class="ewmsg"><?php echo $_SESSION[EW_SESSION_MESSAGE] ?></span></p>
<?php
	$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message
}
?>
<p>
<form>
<table class="ewTable">
	<tr class="ewTableRow">
		<td class="ewTableHeader">kode</td>
		<td<?php echo $icd_cm->kode->CellAttributes() ?>>
<div<?php echo $icd_cm->kode->ViewAttributes() ?>><?php echo $icd_cm->kode->ViewValue ?></div>
</td>
	</tr>
	<tr class="ewTableAltRow">
		<td class="ewTableHeader">keterangan</td>
		<td<?php echo $icd_cm->keterangan->CellAttributes() ?>>
<div<?php echo $icd_cm->keterangan->ViewAttributes() ?>><?php echo $icd_cm->keterangan->ViewValue ?></div>
</td>
	</tr>
</table>
</form>
<p>
<script language="JavaScript" type="text/javascript">
<!--

// Write your table-specific startup script here
// document.write("page loaded");
//-->

</script>
<?php include "footer.php" ?>
<?php

// If control is passed here, simply terminate the page without redirect
Page_Terminate();

// -----------------------------------------------------------------
//  Subroutine Page_Terminate
//  - called when exit page
//  - clean up connection and objects
//  - if url specified, redirect to url, otherwise end response
function Page_Terminate($url = "") {
	global $conn;

	// Page unload event, used in current page
	Page_Unload();

	// Global page unloaded event (in userfn*.php)
	Page_Unloaded();

	 // Close Connection
	$conn->Close();

	// Go to url if specified
	if ($url <> "") {
		ob_end_clean();
		header("Location: $url");
	}
	exit();
}
?>
<?php

// Load row based on key values
function LoadRow() {
	global $conn, $Security, $icd_cm;
	$sFilter = $icd_cm->SqlKeyFilter();
	$sFilter = str_replace("@kode@", ew_AdjustSql($icd_cm->kode->CurrentValue), $sFilter); // Replace key value

	// Call Row Selecting event
	$icd_cm->Row_Selecting($sFilter);

	// Load sql based on filter
	$icd_cm->CurrentFilter = $sFilter;
	$sSql = $icd_cm->SQL();
	if ($rs = $conn->Execute($sSql)) {
		if ($rs->EOF) {
			$LoadRow = FALSE;
		} else {
			$LoadRow = TRUE;
			$rs->MoveFirst();
			LoadRowValues($rs); // Load row values

			// Call Row Selected event
			$icd_cm->Row_Selected($rs);
		}
		$rs->Close();
	} else {
		$LoadRow = FALSE;
	}
	return $LoadRow;
}

// Load row values from recordset
function LoadRowValues(&$rs) {
	global $icd_cm;
	$icd_cm->kode->setDbValue($rs->fields('kode'));
	$icd_cm->keterangan->setDbValue($rs->fields('keterangan'));
}
?>
<?php

// Render row values based on field settings
function RenderRow() {
	global $conn, $Security, $icd_cm;

	// Call Row Rendering event
	$icd_cm->Row_Rendering();

	// Common render codes for all row types
	// kode

	$icd_cm->kode->CellCssStyle = "";
	$icd_cm->kode->CellCssClass = "";

	// keterangan
	$icd_cm->keterangan->CellCssStyle = "";
	$icd_cm->keterangan->CellCssClass = "";
	if ($icd_cm->RowType == EW_ROWTYPE_VIEW) { // View row

		// kode
		$icd_cm->kode->ViewValue = $icd_cm->kode->CurrentValue;
		$icd_cm->kode->CssStyle = "";
		$icd_cm->kode->CssClass = "";
		$icd_cm->kode->ViewCustomAttributes = "";

		// keterangan
		$icd_cm->keterangan->ViewValue = $icd_cm->keterangan->CurrentValue;
		if (!is_null($icd_cm->keterangan->ViewValue)) $icd_cm->keterangan->ViewValue = str_replace("\n", "<br>", $icd_cm->keterangan->ViewValue); 
		$icd_cm->keterangan->CssStyle = "";
		$icd_cm->keterangan->CssClass = "";
		$icd_cm->keterangan->ViewCustomAttributes = "";

		// kode
		$icd_cm->kode->HrefValue = "";

		// keterangan
		$icd_cm->keterangan->HrefValue = "";
	} elseif ($icd_cm->RowType == EW_ROWTYPE_ADD) { // Add row
	} elseif ($icd_cm->RowType == EW_ROWTYPE_EDIT) { // Edit row
	} elseif ($icd_cm->RowType == EW_ROWTYPE_SEARCH) { // Search row
	}

	// Call Row Rendered event
	$icd_cm->Row_Rendered();
}
?>
<?php

// Set up Starting Record parameters based on Pager Navigation
function SetUpStartRec() {
	global $nDisplayRecs, $nStartRec, $nTotalRecs, $nPageNo, $icd_cm;
	if ($nDisplayRecs == 0) return;

	// Check for a START parameter
	if (@$_GET[EW_TABLE_START_REC] <> "") {
		$nStartRec = $_GET[EW_TABLE_START_REC];
		$icd_cm->setStartRecordNumber($nStartRec);
	} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
		$nPageNo = $_GET[EW_TABLE_PAGE_NO];
		if (is_numeric($nPageNo)) {
			$nStartRec = ($nPageNo-1)*$nDisplayRecs+1;
			if ($nStartRec <= 0) {
				$nStartRec = 1;
			} elseif ($nStartRec >= intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1) {
				$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1;
			}
			$icd_cm->setStartRecordNumber($nStartRec);
		} else {
			$nStartRec = $icd_cm->getStartRecordNumber();
		}
	} else {
		$nStartRec = $icd_cm->getStartRecordNumber();
	}

	// Check if correct start record counter
	if (!is_numeric($nStartRec) || $nStartRec == "") { // Avoid invalid start record counter
		$nStartRec = 1; // Reset start record counter
		$icd_cm->setStartRecordNumber($nStartRec);
	} elseif (intval($nStartRec) > intval($nTotalRecs)) { // Avoid starting record > total records
		$nStartRec = intval(($nTotalRecs-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to last page first record
		$icd_cm->setStartRecordNumber($nStartRec);
	} elseif (($nStartRec-1) % $nDisplayRecs <> 0) {
		$nStartRec = intval(($nStartRec-1)/$nDisplayRecs)*$nDisplayRecs+1; // Point to page boundary
		$icd_cm->setStartRecordNumber($nStartRec);
	}
}
?>
<?php

// Page Load event
function Page_Load() {

	//echo "Page Load";
}

// Page Unload event
function Page_Unload() {

	//echo "Page Unload";
}
?>
