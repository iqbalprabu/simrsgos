<?php
require_once('./ps_pagination_x.php');
$sql	= mysql_query("SELECT * FROM t_diagnosakep WHERE NOMR ='".$_GET['NOMR']."' and idadmission = '".$_GET['idadmission']."' ORDER BY id_diagnosakep");
$data	= mysql_fetch_array($sql);
if(empty($_REQUEST['PERID'])){
	$edit = "no";
} else {
	$edit = "ok";
}
?>
<script>
/*
	Masked Input plugin for jQuery
	Copyright (c) 2007-2011 Josh Bush (digitalbush.com)
	Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license) 
	Version: 1.3
*/
jQuery(document).ready(function(){
	jQuery("#ID_DOMAIN").change(function(){
		var selectValues = jQuery("#ID_DOMAIN").val();
		jQuery.post('./include/ajaxload.php',{iddomain:selectValues,load_diagnosis:'true'},function(data){
			jQuery('#diagnosispilih').html(data);
			jQuery('#dafgejalapilih').html("<select name=\"ID_SUB_VAR1\" class=\"text required\" title=\"*\" id=\"ID_SUB_VAR1\"><option value=\"0\"> --pilih-- </option></select>");
			jQuery('#dafetiologipilih').html("<select name=\"ID_SUB_VAR2\" class=\"text required\" title=\"*\" id=\"ID_SUB_VAR2\"><option value=\"0\"> --pilih-- </option></select>");
		});
	});
	
	jQuery("#ID_DIAGNOSIS").change(function(){
		var selectValues = jQuery("#ID_DIAGNOSIS").val();
		jQuery.post('./include/ajaxload.php',{kdID_DIAGNOSIS:selectValues,load_kecamatan:'true'},function(data){
			jQuery('#dafgejalapilih').html(data);
			jQuery('#dafetiologipilih').html("<select name=\"ID_SUB_VAR2\" class=\"text required\" title=\"*\" id=\"ID_SUB_VAR2\"><option value=\"0\"> --pilih-- </option></select>");
		});
	});
	
	jQuery("#ID_SUB_VAR1").change(function(){
		var selectValues = jQuery("#ID_SUB_VAR1").val();
		jQuery.post('./include/ajaxload.php',{ID_SUB_VAR1:selectValues,load_ID_SUB_VAR2:'true'},function(data){
			jQuery('#dafetiologipilih').html(data);
		});
	});
});

</script>
<div align="center">
  <div id="frame">
  <div id="frame_title"><h3 align="left">Diagnosa Keperawatan</h3></div>
	<div id="all">
    <form name="myform" id="myform" action="./kep/add_edit_diagnosa_kep.php?edit=<?phpecho $edit;?>" method="post">
    
	<div id="list_data"></div>
	<br>
	<fieldset class="fieldset" align="left">
            <table width="317" border="0" cellspacing="0" class="tb">
                <tr><td width="80">No RM :</td><td width="233"><?php echo $_GET['NOMR']?></td></tr>
                <tr><td>Nama :</td><td><?php echo $_GET['nama']?>
					<input class="text" value="<?php echo $_GET['idadmission']?>" type="hidden" name="idadmission" id="idadmission" >
					<input class="text" value="<?php echo $_GET['NOMR']?>" type="hidden" name="nomr" id="nomr" >
					<input class="text" value="<?php echo $_GET['nama']?>" type="hidden" name="nama" id="nama" >
				</td></tr>				
            </table>
			</fieldset>
    <fieldset class="fieldset"><legend>Diagnosa Keperawatan</legend>
      <table width="100%" border="0" cellpadding="3" cellspacing="0">
        <tr>
          <td width="300">Domain</td>
          <td colspan="3"><select name="ID_DOMAIN" class="text required" title="*" id="ID_DOMAIN">
            <option value="0"> --pilih-- </option>
			<?php
			  $ss	= mysql_query('select * from m_domain_diagnosa_kep order by id_domain ASC');
			  while($ds = mysql_fetch_array($ss)){
				if($data['ID_DOMAIN'] == $ds['id_domain']): $sel = "selected=Selected"; else: $sel = ''; endif;
				echo '<option value="'.$ds['id_domain'].'" '.$sel.' /> Domain '.$ds['id_domain'].' - '.$ds['nama_domain'].'</option>&nbsp;';
			  }
			?>
          </select>
		  <input class="text" value="" type="hidden" name="ID_DIAGNOSISHIDDEN" id="ID_DIAGNOSISHIDDEN" >		  
		  <input class="text" value="" type="hidden" name="KECAMATANHIDDEN" id="KECAMATANHIDDEN" >
		  <input class="text" value="" type="hidden" name="ID_SUB_VAR2HIDDEN" id="ID_SUB_VAR2HIDDEN" ></td>
        </tr>
        <tr>
          <td>Diagnosis Keperawatan</td>
          <td colspan="3"><div id="diagnosispilih"><select name="ID_DIAGNOSIS" class="text required" title="*" id="ID_DIAGNOSIS">
            <option value="0"> --pilih-- </option>
			<?php
			  $ss	= mysql_query('select * from m_diagnosis_kep where id_domain = "'.$data['ID_DOMAIN'].'" order by ID_DIAGNOSIS ASC');
			  while($ds = mysql_fetch_array($ss)){
				if($data['ID_DIAGNOSIS'] == $ds['ID_DIAGNOSIS']): $sel = "selected=Selected"; else: $sel = ''; endif;
				echo '<option value="'.$ds['ID_DIAGNOSIS'].'" '.$sel.' /> '.$ds['namaID_DIAGNOSIS'].'</option>&nbsp;';
			  }
			?>
          </select></div></td>
        </tr>
        <tr>
          <td>Daftar Gejala - Batasan Karakteristik</td>
          <td colspan="3"><div id="dafgejalapilih"><select name="ID_SUB_VAR1" class="text required" title="*" id="ID_SUB_VAR1">
            <option value="0"> --pilih-- </option>
			<?php
			  $ss	= mysql_query('select * from m_sub_var1_diagnosa_kep where ID_DIAGNOSIS = "'.$data['ID_DIAGNOSIS'].'" order by id_sub_var1 ASC');
			  while($ds = mysql_fetch_array($ss)){
				if($data['ID_SUB_VAR1'] == $ds['ID_SUB_VAR1']): $sel = "selected=Selected"; else: $sel = ''; endif;
				echo '<option value="'.$ds['ID_SUB_VAR1'].'" '.$sel.' /> '.$ds['NAMA_SUB_VAR1'].'</option>&nbsp;';
			  }
			?>
          </select></div></td>
        </tr>
        <tr>
          <td>Daftar Etiologi - Faktor Resiko</td>
          <td colspan="3"><div id="dafetiologipilih"><select name="ID_SUB_VAR2" class="text required" title="*" id="ID_SUB_VAR2">
            <option value="0"> --pilih-- </option>
			<?php
			  $ss	= mysql_query('select * from m_SUB_VAR2_diagnosa_kep where id_diagnosis = "'.$data['ID_DIAGNOSIS'].'" order by ID_SUB_VAR2 ASC');
			  while($ds = mysql_fetch_array($ss)){
				if($data['ID_SUB_VAR2'] == $ds['ID_SUB_VAR2']): $sel = "selected=Selected"; else: $sel = ''; endif;
				echo '<option value="'.$ds['ID_SUB_VAR2'].'" '.$sel.' /> '.$ds['nama_SUB_VAR2'].'</option>&nbsp;';
			  }
			?>
			</select></div></td>
        </tr>
        <tr>
          <td colspan="5" align="right"><input type="submit" name="daftar" class="text" value="  S a v e  "/></td>
        </tr>
      </table>	  
    </fieldset>	
  </form>
  <?php
           $page=1;
				$pager = new PS_Pagination($connect, $sql, $sql1, 15, 5, "orderby=''&searchkey=''&searchfield=''", "index.php?link=diagnosa_kep&");
				$rs = $pager->paginate();?>
                <table class="tb" width="95%" style="margin:10px;" border="0" cellspacing="1" cellspading="1" title="List Kunjungan Data Pasien Per Hari Ini">
                    <tr align="center">
                        <th width="2%">NO</th>
                        <th width="20%">Domain</th>
                        <th width="5%">Kode Diagnosis</th>
                        <th width="23%">Diagnosis Keperawatan</th>
                        <th width="25%">Daftar Gejala - Batasan Karakteristik</th>
                        <th width="25%">Daftar Etiologi - Faktor Resiko</th>
                    </tr>
                <?php   $sql = "SELECT a.*, (select nama_domain from m_domain_diagnosa_kep where id_domain = a.id_domain) nama_domain, (select kode_diagnosis from m_diagnosis_kep where id_diagnosis=a.id_diagnosis) kode_diagnosis, (select nama_diagnosis from m_diagnosis_kep where id_diagnosis=a.id_diagnosis) nama_diagnosis, (select nama_sub_var1 from m_sub_var1_diagnosa_kep where id_sub_var1 = a.id_sub_var1) nama_sub_var1, (select nama_sub_var2 from m_sub_var2_diagnosa_kep where id_sub_var2 = a.id_sub_var2) nama_sub_var2 FROM t_diagnosakep a WHERE a.NOMR ='".$_GET['NOMR']."' and a.idadmission = '".$_GET['idadmission']."' ORDER BY a.id_diagnosakep";
                $sqlcounter = "SELECT count(id_diagnosakep) FROM t_diagnosakep WHERE NOMR ='".$_GET['NOMR']."' and idadmission = '".$_GET['idadmission']."' ORDER BY id_diagnosakep";			

                    $pager->PS_Pagination($connect, $sql, $sqlcounter, 15, 5, "","index.php?link=diagnosa_kep&");
                    //The paginate() function returns a mysql result set
                    $rs = $pager->paginate();
                    if(!$rs) die(mysql_error());
					$NO = 0;
                    while($data = mysql_fetch_array($rs)) {?>
                    <tr <?php   echo "class =";
                        $count++;
                        if ($count % 2) {
                            echo "tr1";
                        }
                        else {
                            echo "tr2";
                        }
                            ?>>
                        <td><?php $NO=($NO+1);
                                if (isset($_GET['page'])==0) {
                                    $hal=0;
                                }else {
                                    $hal=isset($_GET['page'])-1;
                                } echo
					($hal*15)+$NO;?></td>
                        <td><?php echo $data['nama_domain']; ?></td>
                        <td align='center'><?php echo $data['kode_diagnosis']; ?></td>
                        <td><?php echo $data['nama_diagnosis']; ?></td>
                        <td><?php echo $data['nama_sub_var1']; ?></td>
                        <td><?php echo $data['nama_sub_var2']; ?></td>
                    </tr>
                        <?php	}

                    //Display the full navigation in one go
                    //echo $pager->renderFullNav();

                    //Or you can display the inidividual links
                    echo "<div style='padding:5px;' align=\"center\"><br />";

                    //Display the link to first page: First
                    echo $pager->renderFirst()." | ";

                    //Display the link to previous page: <<
                    echo $pager->renderPrev()." | ";

                    //Display page links: 1 2 3
                    echo $pager->renderNav()." | ";

                    //Display the link to next page: >>
                    echo $pager->renderNext()." | ";

                    //Display the link to last page: Last
                    echo $pager->renderLast();

                    echo "</div>";
                    ?>

                </table>

                <?php

                //Display the full navigation in one go
                //echo $pager->renderFullNav();

                //Or you can display the inidividual links
                echo "<div style='padding:5px;' align=\"center\"><br />";

                //Display the link to first page: First
                echo $pager->renderFirst()." | ";

                //Display the link to previous page: <<
                echo $pager->renderPrev()." | ";

                //Display page links: 1 2 3
                echo $pager->renderNav()." | ";

                //Display the link to next page: >>
                echo $pager->renderNext()." | ";

                //Display the link to last page: Last
                echo $pager->renderLast();

                echo "</div>";
                ?>
   </div>
  </div>
  </div>